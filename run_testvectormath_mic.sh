#!/bin/bash

MIC_FLOAT_16_RES_FILE_NAME="results_mic_f16.txt"
MIC_DOUBLE_8_RES_FILE_NAME="results_mic_d8.txt"

OUT_BINARY="mic.out"

rm $MIC_FLOAT_16_RES_FILE_NAME

icc -DVECTOR_TYPE=3 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=1 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_FLOAT_16_RES_FILE_NAME

icc -DVECTOR_TYPE=3 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=2 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_FLOAT_16_RES_FILE_NAME

icc -DVECTOR_TYPE=3 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=3 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_FLOAT_16_RES_FILE_NAME

icc -DVECTOR_TYPE=3 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=4 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_FLOAT_16_RES_FILE_NAME

icc -DVECTOR_TYPE=3 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=5 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_FLOAT_16_RES_FILE_NAME


icc -DVECTOR_TYPE=3 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=10 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_FLOAT_16_RES_FILE_NAME

icc -DVECTOR_TYPE=3 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=11 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_FLOAT_16_RES_FILE_NAME

icc -DVECTOR_TYPE=3 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=12 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_FLOAT_16_RES_FILE_NAME

icc -DVECTOR_TYPE=3 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=13 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_FLOAT_16_RES_FILE_NAME

icc -DVECTOR_TYPE=3 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=14 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_FLOAT_16_RES_FILE_NAME

icc -DVECTOR_TYPE=3 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=15 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_FLOAT_16_RES_FILE_NAME


rm $MIC_DOUBLE_8_RES_FILE_NAME

icc -DVECTOR_TYPE=6 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=1 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_DOUBLE_8_RES_FILE_NAME

icc -DVECTOR_TYPE=6 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=2 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_DOUBLE_8_RES_FILE_NAME

icc -DVECTOR_TYPE=6 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=3 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_DOUBLE_8_RES_FILE_NAME

icc -DVECTOR_TYPE=6 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=4 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_DOUBLE_8_RES_FILE_NAME

icc -DVECTOR_TYPE=6 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=5 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_DOUBLE_8_RES_FILE_NAME


icc -DVECTOR_TYPE=6 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=10 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_DOUBLE_8_RES_FILE_NAME

icc -DVECTOR_TYPE=6 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=11 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_DOUBLE_8_RES_FILE_NAME

icc -DVECTOR_TYPE=6 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=12 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_DOUBLE_8_RES_FILE_NAME

icc -DVECTOR_TYPE=6 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=13 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_DOUBLE_8_RES_FILE_NAME

icc -DVECTOR_TYPE=6 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=14 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_DOUBLE_8_RES_FILE_NAME

icc -DVECTOR_TYPE=6 -DMAT_IMPLEMENT=5 -DMAT_FUNCTION=15 -mmic -O2 testvectormath.cpp -o $OUT_BINARY
micnativeloadex ./$OUT_BINARY >> $MIC_DOUBLE_8_RES_FILE_NAME