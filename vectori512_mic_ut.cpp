

#define INSTRSET 9

#include "vectori512_mic.h"
#include "vectorf512_mic.h"

#include <stdio.h>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

int g_totalTests = 0;
int g_totalFailed = 0;
int g_testMaxId = 0;
int g_failCount = 0;
bool g_allSuccess = true;
bool g_supressMessages = false;
char *g_test_header_ptr = NULL;


#define INIT_TEST(test_header_ptr) { \
    g_test_header_ptr = (test_header_ptr); \ 
    g_failCount = 0; \
    g_testMaxId = 0; \
    g_allSuccess = true; \
    g_supressMessages = false;}

#define SUPRESS_MESSAGES() { g_supressMessages = true; }
#define CHECK_CONDITION(cond, msg) \    
    g_totalTests++; \
    g_testMaxId++; \
    if(!(cond)){\
            if(g_supressMessages == false) printf("%s Id: %d - %s FAIL\n", g_test_header_ptr, g_testMaxId, (msg)); \
            g_totalFailed++; \
            g_failCount++; \
            g_allSuccess = false; \
    } \
    else \
    { \
        if(g_supressMessages == false) printf("%s Id: %d - %s OK\n",g_test_header_ptr, g_testMaxId, (msg));  \
    } \

typedef enum VECTYPE{
    VECTYPE_512B,
    VECTYPE_16B,
    VECTYPE_8B,
    VECTYPE_16I,
    VECTYPE_16UI,
    VECTYPE_8Q,
    VECTYPE_8UQ,
    VECTYPE_16F,
    VECTYPE_8D
}VECTYPE;

// Test functions
void test_vec512b_basic();
void test_vec16b_basic();
void test_vec8b_basic();
void test_vec16i_basic();
void test_vec16ui_basic();
void test_vec8q_basic();
// void test_vec8uq_basic();
void test_vec16f_basic();
void test_vec8d_basic();

void test_vec16f_functions();
void test_vec8d_functions();

// Helper functions
int test_element(void * vector, VECTYPE type, uint64_t value, int index);
int test_all_elements(void * vector, VECTYPE type, uint64_t value);
int test_vector_with_array(void *vector, VECTYPE type, void *arr);
int test_vector_with_pattern(void * vector, VECTYPE type, uint64_t value);

int test_float(float x, float y);
int test_double(double x, double y);
int test_vector_with_float(Vec16f & vec, float value);
int test_vector_with_double(Vec8d & vec, double value);

void fill_vector_with_pattern(void *vector, VECTYPE type, uint64_t pattern);
void print_all_elements(void * vector, VECTYPE type);

int main()
{
    // Basic tests:
    test_vec512b_basic();
    test_vec16b_basic();
    test_vec8b_basic();
    test_vec16i_basic();
    test_vec16ui_basic();
    test_vec8q_basic();
    //TODO: test_vec8uq();
    test_vec16f_basic();
    test_vec8d_basic();

    test_vec16f_functions();
    test_vec8d_functions();

    printf(" Executed tests: %d, Failed: %d\n", g_totalTests, g_totalFailed);
}

void test_vec512b_basic()
{
    char header[] = "Vec512b test";
    INIT_TEST(header);
    
    // comment this out to see detailed test list
    SUPRESS_MESSAGES();

    // Basic tests for 512b vectors
    // Vec512i()
    {
        Vec512b vec0 = Vec512b();
        CHECK_CONDITION(&vec0 != NULL, "Vec512b()");
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_512B, 0) == 0, "Vec512b() 1");
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_512B, 1) != 0, "Vec512b() 2");
    }
    
    // Vec512i(int i)
    {
        Vec512b vec0 = Vec512b(1);
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_512B, 1) == 0, "Vec512b(int i) 1");
    }
    
    // Vec512b(__m512i const & x)
    {
        __m512i zmm0 = _mm512_setzero_epi32();
        Vec512b vec0 = Vec512b(zmm0);
        // all bits should be '0'
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_512B, 0) == 0, "Vec512b(__m512i const & x) 1");
        // all bits should be '1'
        zmm0 = _mm512_set1_epi32(0xFFFFFFFF);
        Vec512b vec1 = Vec512b(zmm0);
        CHECK_CONDITION(test_all_elements((void*)&vec1, VECTYPE_512B, 1) == 0, "Vec512b(__m512i const & x) 2");
        // 16/16 pattern
        zmm0 = _mm512_set1_epi32(0x0000FFFF);
        Vec512b vec2 = Vec512b(zmm0);
        CHECK_CONDITION(test_all_elements((void*)&vec2, VECTYPE_512B, 0) != 0, "Vec512b(__m512i const & x) 3");
        CHECK_CONDITION(test_element((void*)&vec2, VECTYPE_512B, 1, 15) == 0, "Vec512b(__m512i const & x) 4");
        CHECK_CONDITION(test_element((void*)&vec2, VECTYPE_512B, 0, 16) == 0, "Vec512b(__m512i const & x) 5");
    }
        
    // Vec512b(Vec512b const & a0)
    {
        Vec512b vec0 = Vec512b(_mm512_set1_epi32(0xFFFFFFFF));
        Vec512b vec1 = Vec512b(vec0);
        CHECK_CONDITION(test_all_elements((void*)&vec1, VECTYPE_512B, 1) == 0, "Vec512b(Vec512b const & a0) 1");
    }
        
    // Vec512b & operator = (__m512i const & x)
    {
        Vec512b vec0;
        __m512i zmm0 = _mm512_set1_epi32(0xFFFFFFFF);
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_512B, 0) == 0, "Vec512b & operator = (__m512i const & x) 1");
        vec0 = zmm0;
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_512B, 1) == 0, "Vec512b & operator = (__m512i const & x) 2");
    }
        
    // operator __m512i()
    {
        Vec512b vec0 = _mm512_set1_epi32(0xFFFF0000);
        __m512i zmm0 = vec0;
        Vec512b vec1 = zmm0;
        // vec10 filled with pattern 0xFFFF0000
        CHECK_CONDITION(test_element((void*)&vec1, VECTYPE_512B, 0, 4) == 0, "operator __m512i() 1");
        CHECK_CONDITION(test_element((void*)&vec1, VECTYPE_512B, 1, 24) == 0, "operator __m512i() 2");
    }

    // Vec512b & load(void const * p)
    {
        __declspec(align(64)) uint32_t p0[16] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
        Vec512b vec0;
        vec0.load(p0);
        CHECK_CONDITION(test_element((void*)&vec0, VECTYPE_512B, 1, 4) == 0, "Vec512b & load(void const * p) 1");
        CHECK_CONDITION(test_element((void*)&vec0, VECTYPE_512B, 0, 50) == 0, "Vec512b & load(void const * p) 2");
    }
        
    // Vec512b & load_a(void const * p)
    {
        __declspec(align(64)) uint32_t p0[16] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
        Vec512b vec0;
        vec0.load_a(p0);
        CHECK_CONDITION(test_element((void*)&vec0, VECTYPE_512B, 1, 4) == 0, "Vec512b & load_a(void const * p) 1");
        CHECK_CONDITION(test_element((void*)&vec0, VECTYPE_512B, 0, 510) == 0, "Vec512b & load_a(void const * p) 2");
    }

    // void store(void * p)
    {
        __declspec(align(64)) uint32_t p0[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        Vec512b vec0 = Vec512b(1);
        vec0.store(p0);
        CHECK_CONDITION((p0[5] == 0xFFFFFFFF), "void store(void *p) 1");
    }


    // void store_a(void * p)
    // Vec512b const & set_bit(uint32_t index, int value)
    {
        __declspec(align(64)) uint32_t p0[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        Vec512b vec0 = Vec512b();
        vec0.set_bit(300, 1);
        vec0.set_bit(302, 1);
        vec0.store_a(p0);
        CHECK_CONDITION((p0[9] == 0x00005000), "void store_a(void *p) + set_bit 1");
    }
        
    // int get_bit(uint32_t index) + int operator [] (uint32_t index)
    {
        Vec512b vec0 = Vec512b(1);
        CHECK_CONDITION((vec0.get_bit(56) == 1 && vec0.get_bit(456) == 1), "int get_bit(uint32_t index) 1");
        CHECK_CONDITION((vec0[83] == 1 && vec0[123] == 1), "int operator [] (uint32_t index) 2");
    }
        
    // static inline Vec512b operator & (Vec512b const & a, Vec512b const & b)
    {
        Vec512b vec0;
        fill_vector_with_pattern((void*)&vec0, VECTYPE_512B, 0x1234ABCD1234ABCD);
        Vec512b vec1;
        fill_vector_with_pattern((void*)&vec1, VECTYPE_512B, 0xEDCB5432EDCB5432);
        Vec512b vec2 = vec0 & vec1;
        CHECK_CONDITION(test_all_elements((void*)&vec2, VECTYPE_512B, 0) == 0, "Vec512b operator & (Vec512b const & a, Vec512b const & b) 1");
    }

    // static inline Vec512b operator && (Vec512b const & a, Vec512b const & b) 
    {
        Vec512b vec0;
        fill_vector_with_pattern((void*)&vec0, VECTYPE_512B, 0x1234ABCD1234ABCD);
        Vec512b vec1;
        fill_vector_with_pattern((void*)&vec1, VECTYPE_512B, 0xEDCB5432EDCB5432);
        Vec512b vec2 = vec0 && vec1;
        CHECK_CONDITION(test_all_elements((void*)&vec2, VECTYPE_512B, 0) == 0, "Vec512b operator && (Vec512b const & a, Vec512b const & b) 1");
    }

    // static inline Vec512b operator | (Vec512b const & a, Vec512b const & b)
    {
        Vec512b vec0;
        fill_vector_with_pattern((void*)&vec0, VECTYPE_512B, 0x1234ABCD1234ABCD);
        Vec512b vec1;
        fill_vector_with_pattern((void*)&vec1, VECTYPE_512B, 0xEDCB5432EDCB5432);
        Vec512b vec2 = vec0 | vec1;
        CHECK_CONDITION(test_all_elements((void*)&vec2, VECTYPE_512B, 1) == 0, "Vec512b operator | (Vec512b const & a, Vec512b const & b) 1");
    }

    // static inline Vec512b operator || (Vec512b const & a, Vec512b const & b)
    {
        Vec512b vec0;
        fill_vector_with_pattern((void*)&vec0, VECTYPE_512B, 0x1234ABCD1234ABCD);
        Vec512b vec1;
        fill_vector_with_pattern((void*)&vec1, VECTYPE_512B, 0xEDCB5432EDCB5432);
        Vec512b vec2 = vec0 || vec1;
        CHECK_CONDITION(test_all_elements((void*)&vec2, VECTYPE_512B, 1) == 0, "Vec512b operator || (Vec512b const & a, Vec512b const & b) 1");
    }

    // static inline Vec512b operator ^ (Vec512b const & a, Vec512b const & b)
    {
        Vec512b vec0;
        fill_vector_with_pattern((void*)&vec0, VECTYPE_512B, 0x1234ABCD1234ABCD);
        Vec512b vec1;
        fill_vector_with_pattern((void*)&vec1, VECTYPE_512B, 0xEDCB5432EDCB5432);
        Vec512b vec2 = vec0 ^ vec1;
        CHECK_CONDITION(test_all_elements((void*)&vec2, VECTYPE_512B, 1) == 0, "Vec512b operator ^ (Vec512b const & a, Vec512b const & b) 1");
        fill_vector_with_pattern((void*)&vec0, VECTYPE_512B, 0xDEADBEEFDEADBEEF);
        fill_vector_with_pattern((void*)&vec1, VECTYPE_512B, 0xDEADBEEFDEADBEEF);
        Vec512b vec3 = vec0 ^ vec1;
        CHECK_CONDITION(test_all_elements((void*)&vec3, VECTYPE_512B, 0) == 0, "Vec512b operator ^ (Vec512b const & a, Vec512b const & b) 2");
    }
        
        
    // static inline Vec512b operator ~ (Vec512b const & a)
    {
        Vec512b vec0 = Vec512b(0);
        Vec512b vec1 = Vec512b(1);
        Vec512b vec2 = ~vec0;
        Vec512b vec3 = ~vec1;
        CHECK_CONDITION(test_all_elements((void*)&vec2, VECTYPE_512B, 1) == 0, "static inline Vec512b operator ~ (Vec512b const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec3, VECTYPE_512B, 0) == 0, "static inline Vec512b operator ~ (Vec512b const & a) 2");
    }
        
    
    // static inline Vec512b & operator &= (Vec512b & a, Vec512b const & b)
    {
        Vec512b vec0;
        Vec512b vec1;
        fill_vector_with_pattern((void*)&vec0, VECTYPE_512B, 0xDEADBEEFDEADBEEF);
        fill_vector_with_pattern((void*)&vec1, VECTYPE_512B, 0xFEEDBABEFEEDBABE);
        vec1 &= vec0;
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec1, VECTYPE_512B, 0xDEADBAAEDEADBAAE) == 0, "static inline Vec512b & operator &= (Vec512b & a, Vec512b const & b) 1");
    }

    // static inline Vec512b & operator |= (Vec512b & a, Vec512b const & b)
    {
        Vec512b vec0;
        Vec512b vec1;
        fill_vector_with_pattern((void*)&vec0, VECTYPE_512B, 0x10012254FDBACF36);
        fill_vector_with_pattern((void*)&vec1, VECTYPE_512B, 0x51389D43F9C2B566);
        vec1 |= vec0;
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec1, VECTYPE_512B, 0x5139BF57FDFAFF76) == 0, "static inline Vec512b & operator |= (Vec512b & a, Vec512b const & b)");
    }

    // static inline Vec512b & operator ^= (Vec512b & a, Vec512b const & b)
    {
        Vec512b vec0;
        Vec512b vec1;
        fill_vector_with_pattern((void*)&vec0, VECTYPE_512B, 0x10012254FDBACF36);
        fill_vector_with_pattern((void*)&vec1, VECTYPE_512B, 0x51389D43F9C2B566);
        vec1 ^= vec0;
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec1, VECTYPE_512B, 0x4139BF1704787A50) == 0, "static inline Vec512b & operator ^= (Vec512b & a, Vec512b const & b)");
    }

    // static inline Vec512b andnot (Vec512b const & a, Vec512b const & b)
    {
        Vec512b vec0;
        Vec512b vec1;
        fill_vector_with_pattern((void*)&vec0, VECTYPE_512B, 0x10012254FDBACF36);
        fill_vector_with_pattern((void*)&vec1, VECTYPE_512B, 0x51389D43F9C2B566);
        Vec512b vec2 = andnot(vec0, vec1);
        // print_all_elements((void*)&vec0, VECTYPE_512B);
        // print_all_elements((void*)&vec1, VECTYPE_512B);
        // print_all_elements((void*)&vec2, VECTYPE_512B);
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec2, VECTYPE_512B, 0x0001221404384A10) == 0, "static inline Vec512b andnot (Vec512b const & a, Vec512b const & b)");
    }

    if(g_allSuccess == true)
    {
        printf("Vec512b OK\n");
    }
    else
    {
        printf("Vec512b found %d errors\n", g_failCount);
    }
}

void test_vec16b_basic()
{
    char header[] = "Vec16b test";
    INIT_TEST(header);
    
    // comment this out to see detailed test list
    SUPRESS_MESSAGES();

    // Vec16b()
    {
        Vec16b vec0 = Vec16b();
        CHECK_CONDITION(&vec0 != NULL, "Vec16b() 1");
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16B, 0) == 0), "Vec16b() 2");
    }

    // Vec16b(__mmask16 mask)
    {
        Vec16b vec0 = Vec16b(0xFFFF);
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16B, 1) == 0), "Vec16b(__mmask16 mask) 1");
        Vec16b vec1 = Vec16b(0xABBA);
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec1, VECTYPE_16B, 0xABBA) == 0), "Vec16b(__mmask16 mask) 2");
    }
    
    // Vec16b & operator = (__mmask16 const & x)
    {
        Vec16b vec0 = Vec16b(0xABBA);
        Vec16b vec1 = vec0;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec1, VECTYPE_16B, 0xABBA) == 0), "Vec16b & operator = (__mmask16 const & x) 1");
    }

    // operator __mmask16() const 
    {
        Vec16b vec0 = Vec16b(0x1234);
        __declspec(align(64)) __mmask16 mask = vec0;
        CHECK_CONDITION((_mm512_mask2int(mask) == 0x1234), "operator __mmask16() const 1");
    }

    // Vec16b & load(void const *p) 
    {
        __declspec(align(64)) int p0 = 0xDEAD;
        Vec16b vec0;
        vec0.load((void*)&p0);
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec0, VECTYPE_16B, 0xDEAD) == 0), "Vec16b & load(void const *p) 1");
    }

    // Vec16b & load_a(void const *p)
    {
        __declspec(align(64)) int p0 = 0xBEEF;
        Vec16b vec0;
        vec0.load_a((void*)&p0);
        //print_all_elements((void*)&vec0, VECTYPE_16B);
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec0, VECTYPE_16B, 0xBEEF) == 0), "Vec16b & load_a(void const *p) 1");
    }

    // void store(void * p) const
    {
        __declspec(align(64)) int p0 = 0x0000;
        Vec16b vec0(0x5423);
        vec0.store((void*)&p0);
        CHECK_CONDITION((p0 == 0x5423), "void store(void * p) const  1");
    }

    // void store_a(void * p) const
    {
        __declspec(align(64)) int p0 = 0x0000;
        Vec16b vec0(0x7A1B);
        vec0.store_a((void*)&p0);
        CHECK_CONDITION((p0 == 0x7A1B), "void store_a(void * p) const  1");
    }
 
    // Vec16b const & set_bit(uint32_t index, int value) 
    {
        Vec16b vec0(0x7A1B);
        vec0.set_bit(5, 1);
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec0, VECTYPE_16B, 0x7A3B) == 0), "Vec16b const & set_bit(uint32_t index, int value) 1");
    }
   
    // int get_bit(uint32_t index) const
    {
        Vec16b vec0(0x7A1B);
        __declspec(align(64)) int p0 = vec0.get_bit(7);
        __declspec(align(64)) int p1 = vec0.get_bit(11);
        CHECK_CONDITION(((p0 == 0) && (p1 == 1)), "int get_bit(uint32_t index) const 1");
    }
   
    // int operator [] (uint32_t index) const 
    {
        Vec16b vec0(0x7A1B);
        __declspec(align(64)) int p0 = vec0[7];
        __declspec(align(64)) int p1 = vec0[11];
        CHECK_CONDITION(((p0 == 0) && (p1 == 1)), "int operator [] (uint32_t index) const 1");
    }

    // static inline Vec16b operator & (Vec16b const & a, Vec16b const & b) 
    {
        Vec16b vec0(0x1235);
        Vec16b vec1(0xAB38);
        Vec16b vec2 = vec0 & vec1;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec2, VECTYPE_16B, 0x0230) == 0), "static inline Vec16b operator & (Vec16b const & a, Vec16b const & b) 1");
    }

    // static inline Vec16b operator && (Vec16b const & a, Vec16b const & b)
    {
        Vec16b vec0(0x1235);
        Vec16b vec1(0xAB38);
        Vec16b vec2 = vec0 && vec1;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec2, VECTYPE_16B, 0x0230) == 0), "static inline Vec16b operator && (Vec16b const & a, Vec16b const & b) 1");
    }
   
    // static inline Vec16b & operator &= (Vec16b & a, Vec16b const & b)
    {
        Vec16b vec0(0x0000);
        Vec16b vec1(0xFFFF);
        vec1 &= vec0;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec1, VECTYPE_16B, 0x0000) == 0), "static inline Vec16b & operator &= (Vec16b & a, Vec16b const & b) 1");
    }
   
    // static inline Vec16b operator | (Vec16b const & a, Vec16b const & b)
    {
        Vec16b vec0(0x00AB);
        Vec16b vec1(0xFF03);
        Vec16b vec2;
        vec2 = vec1 | vec0;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec2, VECTYPE_16B, 0xFFAB) == 0), "static inline Vec16b operator | (Vec16b const & a, Vec16b const & b) 1");
    }
   
    // static inline Vec16b operator || (Vec16b const & a, Vec16b const & b)
    {
        Vec16b vec0(0x00AB);
        Vec16b vec1(0xF003);
        Vec16b vec2;
        vec2 = vec0 || vec1;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec2, VECTYPE_16B, 0xF0AB) == 0), "static inline Vec16b operator || (Vec16b const & a, Vec16b const & b) 1");
    }
   
    // static inline Vec16b & operator |= (Vec16b & a, Vec16b const & b)
    {
        Vec16b vec0(0x00AB);
        Vec16b vec1(0xF003);
        vec1 |= vec0;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec1, VECTYPE_16B, 0xF0AB) == 0), "static inline Vec16b operator || (Vec16b const & a, Vec16b const & b) 1");
    }
   
    // static inline Vec16b operator ^ (Vec16b const & a, Vec16b const & b)
    {
        Vec16b vec0(0xF11C);
        Vec16b vec1(0xB78D);
        Vec16b vec2;
        vec2 = vec0 ^ vec1;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec2, VECTYPE_16B, 0x4691) == 0), "static inline Vec16b operator ^ (Vec16b const & a, Vec16b const & b) 1");
    }
   
    // static inline Vec16b & operator ^= (Vec16b & a, Vec16b const & b) 
    {
        Vec16b vec0(0x00AB);
        Vec16b vec1(0xF003);
        vec0 ^= vec1;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec0, VECTYPE_16B, 0xF0A8) == 0), "static inline Vec16b & operator ^= (Vec16b & a, Vec16b const & b) 1");
    }
   
    // static inline Vec16b operator ~ (Vec16b const & a)
    {
        Vec16b vec0(0x00AB);
        Vec16b vec1 = ~vec0;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec1, VECTYPE_16B, 0xFF54) == 0), "static inline Vec16b operator ~ (Vec16b const & a) 1");
    }
   
    // static inline Vec16b operator ! (Vec16b const & a)
    {
        Vec16b vec0(0x59BF);
        Vec16b vec1 = !vec0;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec1, VECTYPE_16B, 0xA640) == 0), "static inline Vec16b operator ! (Vec16b const & a) 1");
    }
   
    // static inline Vec16b andnot(Vec16b const & a, Vec16b const & b)
    {
        Vec16b vec0(0x59BF); 
        Vec16b vec1(0xFACE);
        Vec16b vec2 = andnot(vec0, vec1);
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec2, VECTYPE_16B, 0x0131) == 0), "static inline Vec16b andnot(Vec16b const & a, Vec16b const & b) 1");
    }
    
    if(g_allSuccess == true)
    {
        printf("Vec16b OK\n");
    }
    else
    {
        printf("Vec16b found %d errors\n", g_failCount);
    }
}

void test_vec8b_basic()
{
    char header[] = "Vec8b test";
    INIT_TEST(header);
    
    // comment this out to see detailed test list
    SUPRESS_MESSAGES();

    // Vec8b()
    {
        Vec8b vec0;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec0, VECTYPE_8B, 0x00) == 0), "Vec8b() 1");
    }

    // Vec8b(int mask)
    {
        Vec8b vec0(0xCE);
        //print_all_elements((void*)&vec2, VECTYPE_8B);
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec0, VECTYPE_8B, 0xCE) == 0), "Vec8b(int mask) 1");
    }

    // Vec8b & operator = (__mmask8 const & x)
    {
        __declspec(align(64)) __mmask8 mask = _mm512_int2mask(0x67);
        Vec8b vec0(0xCE);
        vec0 = mask;
        //print_all_elements((void*)&vec3, VECTYPE_8B);
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec0, VECTYPE_8B, 0x67) == 0), "Vec8b & operator = (__mmask8 const & x) 1");
    }

    // operator __mmask8() const
    {
        Vec8b vec0(187);
        __mmask8 mask = vec0;
        CHECK_CONDITION((_mm512_mask2int(mask) == 187), "operator __mmask8() const 1");
    }
    
    // Vec8b & load(void const *p)
    {
        __declspec(align(64)) int p0 = 0x9A;
        Vec8b vec0;
        vec0.load((void*)&p0);
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec0, VECTYPE_8B, 0x9A) == 0), "Vec8b & load(void const *p) 1");
    }
    
    // Vec8b & load_a(void const *p)
    {
        __declspec(align(64)) int p0 = 0x76;
        Vec8b vec0;
        vec0.load_a((void*)&p0);
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec0, VECTYPE_8B, 0x76) == 0), "Vec8b & load_a(void const *p) 1");
    }
    
    // void store(void * p) const 
    {
        __declspec(align(64)) int p0 = 0;
        Vec8b vec0 = 0x63;
        vec0.store((void*)&p0);
        CHECK_CONDITION((p0 == 0x63), "void store(void * p) const  1");
    }

    // void store_a(void * p) const 
    {
        __declspec(align(64)) int p0 = 0;
        Vec8b vec0 = 0x63;
        vec0.store((void*)&p0);
        CHECK_CONDITION((p0 == 0x63), "void store_a(void * p) const 1");
    }
    
    // Vec8b const & set_bit(uint32_t index, int value)
    {
        Vec8b vec0 = 0x55;
        vec0.set_bit(0, 1);
        vec0.set_bit(1, 1);
        vec0.set_bit(2, 1);
        vec0.set_bit(3, 0);
        //print_all_elements((void*)&vec0, VECTYPE_8B);
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec0, VECTYPE_8B, 0x57) == 0), "Vec8b const & set_bit(uint32_t index, int value) 1");
    
    
    // int get_bit(uint32_t index) const
        __declspec(align(64)) int p0 = 0;
        p0 = vec0.get_bit(4);
        CHECK_CONDITION((p0 == 1), "Vec8b const & get_bit(uint32_t index) const 1");

    // int operator [] (uint32_t index) const
        p0 = vec0[7];
        CHECK_CONDITION((p0 == 0), "int operator [] (uint32_t index) const 1");
    }

    // static inline Vec8b operator & (Vec8b const & a, Vec8b const & b)
    {
        Vec8b vec0 = 0x48;
        Vec8b vec1 = 0x57;
        Vec8b vec2 = vec0 & vec1;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec2, VECTYPE_8B, 0x40) == 0), "Vec8b const & set_bit(uint32_t index, int value) 1");
    }
    
    // static inline Vec8b operator && (Vec8b const & a, Vec8b const & b)
    {
        Vec8b vec0 = 0x32;
        Vec8b vec1 = 0xF3;
        Vec8b vec2 = vec0 && vec1;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec2, VECTYPE_8B, 0x32) == 0), "static inline Vec8b operator && (Vec8b const & a, Vec8b const & b) 1");
    }
    
    // static inline Vec8b & operator &= (Vec8b & a, Vec8b const & b)
    {
        Vec8b vec0 = 0x5B;
        Vec8b vec1 = 0x9A;
        vec1 &= vec0;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec1, VECTYPE_8B, 0x1A) == 0), "static inline Vec8b & operator &= (Vec8b & a, Vec8b const & b) 1");
    }
    
    // static inline Vec8b operator | (Vec8b const & a, Vec8b const & b)
    {
        Vec8b vec0 = 0x66;
        Vec8b vec1 = 0x11;
        Vec8b vec2 = vec0 | vec1;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec2, VECTYPE_8B, 0x77) == 0), "static inline Vec8b operator | (Vec8b const & a, Vec8b const & b) 1");
    }
    
    // static inline Vec8b operator || (Vec8b const & a, Vec8b const & b)
    {
        Vec8b vec0 = 0x01;
        Vec8b vec1 = 0x10;
        Vec8b vec2 = vec0 || vec1;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec2, VECTYPE_8B, 0x11) == 0), "static inline Vec8b operator || (Vec8b const & a, Vec8b const & b) 1");
    }
    
    // static inline Vec8b & operator |= (Vec8b & a, Vec8b const & b)
    {
        Vec8b vec0 = 0x78;
        Vec8b vec1 = 0x66;
        vec1 |= vec0;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec1, VECTYPE_8B, 0x7E) == 0), "static inline Vec8b operator || (Vec8b const & a, Vec8b const & b) 1");
    }
    
    // static inline Vec8b operator ^ (Vec8b const & a, Vec8b const & b)
    {
        Vec8b vec0 = 0x78;
        Vec8b vec1 = 0x66;
        Vec8b vec2 = vec0 ^ vec1;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec2, VECTYPE_8B, 0x1E) == 0), "static inline Vec8b operator ^ (Vec8b const & a, Vec8b const & b) 1");
    }
    
    // static inline Vec8b & operator ^= (Vec8b & a, Vec8b const & b)
    {
        Vec8b vec0 = 0x13;
        Vec8b vec1 = 0x05;
        vec1 ^= vec0;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec1, VECTYPE_8B, 0x16) == 0), "static inline Vec8b & operator ^= (Vec8b & a, Vec8b const & b) 1");
    }
    
    // static inline Vec8b operator ~ (Vec8b const & a)
    {
        Vec8b vec0 = 0x82;
        Vec8b vec1 = ~vec0;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec1, VECTYPE_8B, 0x7D) == 0), "static inline Vec8b operator ~ (Vec8b const & a) 1");
    }
    
    // static inline Vec8b operator ! (Vec8b const & a)
    {
        Vec8b vec0 = 0x9A;
        Vec8b vec1 = !vec0;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec1, VECTYPE_8B, 0x65) == 0), "static inline Vec8b operator ! (Vec8b const & a) 1");
    }
    
    // static inline Vec8b andnot(Vec8b const & a, Vec8b const & b)
    {
        Vec8b vec0 = 0xBF;
        Vec8b vec1 = 0xF0;
        Vec8b vec2 = andnot(vec0, vec1);
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec2, VECTYPE_8B, 0x0F) == 0), "static inline Vec8b operator ! (Vec8b const & a) 1");
    }

    if(g_allSuccess == true)
    {
        printf("Vec8b OK\n");
    }
    else
    {
        printf("Vec8b found %d errors\n", g_failCount);
    }
}

void test_vec16i_basic()
{
    char header[] = "Vec16i test";
    INIT_TEST(header);
    
    // comment this out to see detailed test list
    SUPRESS_MESSAGES();
    
    // Vec16i()
    {
        Vec16i vec0 = Vec16i();
        CHECK_CONDITION((&vec0 != NULL), "Vec16i() 1");
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_16I, 0) == 0, "Vec16i() 2");
    }
    
    // Vec16i(int i)
    {
        Vec16i vec0 = Vec16i(-18);
        //print_all_elements((void *)&vec0, VECTYPE_16I);
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_16I, -18) == 0, "Vec16i(int i) 1");
    }

    // Vec16i(int32_t i0, int32_t i1, int32_t i2,  int32_t i3,  int32_t i4,  int32_t i5,  int32_t i6,  int32_t i7,
    //     int32_t i8, int32_t i9, int32_t i10, int32_t i11, int32_t i12, int32_t i13, int32_t i14, int32_t i15)
    {
        Vec16i vec0 = Vec16i(-15, -43, -128, -73, -89, 0, 2, 5, 9, 12, 18, 34, 37, 43, 64, 112);
        int arr0[16] =      {-15, -43, -128, -73, -89, 0, 2, 5, 9, 12, 18, 34, 37, 43, 64, 112};
        //print_all_elements((void *)&vec0, VECTYPE_16I);
        CHECK_CONDITION(test_vector_with_array((void*)&vec0, VECTYPE_16I, (void*)arr0) == 0, "Vec16i(int32_t i0, int32_t i1, ... 1");
    }

    // Vec16i(Vec512b const & a)
    {
        Vec512b vec0 = Vec512b(1);
        Vec16i vec1 = Vec16i(vec0);
        // print_all_elements((void *)&vec0, VECTYPE_512B);
        // print_all_elements((void *)&vec1, VECTYPE_16I);
        CHECK_CONDITION(test_all_elements((void*)&vec1, VECTYPE_16I, 0xFFFFFFFF) == 0, "Vec16i(Vec512b const & a) 1");
    }

    // Vec16i(__m512i const & x) 
    {
        __m512i zmm0 = _mm512_set1_epi32(17);
        Vec16i vec0 = Vec16i(zmm0);
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_16I, 17) == 0, "Vec16i(__m512i const & x) 1");
    }

    // Vec16i & operator = (__m512i const & x)
    {
        __m512i zmm0 = _mm512_set1_epi32(908);
        Vec16i vec0 = Vec16i(-12356);
        vec0 = zmm0;
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_16I, 908) == 0, "Vec16i & operator = (__m512i const & x) 1");
    }

    // operator __m512i() const 
    {
        Vec16i vec0 = Vec16i(8892);
        __m512i zmm0 = vec0;
        Vec16i vec1 = Vec16i(zmm0);
        CHECK_CONDITION(test_all_elements((void*)&vec1, VECTYPE_16I, 8892) == 0, "operator __m512i() const  1");
    }

    
    // Vec16i & load(void const * p)
    {
        __declspec(align(64)) int p0[16] = {5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298};
        Vec16i vec0;
        vec0.load((void*)p0);
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_16I, 5298) == 0, "Vec16i & load(void const * p)  1");
    }

    // Vec16i & load_a(void const * p)
    {
        __declspec(align(64)) int p0[16]  = { -987124, -987124, -987124, -987124, -987124, -987124, -987124, -987124, -987124, -987124, -987124, -987124, -987124, -987124, -987124, -987124};
        Vec16i vec0;
        vec0.load_a((void*)p0);
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_16I, -987124) == 0, "Vec16i & load_a(void const * p)  1");
    }
    
    // Vec16i & load_partial(int n, void const * p) 
    {
        __declspec(align(64)) int p0[16] =  { 915, 915, 915, 915, 915, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17 };
        Vec16i vec0 = Vec16i(8);
        vec0.load_partial(6, (void*)p0);
        // print_all_elements((void*)&vec0, VECTYPE_16I);
        CHECK_CONDITION(test_element((void*)&vec0, VECTYPE_16I, 915, 4) == 0, "Vec16i & load_partial(int n, void const * p)  1");
        CHECK_CONDITION(test_element((void*)&vec0, VECTYPE_16I, 17, 5) == 0, "Vec16i & load_partial(int n, void const * p)  2");
        CHECK_CONDITION(test_element((void*)&vec0, VECTYPE_16I, 0, 6) == 0, "Vec16i & load_partial(int n, void const * p)  3");
    }
    
    // void store_partial(int n, void * p) const
    {
        __declspec(align(64)) int p0[16] = {12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12}; 
        Vec16i vec0 = Vec16i(74);
        vec0.store_partial(5, (void*)p0);
        CHECK_CONDITION((p0[4] == 74 && p0[5] == 12) , "void store_partial(int n, void * p) const  1");
    }

    // Vec16i & cutoff(int n)
    {
        Vec16i vec0 = Vec16i(908243);
        vec0.cutoff(12);
        CHECK_CONDITION((test_element((void*)&vec0, VECTYPE_16I, 908243, 11) == 0) , "Vec16i & cutoff(int n)  1");
        CHECK_CONDITION((test_element((void*)&vec0, VECTYPE_16I, 0, 12) == 0) , "Vec16i & cutoff(int n)  2");
    }

    // Vec16i const & insert(uint32_t index, int32_t value)
    {
        Vec16i vec0 = Vec16i(-8529);
        vec0.insert(10, 1819203);
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16I, -8529) != 0) , "Vec16i const & insert(uint32_t index, int32_t value) 1");
        CHECK_CONDITION((test_element((void*)&vec0, VECTYPE_16I, 1819203, 10) == 0) , "Vec16i const & insert(uint32_t index, int32_t value) 2");
    }
    
    // int32_t extract(uint32_t index) const
    // int32_t operator [] (uint32_t index) const
    // Tested implicitly by all calls to test_element and test_all_elements
    
    // static inline Vec16i operator + (Vec16i const & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(74);
        Vec16i vec1 = Vec16i(26);
        Vec16i vec2 = vec0 + vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16I, 100) == 0) , "static inline Vec16i operator + (Vec16i const & a, Vec16i const & b) 1");
    }
    
    // static inline Vec16i operator += (Vec16i const & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(74);
        Vec16i vec1 = Vec16i(-45);
        vec1 += vec0;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_16I, 29) == 0) , "static inline Vec16i operator += (Vec16i const & a, Vec16i const & b) 1");
    }
    
    // postfix operator ++
    // static inline Vec16i operator ++ (Vec16i & a, int)
    {
        Vec16i vec0 = Vec16i(89);
        vec0++;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16I, 90) == 0) , "static inline Vec16i operator ++ (Vec16i & a, int) 1");
    }
    
    // prefix operator ++
    // static inline Vec16i & operator ++ (Vec16i & a)
    {
        Vec16i vec0 = Vec16i(89);
        ++vec0;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16I, 90) == 0) , "static inline Vec16i & operator ++ (Vec16i & a) 1");
    }
    

    // static inline Vec16i operator - (Vec16i const & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(83);
        Vec16i vec1 = Vec16i(-1232);
        Vec16i vec2 = vec0 - vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16I, 1315) == 0) , "static inline Vec16i operator - (Vec16i const & a, Vec16i const & b) 1");
    }
      
    // static inline Vec16i operator - (Vec16i const & a)
    {
        Vec16i vec0 = Vec16i(72);
        Vec16i vec1 = -vec0;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_16I, -72) == 0) , "static inline Vec16i operator - (Vec16i const & a) 1");
    }
    
    // static inline Vec16i & operator -= (Vec16i & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(96);
        Vec16i vec1 = Vec16i(84);
        vec1 -= vec0;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_16I, -12) == 0) , "static inline Vec16i & operator -= (Vec16i & a, Vec16i const & b) 1");
    }
    
    // postfix operator --
    // static inline Vec16i operator -- (Vec16i & a, int)
    {
        Vec16i vec0 = Vec16i(58);
        vec0--;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16I, 57) == 0) , "static inline Vec16i operator -- (Vec16i & a, int) 1");
    }

    // prefix operator --
    // static inline Vec16i & operator -- (Vec16i & a)
    {
        Vec16i vec0 = Vec16i(58);
        vec0--;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16I, 57) == 0) , "static inline Vec16i operator -- (Vec16i & a, int) 1");
    }
    
    // static inline Vec16i operator * (Vec16i const & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(345);
        Vec16i vec1 = Vec16i(-567);
        Vec16i vec2 = vec0 * vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16I, -195615) == 0) , "static inline Vec16i operator * (Vec16i const & a, Vec16i const & b) 1");
    }
    
    // static inline Vec16i & operator *= (Vec16i & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(698);
        Vec16i vec1 = Vec16i(12);
        vec1 *= vec0;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_16I, 8376) == 0) , "static inline Vec16i & operator *= (Vec16i & a, Vec16i const & b) 1");
    }

    // static inline Vec16i operator << (Vec16i const & a, int32_t b)
    {
        Vec16i vec0 = Vec16i(623);
        Vec16i vec1 = vec0 << 2;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_16I, 2492) == 0) , "static inline Vec16i operator << (Vec16i const & a, int32_t b) 1");
    }

    // static inline Vec16i & operator <<= (Vec16i & a, int32_t b)
    {
        Vec16i vec0 = Vec16i(623);
        vec0 <<= 3;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16I, 4984) == 0) , "static inline Vec16i & operator <<= (Vec16i & a, int32_t b) 1");
    }

    // static inline Vec16i operator >> (Vec16i const & a, int32_t b)
    {
        Vec16i vec0 = Vec16i(289244);
        Vec16i vec1 = vec0 >> 4;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_16I, 18077) == 0) , "static inline Vec16i operator >> (Vec16i const & a, int32_t b) 1");
    }
    
    // static inline Vec16i & operator >>= (Vec16i & a, int32_t b)
    {
        Vec16i vec0 = Vec16i(289244);
        vec0 >>= 3;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16I, 36155) == 0) , "static inline Vec16i & operator >>= (Vec16i & a, int32_t b) 1");
    }
    
    // static inline Vec16b operator == (Vec16i const & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(890);
        Vec16i vec1 = Vec16i(900);
        Vec16i vec2 = Vec16i(890);
        Vec16b vec3 = (vec0 == vec1);
        Vec16b vec4 = (vec0 == vec2);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16B, 0) == 0) , "static inline Vec16b operator == (Vec16i const & a, Vec16i const & b) 1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_16B, 1) == 0) , "static inline Vec16b operator == (Vec16i const & a, Vec16i const & b) 2");
    }
    
    // static inline Vec16b operator != (Vec16i const & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(890);
        Vec16i vec1 = Vec16i(900);
        Vec16i vec2 = Vec16i(890);
        Vec16b vec3 = (vec0 != vec1);
        Vec16b vec4 = (vec0 != vec2);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16B, 1) == 0) , "static inline Vec16b operator != (Vec16i const & a, Vec16i const & b) 1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_16B, 0) == 0) , "static inline Vec16b operator != (Vec16i const & a, Vec16i const & b) 2");
    }
    
    // static inline Vec16b operator > (Vec16i const & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(890);
        Vec16i vec1 = Vec16i(900);
        Vec16i vec2 = Vec16i(890);
        Vec16b vec3 = (vec0 > vec1);
        Vec16b vec4 = (vec0 > vec2);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16B, 0) == 0) , "static inline Vec16b operator > (Vec16i const & a, Vec16i const & b) 1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_16B, 0) == 0) , "static inline Vec16b operator > (Vec16i const & a, Vec16i const & b) 2");
    }
    
    // static inline Vec16b operator < (Vec16i const & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(890);
        Vec16i vec1 = Vec16i(900);
        Vec16i vec2 = Vec16i(890);
        Vec16b vec3 = (vec0 < vec1);
        Vec16b vec4 = (vec0 < vec2);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16B, 1) == 0) , "static inline Vec16b operator < (Vec16i const & a, Vec16i const & b) 1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_16B, 0) == 0) , "static inline Vec16b operator < (Vec16i const & a, Vec16i const & b) 2");
    }

    // static inline Vec16b operator >= (Vec16i const & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(32856);
        Vec16i vec1 = Vec16i(1291);
        Vec16b vec2 = vec0 >= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16B, 1) == 0) , "static inline Vec16b operator >= (Vec16i const & a, Vec16i const & b) 1");
    }

    // static inline Vec16b operator <= (Vec16i const & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(32856);
        Vec16i vec1 = Vec16i(1291);
        Vec16b vec2 = vec0 <= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16B, 0) == 0) , "static inline Vec16b operator <= (Vec16i const & a, Vec16i const & b) 1");
    }

    // static inline Vec16i operator & (Vec16i const & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(2197019);
        Vec16i vec1 = Vec16i(932185);
        Vec16i vec2 = vec0 & vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16I, 25) == 0) , "static inline Vec16i operator & (Vec16i const & a, Vec16i const & b) 1");
    }

    // static inline Vec16i operator && (Vec16i const & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(2197019);
        Vec16i vec1 = Vec16i(932185);
        Vec16i vec2 = vec0 && vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16I, 25) == 0) , "static inline Vec16i operator && (Vec16i const & a, Vec16i const & b) 1");
    }

    // static inline Vec16i & operator &= (Vec16i & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(2197019);
        Vec16i vec1 = Vec16i(932185);
        vec0 &= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16I, 25) == 0) , "static inline Vec16i operator &= (Vec16i const & a, Vec16i const & b) 1");
    }

    // static inline Vec16i operator | (Vec16i const & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(12469);
        Vec16i vec1 = Vec16i(102945126);
        Vec16i vec2 = vec0 | vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16I, 102953463) == 0) , "static inline Vec16i operator | (Vec16i const & a, Vec16i const & b) 1");
    }

    // static inline Vec16i operator || (Vec16i const & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(12469);
        Vec16i vec1 = Vec16i(102945126);
        Vec16i vec2 = vec0 || vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16I, 102953463) == 0) , "static inline Vec16i operator || (Vec16i const & a, Vec16i const & b) 1");
    }

    // static inline Vec16i & operator |= (Vec16i & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(2197019);
        Vec16i vec1 = Vec16i(932185);
        vec0 |= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16I, 3129179) == 0) , "static inline Vec16i operator |= (Vec16i const & a, Vec16i const & b) 1");
    }

    // static inline Vec16i operator ^ (Vec16i const & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(12469);
        Vec16i vec1 = Vec16i(102945126);
        Vec16i vec2 = vec0 ^ vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16I, 102949331) == 0) , "static inline Vec16i operator ^ (Vec16i const & a, Vec16i const & b) 1");
    }

    // static inline Vec16i & operator ^= (Vec16i & a, Vec16i const & b)
    {
        Vec16i vec0 = Vec16i(2197019);
        Vec16i vec1 = Vec16i(932185);
        vec0 ^= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16I, 3129154) == 0) , "static inline Vec16i & operator ^= (Vec16i & a, Vec16i const & b) 1");
    }

    // static inline Vec16i operator ~ (Vec16i const & a)
    {
        Vec16i vec0 = Vec16i(-2137642);
        Vec16i vec1 = ~vec0;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_16I, 2137641) == 0) , "static inline Vec16i operator ~ (Vec16i const & a) 1");
    

        Vec16i vec2 = Vec16i(2963);
        Vec16i vec3 = ~vec2;
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16I, -2964) == 0) , "static inline Vec16i operator ~ (Vec16i const & a) 2");
    }

    // static inline Vec16b operator ! (Vec16i const & a)
    {
        Vec16i vec0 = Vec16i(16);
        vec0.cutoff(10);
        //print_all_elements((void*)&vec94, VECTYPE_16I);
        Vec16b vec1 = !vec0;
        //print_all_elements((void*)&vec95, VECTYPE_16B);
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec1, VECTYPE_16B, 0xFC00) == 0) , "static inline Vec16i operator ~ (Vec16i const & a) 2");
    }

    if(g_allSuccess == true)
    {
        printf("Vec16i OK\n");
    }
    else
    {
        printf("Vec16i found %d errors\n", g_failCount);
    }

}

void test_vec16ui_basic()
{
    char header[] = "Vec16ui test";
    INIT_TEST(header);

    // comment this out to see detailed test list
    SUPRESS_MESSAGES();
        
    // Vec16ui()
    {
        Vec16ui vec0 = Vec16ui();
        CHECK_CONDITION((&vec0 != NULL), "Vec16ui() 1");
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_16UI, 0) == 0, "Vec16ui() 2");
    }
    
    // Vec16ui(uint32_t i) 
    {
        Vec16ui vec0 = Vec16ui(18);
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_16UI, 18) == 0, "Vec16ui(uint32_t i)  1");
    }
    
    // Vec16ui(uint32_t i0, uint32_t i1, uint32_t i2,  uint32_t i3,  uint32_t i4,  uint32_t i5,  uint32_t i6,  uint32_t i7,
    //             uint32_t i8, uint32_t i9, uint32_t i10, uint32_t i11, uint32_t i12, uint32_t i13, uint32_t i14, uint32_t i15)
    {
        Vec16ui vec0 = Vec16ui(15, 43, 128, 73, 89, 0, 2, 5, 9, 12, 18, 34, 37, 43, 64, 112);
        uint32_t arr0[16] =   {15, 43, 128, 73, 89, 0, 2, 5, 9, 12, 18, 34, 37, 43, 64, 112};
        CHECK_CONDITION(test_vector_with_array((void*)&vec0, VECTYPE_16UI, (void*)arr0) == 0, "Vec16ui(int32_t i0, int32_t i1, ... 1");
    }


    // Vec16ui(__m512i const & x) 
    {
        __m512i zmm0 = _mm512_set1_epi32(17);
        Vec16ui vec0 = Vec16ui(zmm0);
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_16UI, 17) == 0, "Vec16ui(__m512i const & x) 1");
    }

    // Vec16ui & operator = (__m512i const & x)
    {
        __m512i zmm0 = _mm512_set1_epi32(908);
        Vec16ui vec0 = Vec16ui(12356);
        vec0 = zmm0;
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_16UI, 908) == 0, "Vec16ui & operator = (__m512i const & x) 1");
    }

    // operator __m512i() const
    {
        Vec16ui vec0 = Vec16ui(8892);
        __m512i zmm0  = vec0;
        Vec16ui vec1 = Vec16ui(zmm0);
        CHECK_CONDITION(test_all_elements((void*)&vec1, VECTYPE_16UI, 8892) == 0, "operator __m512i() const  1");
    }


    // Vec16ui & load(void const * p)
    {
        __declspec(align(64)) uint32_t p0[16] = {5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298};
        Vec16ui vec0;
        vec0.load((void*)p0);
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_16UI, 5298) == 0, "Vec16ui & load(void const * p)  1");
    }

    // Vec16ui & load_a(void const * p) 
    {
        __declspec(align(64)) uint32_t p0[16]  = { 987124, 987124, 987124, 987124, 987124, 987124, 987124, 987124, 987124, 987124, 987124, 987124, 987124, 987124, 987124, 987124};
        Vec16ui vec0;
        vec0.load_a((void*)p0);
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_16UI, 987124) == 0, "Vec16ui & load_a(void const * p)  1");
    }

    // Vec16ui & load_partial(int n, void const * p)
    {
        __declspec(align(64)) uint32_t p0[16] =  { 915, 915, 915, 915, 915, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17 };
        Vec16ui vec0 = Vec16ui(8);
        vec0.load_partial(6, (void*)p0);
        // print_all_elements((void*)&vec12, VECTYPE_16UI);
        CHECK_CONDITION(test_element((void*)&vec0, VECTYPE_16UI, 915, 4) == 0, "Vec16ui & load_partial(int n, void const * p)  1");
        CHECK_CONDITION(test_element((void*)&vec0, VECTYPE_16UI, 17, 5) == 0, "Vec16ui & load_partial(int n, void const * p)  2");
        CHECK_CONDITION(test_element((void*)&vec0, VECTYPE_16UI, 0, 6) == 0, "Vec16ui & load_partial(int n, void const * p)  3");
    }
    
    // Vec16ui const & insert(uint32_t index, uint32_t value)
    {
        Vec16ui vec0 = Vec16ui(8529);
        vec0.insert(10, 1819203);
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16UI, 8529) != 0) , "Vec16ui const & insert(uint32_t index, int32_t value) 1");
        CHECK_CONDITION((test_element((void*)&vec0, VECTYPE_16UI, 1819203, 10) == 0) , "Vec16ui const & insert(uint32_t index, int32_t value) 2");
    }
    
    
    // static inline Vec16ui operator + (Vec16ui const & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(74);
        Vec16ui vec1 = Vec16ui(26);
        Vec16ui vec2 = vec0 + vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16UI, 100) == 0) , "static inline Vec16ui operator + (Vec16ui const & a, Vec16ui const & b) 1");
    }
    
    // static inline Vec16ui & operator += (Vec16ui & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(74);
        Vec16ui vec1 = Vec16ui(45);
        vec0 += vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16UI, 119) == 0) , "static inline Vec16ui & operator += (Vec16ui & a, Vec16ui const & b) 1");
    }
    
    // postfix operator ++
    // static inline Vec16ui operator ++ (Vec16ui & a, int)
    {
        Vec16ui vec0 = Vec16ui(89);
        vec0++;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16UI, 90) == 0) , "static inline Vec16ui operator ++ (Vec16ui & a, int) 1");
    }
    
    // prefix operator ++
    // static inline Vec16ui operator ++ (Vec16ui & a, int)
    {
        Vec16ui vec0 = Vec16ui(89);
        ++vec0;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16UI, 90) == 0) , "static inline Vec16ui operator ++ (Vec16ui & a, int) 1");
    }
    
    // static inline Vec16ui operator - (Vec16ui const & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(83);
        Vec16ui vec1 = Vec16ui(1232);
        Vec16ui vec2 = vec0 - vec1;
        // print_all_elements((void*)&vec20, VECTYPE_16UI);
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16UI, 0xFFFFFB83) == 0) , "static inline Vec16ui operator - (Vec16ui const & a, Vec16ui const & b) 1");
    }

    // static inline Vec16ui & operator -= (Vec16ui & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(84);
        Vec16ui vec1 = Vec16ui(96);
        vec1 -= vec0;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_16UI, 12) == 0) , "static inline Vec16ui & operator -= (Vec16ui & a, Vec16ui const & b) 1");
    }
    
    // postfix operator --
    // static inline Vec16ui operator -- (Vec16ui & a, int)
    {
        Vec16i vec0 = Vec16i(58);
        vec0--;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16UI, 57) == 0) , "static inline Vec16ui operator -- (Vec16ui & a, int) 1");
    }

    // prefix operator --
    // static inline Vec16ui & operator -- (Vec16ui & a)
    {
        Vec16ui vec0 = Vec16ui(58);
        vec0--;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16UI, 57) == 0) , "static inline Vec16ui operator -- (Vec16ui & a, int) 1");
    }
    
    // static inline Vec16ui operator * (Vec16ui const & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(345);
        Vec16ui vec1 = Vec16ui(567);
        Vec16ui vec2 = vec0 * vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16UI, 195615) == 0) , "static inline Vec16ui operator * (Vec16ui const & a, Vec16ui const & b) 1");
    }
    
    // static inline Vec16ui & operator *= (Vec16ui & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(698);
        Vec16ui vec1 = Vec16ui(12);
        vec0 *= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16UI, 8376) == 0) , "static inline Vec16ui & operator *= (Vec16ui & a, Vec16ui const & b) 1");
    }
    
    // static inline Vec16i operator >> (Vec16ui const & a, int32_t b)
    {
        Vec16ui vec0 = Vec16ui(289244);
        Vec16ui vec1 = vec0 >> 4;
        //print_all_elements((void*)&vec31, VECTYPE_16UI);
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_16UI, 18077) == 0) , "static inline Vec16ui operator >> (Vec16ui const & a, int32_t b) 1");
    }
    
    // static inline Vec16ui & operator >>= (Vec16ui & a, int32_t b)
    {
        Vec16ui vec0 = Vec16ui(289244);
        vec0 >>= 3;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16UI, 36155) == 0) , "static inline Vec16ui & operator >>= (Vec16ui & a, int32_t b) 1");
    }
    
    // static inline Vec16ui operator << (Vec16ui const & a, int32_t b)
    {
        Vec16ui vec0 = Vec16ui(623);
        Vec16ui vec1 = vec0 << 2;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_16UI, 2492) == 0) , "static inline Vec16ui operator << (Vec16ui const & a, int32_t b) 1");
    }

    // static inline Vec16ui & operator <<= (Vec16ui & a, int32_t b)
    {
        Vec16ui vec0 = Vec16ui(623);
        vec0 <<= 3;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16UI, 4984) == 0) , "static inline Vec16ui & operator <<= (Vec16ui & a, int32_t b) 1");
    }
    
    // static inline Vec16b operator == (Vec16ui const & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(890);
        Vec16ui vec1 = Vec16ui(900);
        Vec16ui vec2 = Vec16ui(890);
        Vec16b vec3 = (vec0 == vec1);
        Vec16b vec4 = (vec0 == vec2);
        //print_all_elements((void*)&vec40, VECTYPE_16B);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16B, 0) == 0) , "static inline Vec16b operator == (Vec16ui const & a, Vec16ui const & b) 1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_16B, 1) == 0) , "static inline Vec16b operator == (Vec16ui const & a, Vec16ui const & b) 2");
    }
    
    // static inline Vec16b operator != (Vec16ui const & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(890);
        Vec16ui vec1 = Vec16ui(900);
        Vec16ui vec2 = Vec16ui(890);
        Vec16b vec3 = (vec0 != vec1);
        Vec16b vec4 = (vec0 != vec2);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16B, 1) == 0) , "static inline Vec16b operator != (Vec16ui const & a, Vec16ui const & b) 1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_16B, 0) == 0) , "static inline Vec16b operator != (Vec16ui const & a, Vec16ui const & b) 2");
    }
    
    // static inline Vec16b operator > (Vec16ui const & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(890);
        Vec16ui vec1 = Vec16ui(900);
        Vec16ui vec2 = Vec16ui(890);
        Vec16b vec3 = (vec0 > vec1);
        Vec16b vec4 = (vec0 > vec2);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16B, 0) == 0) , "static inline Vec16b operator > (Vec16ui const & a, Vec16ui const & b) 1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_16B, 0) == 0) , "static inline Vec16b operator > (Vec16ui const & a, Vec16ui const & b) 2");
    }
    
    // static inline Vec16b operator < (Vec16ui const & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(890);
        Vec16ui vec1 = Vec16ui(900);
        Vec16ui vec2 = Vec16ui(890);
        Vec16b vec3 = (vec0 < vec1);
        Vec16b vec4 = (vec0 < vec2);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16B, 1) == 0) , "static inline Vec16b operator < (Vec16ui const & a, Vec16ui const & b) 1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_16B, 0) == 0) , "static inline Vec16b operator < (Vec16ui const & a, Vec16ui const & b) 2");
    }
    
    // static inline Vec16b operator >= (Vec16ui const & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(32856);
        Vec16ui vec1 = Vec16ui(1291);
        Vec16b vec2 = vec0 >= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16B, 1) == 0) , "static inline Vec16b operator >= (Vec16ui const & a, Vec16ui const & b) 1");
    }
    
    // static inline Vec16b operator <= (Vec16ui const & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(32856);
        Vec16ui vec1 = Vec16ui(1291);
        Vec16b vec2 = vec0 <= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16B, 0) == 0) , "static inline Vec16b operator <= (Vec16ui const & a, Vec16ui const & b) 1");
    }
    
    // static inline Vec16ui operator & (Vec16ui const & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(2197019);
        Vec16ui vec1 = Vec16ui(932185);
        Vec16ui vec2 = vec0 & vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16UI, 25) == 0) , "static inline Vec16ui operator & (Vec16ui const & a, Vec16ui const & b) 1");
    }
    
    // static inline Vec16ui operator && (Vec16ui const & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(2197019);
        Vec16ui vec1 = Vec16ui(932185);
        Vec16ui vec2 = vec0 && vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16UI, 25) == 0) , "static inline Vec16ui operator && (Vec16ui const & a, Vec16ui const & b) 1");
    }
    

    // static inline Vec16ui & opeator &= (Vec16ui & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(2197019);
        Vec16ui vec1 = Vec16ui(932185);
        vec0 &= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16UI, 25) == 0) , "static inline Vec16ui operator &= (Vec16ui const & a, Vec16ui const & b) 1");
    }
    
    // static inline Vec16ui operator | (Vec16ui const & a, Vec16ui const & b)
    {
        Vec16ui vec0= Vec16ui(12469);
        Vec16ui vec1 = Vec16ui(102945126);
        Vec16ui vec2 = vec0 | vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16I, 102953463) == 0) , "static inline Vec16ui operator | (Vec16ui const & a, Vec16ui const & b) 1");
    }
    
    // static inline Vec16ui operator || (Vec16ui const & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(12469);
        Vec16ui vec1 = Vec16ui(102945126);
        Vec16ui vec2 = vec0 || vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16UI, 102953463) == 0) , "static inline Vec16ui operator || (Vec16ui const & a, Vec16ui const & b) 1");
    }
    
    // static inline Vec16ui & operator |= (Vec16ui & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(2197019);
        Vec16ui vec1 = Vec16ui(932185);
        vec0 |= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16UI, 3129179) == 0) , "static inline Vec16ui operator |= (Vec16ui const & a, Vec16ui const & b) 1");
    }
    
    // static inline Vec16ui operator ^ (Vec16ui const & a, Vec16ui const & b)
    {
        Vec16ui vec0 = Vec16ui(12469);
        Vec16ui vec1 = Vec16ui(102945126);
        Vec16ui vec2 = vec1 ^ vec0;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_16UI, 102949331) == 0) , "static inline Vec16ui operator ^ (Vec16ui const & a, Vec16ui const & b) 1");
    }

    // static inline Vec16i & operator ^= (Vec16i & a, Vec16i const & b)
    {
        Vec16ui vec0 = Vec16ui(2197019);
        Vec16ui vec1 = Vec16ui(932185);
        vec0 ^= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_16UI, 3129154) == 0) , "static inline Vec16ui & operator ^= (Vec16ui & a, Vec16ui const & b) 1");
    }

    // static inline Vec16i operator ~ (Vec16i const & a)
    {
        Vec16ui vec0 = Vec16ui(2137642);
        Vec16ui vec1 = ~vec0;
        //print_all_elements((void*)&vec84, VECTYPE_16UI);
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_16UI, 0xFFDF61D5) == 0) , "static inline Vec16ui operator ~ (Vec16ui const & a) 1");

        Vec16ui vec2 = Vec16ui(2963);
        Vec16ui vec3 = ~vec2;
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16UI, 0xFFFFF46C) == 0) , "static inline Vec16ui operator ~ (Vec16ui const & a) 2");
    }

    // static inline Vec16b operator ! (Vec16ui const & a)
    {
        Vec16ui vec0 = Vec16ui(16);
        vec0.cutoff(10);
        Vec16b vec1 = !vec0;
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec1, VECTYPE_16B, 0xFC00) == 0) , "static inline Vec16b operator ! (Vec16ui const & a) 2");
    }
    
    if(g_allSuccess == true)
    {
        printf("Vec16ui OK\n");
    }
    else
    {
        printf("Vec16ui found %d errors\n", g_failCount);
    }
}

void test_vec8q_basic()
{
    char header[] = "Vec8q test";
    INIT_TEST(header);
    
    // comment this out to see detailed test list
    //SUPRESS_MESSAGES();

    // Default constructor:
    // Vec8q()
    {
        Vec8q vec0 = Vec8q();
        CHECK_CONDITION((&vec0 != NULL), "Vec8q() 1");
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_8Q, 0) == 0, "Vec8q() 2");
    }

    // Constructor to broadcast the same value into all elements:
    // Vec8q(uint64_t i)
    {
        Vec8q vec0 = Vec8q(-18);
        //print_all_elements((void *)&vec0, VECTYPE_8Q);
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_8Q, -18) == 0, "Vec8q(uint64_t i) 1");
    }

    // Constructor to build from all elements:
    // Vec8q(int64_t i0, int64_t i1, int64_t i2,  int64_t i3,  int64_t i4,  int64_t i5,  int64_t i6,  int64_t i7)
    {
        Vec8q    vec0   = Vec8q(-15, -43, -128, 0, 2, 5, 9, 12);
        int64_t arr0[8] =      {-15, -43, -128, 0, 2, 5, 9, 12};
        //print_all_elements((void *)&vec0, VECTYPE_16I);
        CHECK_CONDITION(test_vector_with_array((void*)&vec0, VECTYPE_8Q, (void*)arr0) == 0, "Vec8q(int64_t i0, int64_t i1, ... 1");
    }

    // Vec8q(Vec512b const & a)
    {
        Vec512b vec0 = Vec512b(1);
        Vec512b vec1 = Vec512b(0);
        Vec8q vec2 = Vec8q(vec0);
        Vec8q vec3 = Vec8q(vec1);
        // print_all_elements((void *)&vec0, VECTYPE_512B);
        // print_all_elements((void *)&vec1, VECTYPE_8Q);
        CHECK_CONDITION(test_all_elements((void*)&vec2, VECTYPE_8Q, 0xFFFFFFFFFFFFFFFF) == 0, "Vec8q(Vec512b const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec3, VECTYPE_8Q, 0x0000000000000000) == 0, "Vec8q(Vec512b const & a) 2");
    }

    // Constructor to convert from type _m512i used in intrinsics:
    // NOT SUPPORTED BY HW
    // Vec8q(__m512i const & x)
    {
        __m512i zmm0 = _mm512_set1_epi64(17);
        Vec8q vec0 = Vec8q(zmm0);
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_8Q, 17) == 0, "Vec8q(__m512i const & x) 1");
    }

    // Assignment operator to convert from type _m512i used in intrinsics:
    // Vec8q & operator = (__m512i const & x)
    {
        __m512i zmm0 = _mm512_set1_epi64(908);
        Vec8q vec0 = Vec8q(-12356);
        vec0 = zmm0;
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_8Q, 908) == 0, "Vec8q & operator = (__m512i const & x) 1");
    }

    // Type cast operator to convert to _m512i used in intrinsics
    // operator __m512i() const
    {
        Vec8q vec0 = Vec8q(8892);
        __m512i zmm0 = vec0;
        Vec8q vec1 = Vec8q(zmm0);
        CHECK_CONDITION(test_all_elements((void*)&vec1, VECTYPE_8Q, 8892) == 0, "operator __m512i() const  1");
    }
    
    // Member function to load from array (unaligned)
    // Vec8q & load(void const * p)
    {
        int64_t p0[8] = {5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298};
        Vec8q vec0;
        vec0.load((void*)p0);
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_8Q, 5298) == 0, "Vec8q & load(void const * p)  1");
    }

    // Member function to load from array, aligned by 64
    // Vec8q & load_a(void const * p)
    {
        int64_t p0[8] = {5298, 5298, 5298, 5298, 5298, 5298, 5298, 5298};
        Vec8q vec0;
        vec0.load_a((void*)p0);
        CHECK_CONDITION(test_all_elements((void*)&vec0, VECTYPE_8Q, 5298) == 0, "Vec8q & load(void const * p)  1");
    }
    
    // Partial load. Load n elements and set the rest to 0
    // Vec8q & load_partial(int n, void const * p)
    {
        int64_t p0[8] =  { 915, 915, 915, 915, 915, 17, 17, 17};
        Vec8q vec0 = Vec8q(8);
        vec0.load_partial(6, (void*)p0);
        // print_all_elements((void*)&vec0, VECTYPE_8Q);
        CHECK_CONDITION(test_element((void*)&vec0, VECTYPE_8Q, 915, 4) == 0, "Vec8q & load_partial(int n, void const * p)  1");
        CHECK_CONDITION(test_element((void*)&vec0, VECTYPE_8Q, 17, 5) == 0, "Vec8q & load_partial(int n, void const * p) 2");
        CHECK_CONDITION(test_element((void*)&vec0, VECTYPE_8Q, 0, 6) == 0, "Vec8q & load_partial(int n, void const * p) 3");
    }

    // Partial store. Store n elements
    // void store_partial(int64_t n, void * p) const
    {
        int64_t p0[8] = {12, 12, 12, 12, 12, 12, 12, 12}; 
        Vec8q vec0 = Vec8q(74);
        vec0.store_partial(5, (void*)p0);
        CHECK_CONDITION((p0[4] == 74 && p0[5] == 12) , "void store_partial(int64_t n, void * p) const  1");
    }
    
    // cut off vector to n elements. The last 8-n elements are set to zero
    // Vec8q & cutoff(int n)
    {
        Vec8q vec0 = Vec8q(908243);
        vec0.cutoff(5);
        print_all_elements((void*)&vec0, VECTYPE_8Q);
        CHECK_CONDITION((test_element((void*)&vec0, VECTYPE_8Q, 908243, 4) == 0),  "Vec8q & cutoff(int n) 1");
        CHECK_CONDITION((test_element((void*)&vec0, VECTYPE_8Q, 0, 5) == 0),       "Vec8q & cutoff(int n)  2");
    }

    
    // Vec8q const & insert(uint32_t index, int64_t value)
    {
        Vec8q vec0 = Vec8q(-8529);
        vec0.insert(6, 1819203);
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_8Q, -8529) != 0) , "Vec8q const & insert(uint32_t index, int64_t value) 1");
        CHECK_CONDITION((test_element((void*)&vec0, VECTYPE_8Q, 1819203, 6) == 0) , "Vec8q const & insert(uint32_t index, int64_t value) 2");
    }

    // int64_t extract(uint32_t index) const 
    // int64_t operator [] (uint32_t index) const 
        // Tested implicitly by all calls to test_element and test_all_elements
    

// Define operators for Vec8q

    // static inline Vec8q operator + (Vec8q const & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(74);
        Vec8q vec1 = Vec8q(26);
        Vec8q vec2 = vec0 + vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_8Q, 100) == 0) , "static inline Vec8q operator + (Vec8q const & a, Vec8q const & b) 1");
    }

    // static inline Vec8q & operator += (Vec8q & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(74);
        Vec8q vec1 = Vec8q(-45);
        vec1 += vec0;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_8Q, 29) == 0) , "static inline Vec8q & operator += (Vec8q & a, Vec8q const & b) 1");
    }

    // static inline Vec8q operator ++ (Vec8q & a, int)
    {
        Vec8q vec0 = Vec8q(89);
        vec0++;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_8Q, 90) == 0) , "static inline Vec8q operator ++ (Vec8q & a, int) 1");
    }

    // static inline Vec8q & operator ++ (Vec8q & a)
    {
        Vec8q vec0 = Vec8q(89);
        ++vec0;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_8Q, 90) == 0) , "static inline Vec8q & operator ++ (Vec8q & a) 1");
    }

    // static inline Vec8q operator - (Vec8q const & a, Vec8q const & b) 
    {
        Vec8q vec0 = Vec8q(83);
        Vec8q vec1 = Vec8q(-1232);
        Vec8q vec2 = vec0 - vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_8Q, 1315) == 0) , "static inline Vec8q operator - (Vec8q const & a, Vec8q const & b) 1");
    }

    // static inline Vec8q operator - (Vec8q const & a)
    {
        Vec8q vec0 = Vec8q(72);
        Vec8q vec1 = -vec0;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_8Q, -72) == 0) , "static inline Vec8q operator - (Vec8q const & a) 1");
    }

    // static inline Vec8q & operator -= (Vec8q & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(96);
        Vec8q vec1 = Vec8q(84);
        vec1 -= vec0;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_8Q, -12) == 0) , "static inline Vec8q & operator -= (Vec8q & a, Vec8q const & b) 1");
    }

    // static inline Vec8q operator -- (Vec8q & a, int)
    {
        Vec8q vec0 = Vec8q(58);
        vec0--;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_8Q, 57) == 0) , "static inline Vec8q operator -- (Vec8q & a, int) 1");
    }

    // static inline Vec8q & operator -- (Vec8q & a)
    {
        Vec8q vec0 = Vec8q(58);
        vec0--;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_8Q, 57) == 0) , "static inline Vec8q & operator -- (Vec8q & a) 1");
    }

    // static inline Vec8q operator * (Vec8q const & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(345);
        Vec8q vec1 = Vec8q(-567);
        Vec8q vec2 = vec0 * vec1;
        //print_all_elements((void *)&vec1, VECTYPE_8Q);
        //print_all_elements((void *)&vec2, VECTYPE_8Q);
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_8Q, -195615) == 0) , "static inline Vec8q operator * (Vec8q const & a, Vec8q const & b) 1");
    }

    // static inline Vec8q & operator *= (Vec8q & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(698);
        Vec8q vec1 = Vec8q(12);
        vec1 *= vec0;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_8Q, 8376) == 0) , "static inline Vec8q & operator *= (Vec8q & a, Vec8q const & b) 1");
    }
    
    // static inline Vec8q operator << (Vec8q const & a, int32_t b)
    {
        Vec8q vec0 = Vec8q(623);
        Vec8q vec1 = vec0 << 2;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_8Q, 2492) == 0) , "static inline Vec8q operator << (Vec8q const & a, int32_t b) 1");
    }

    // static inline Vec8q & operator <<= (Vec8q & a, int32_t b)
    {
        Vec8q vec0 = Vec8q(623);
        vec0 <<= 3;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_8Q, 4984) == 0) , "static inline Vec8q & operator <<= (Vec8q & a, int32_t b) 1");
    }

    // static inline Vec8q operator >> (Vec8q const & a, int32_t b)
    {
        Vec8q vec0 = Vec8q(289244);
        Vec8q vec1 = vec0 >> 4;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_8Q, 18077) == 0) , "static inline Vec8q operator >> (Vec8q const & a, int32_t b) 1");
    }

    // static inline Vec8q & operator >>= (Vec8q & a, int32_t b)
    {
        Vec8q vec0 = Vec8q(289244);
        vec0 >>= 3;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_8Q, 36155) == 0) , "static inline Vec8q & operator >>= (Vec8q & a, int32_t b) 1");
    }
    
    // static inline Vec8b operator == (Vec8q const & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(890);
        Vec8q vec1 = Vec8q(900);
        Vec8q vec2 = Vec8q(890);
        Vec8b vec3 = (vec0 == vec1);
        Vec8b vec4 = (vec0 == vec2);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_8B, 0) == 0) , "static inline Vec8b operator == (Vec8q const & a, Vec8q const & b) 1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_8B, 1) == 0) , "static inline Vec8b operator == (Vec8q const & a, Vec8q const & b) 2");
    }
    
    // static inline Vec8b operator != (Vec8q const & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(890);
        Vec8q vec1 = Vec8q(900);
        Vec8q vec2 = Vec8q(890);
        Vec8b vec3 = (vec0 != vec1);
        Vec8b vec4 = (vec0 != vec2);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_8B, 1) == 0) , "static inline Vec8b operator != (Vec8q const & a, Vec8q const & b) 1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_8B, 0) == 0) , "static inline Vec8b operator != (Vec8q const & a, Vec8q const & b) 2");
    }

    // static inline Vec8b operator > (Vec8q const & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(890);
        Vec8q vec1 = Vec8q(900);
        Vec8q vec2 = Vec8q(890);
        Vec8b vec3 = (vec0 > vec1);
        Vec8b vec4 = (vec0 > vec2);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_8B, 0) == 0) , "static inline Vec8b operator > (Vec8q const & a, Vec8q const & b) 1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_8B, 0) == 0) , "static inline Vec8b operator > (Vec8q const & a, Vec8q const & b) 2");
    }

    // static inline Vec8b operator < (Vec8q const & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(890);
        Vec8q vec1 = Vec8q(900);
        Vec8q vec2 = Vec8q(890);
        Vec8b vec3 = (vec0 < vec1);
        Vec8b vec4 = (vec0 < vec2);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_8B, 1) == 0) , "static inline Vec8b operator < (Vec8q const & a, Vec8q const & b) 1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_8B, 0) == 0) , "static inline Vec8b operator < (Vec8q const & a, Vec8q const & b) 2");
    }
    
    // static inline Vec8b operator >= (Vec8q const & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(32856);
        Vec8q vec1 = Vec8q(1291);
        Vec8b vec2 = vec0 >= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_8B, 1) == 0) , "static inline Vec8b operator >= (Vec8q const & a, Vec8q const & b) 1");
    }

    // static inline Vec8b operator <= (Vec8q const & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(32856);
        Vec8q vec1 = Vec8q(1291);
        Vec8b vec2 = vec0 <= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_8B, 0) == 0) , "static inline Vec8b operator <= (Vec8q const & a, Vec8q const & b) 1");
    }

    // static inline Vec8q operator & (Vec8q const & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(2197019);
        Vec8q vec1 = Vec8q(932185);
        Vec8q vec2 = vec0 & vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_8Q, 25) == 0) , "static inline Vec8q operator & (Vec8q const & a, Vec8q const & b) 1");
    }

    // static inline Vec8q operator && (Vec8q const & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(2197019);
        Vec8q vec1 = Vec8q(932185);
        Vec8q vec2 = vec0 && vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_8Q, 25) == 0) , "static inline Vec8q operator && (Vec8q const & a, Vec8q const & b) 1");
    }

    // static inline Vec8q & operator &= (Vec8q & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(2197019);
        Vec8q vec1 = Vec8q(932185);
        vec0 &= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_8Q, 25) == 0) , "static inline Vec8q & operator &= (Vec8q & a, Vec8q const & b) 1");
    }

    // static inline Vec8q operator | (Vec8q const & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(12469);
        Vec8q vec1 = Vec8q(102945126);
        Vec8q vec2 = vec0 | vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_8Q, 102953463) == 0) , "static inline Vec8q operator | (Vec8q const & a, Vec8q const & b) 1");
    }

    // static inline Vec8q operator || (Vec8q const & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(12469);
        Vec8q vec1 = Vec8q(102945126);
        Vec8q vec2 = vec0 || vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_8Q, 102953463) == 0) , "static inline Vec8q operator || (Vec8q const & a, Vec8q const & b) 1");
    }

    // static inline Vec8q & operator |= (Vec8q & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(2197019);
        Vec8q vec1 = Vec8q(932185);
        vec0 |= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_8Q, 3129179) == 0) , "static inline Vec8q & operator |= (Vec8q & a, Vec8q const & b) 1");
    }

    // static inline Vec8q operator ^ (Vec8q const & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(12469);
        Vec8q vec1 = Vec8q(102945126);
        Vec8q vec2 = vec0 ^ vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec2, VECTYPE_8Q, 102949331) == 0) , "static inline Vec8q operator ^ (Vec8q const & a, Vec8q const & b) 1");
    }

    // static inline Vec8q & operator ^= (Vec8q & a, Vec8q const & b)
    {
        Vec8q vec0 = Vec8q(2197019);
        Vec8q vec1 = Vec8q(932185);
        vec0 ^= vec1;
        CHECK_CONDITION((test_all_elements((void*)&vec0, VECTYPE_8Q, 3129154) == 0) , "static inline Vec8q & operator ^= (Vec8q & a, Vec8q const & b) 1");
    }

    // static inline Vec8q operator ~ (Vec8q const & a)
    {
        Vec8q vec0 = Vec8q(-2137642);
        Vec8q vec1 = ~vec0;
        CHECK_CONDITION((test_all_elements((void*)&vec1, VECTYPE_8Q, 2137641) == 0) , "static inline Vec8q operator ~ (Vec8q const & a) 1");
    
        Vec8q vec2 = Vec8q(2963);
        Vec8q vec3 = ~vec2;
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_8Q, -2964) == 0) , "static inline Vec8q operator ~ (Vec8q const & a) 2");
    }

    // static inline Vec8b operator ! (Vec8q const & a)
    {
        Vec8q vec0 = Vec8q(16);
        vec0.cutoff(10);
        //print_all_elements((void*)&vec0, VECTYPE_16I);
        Vec8b vec1 = !vec0;
        //print_all_elements((void*)&vec1, VECTYPE_16B);
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec1, VECTYPE_8B, 0xFC00) == 0) , "static inline Vec8b operator ! (Vec8q const & a) 2");
    }
    
    if(g_allSuccess == true)
    {
        printf("Vec8q OK\n");
    }
    else
    {
        printf("Vec8q found %d errors\n", g_failCount);
    }

}

void test_vec16f_basic()
{
    // NOTE: Testing for equality is not a good practice for floating point operations. 
    //       However, basic operations are deterministic and if for some reason results change, the unit tests should be updated with new, correct results.

    char header[] = "Vec16f test";
    INIT_TEST(header);
    
    // comment this out to see detailed test list
    SUPRESS_MESSAGES();

    // Vec16f()
    {
        Vec16f vec0 = Vec16f();
        // print_all_elements((void*)&vec0, VECTYPE_16F);
        CHECK_CONDITION(test_vector_with_float(vec0, 0.0) == 0, "Vec16f() 1");
    }

    // Vec16f(float f) 
    {
        Vec16f vec0 = Vec16f(7.43);
        CHECK_CONDITION(test_vector_with_float(vec0, 7.43) == 0, "Vec16f(float f) 1");
    }

    // Vec16f(float f0, float f1, float f2,  float f3,  float f4,  float f5,  float f6,  float f7,
    //          float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15)
    {
        Vec16f vec0 = Vec16f( 1.01, 1.02, 1.03, 1.04, 1.05, 1.06, 1.07, 1.08, 1.09, 1.10, 1.11, 1.12, 1.13, 1.14, 1.15, 1.16);
        //print_all_elements((void*)&vec0, VECTYPE_16F);
        //CHECK_CONDITION((vec0[6] == 1.07) , "Vec16f(float f0, float f1, float f2 ... "); // NOTE: for some reason this doesn't work...
        CHECK_CONDITION((test_float(vec0[6], 1.07) == 0) , "Vec16f(float f0, float f1, float f2 ... 1");
        CHECK_CONDITION((test_float(vec0[9], 1.10000) == 0) , "Vec16f(float f0, float f1, float f2 ... 2");
    }
    
    // Vec16f(__m512 const & x)
    {
        __m512 vec0 = _mm512_set1_ps(28479.1209);
        Vec16f vec1 = Vec16f(vec0);
        //print_all_elements((void*)&vec1, VECTYPE_16F);
        CHECK_CONDITION( (test_vector_with_float(vec1, 28479.1209)) == 0 , "Vec16f(__m512 const & x) 1");
    }

    // Vec16f & operator = (__m512 const & x)
    {
        __m512 vec0 = _mm512_set1_ps(28479.1209);
        Vec16f vec1 = vec0;
        CHECK_CONDITION( (test_vector_with_float(vec1, 28479.1209)) == 0 , "Vec16f & operator = (__m512 const & x) 1");
    }

    // operator __m512() const
    {
        Vec16f vec0 = Vec16f(32498.123);
        __m512 vec1 = vec0;
        Vec16f vec2 = vec1;
        CHECK_CONDITION( (test_vector_with_float(vec2, 32498.123)) == 0 , "Vec16f operator __m512() const 1");
    }

    // Vec16f & load(float const * p)
    {
        __declspec(align(64)) float p0[16] = {1.1, 1.12, 1.123, 1.1234, 2.1, 2.12, 2.123, 2.1234, 3.1, 3.12, 3.123, 3.1234, 4.1, 4.12, 4.123, 4.1234};
        Vec16f vec0 = Vec16f();
        vec0.load(p0);
        //print_all_elements((void*)&vec0, VECTYPE_16F);
        CHECK_CONDITION( (test_float(vec0[3], 1.1234) == 0), "Vec16f & load(float const * p) 1");
    }

    // Vec16f & load_a(float const * p)
    {
        __declspec(align(64)) float p0[16] = {1.1, 1.12, 1.123, 1.1234, 2.1, 2.12, 2.123, 2.1234, 3.1, 3.12, 3.123, 3.1234, 4.1, 4.12, 4.123, 4.1234};
        Vec16f vec0 = Vec16f();
        vec0.load_a(p0);
        CHECK_CONDITION( (test_float(vec0[15], 4.1234) == 0), "Vec16f & load_a(float const * p) 1");
    }

    // void store(float * p) const
    {
        __declspec(align(64)) float p0[16] = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6};
        Vec16f vec0 = Vec16f(9999.9999);
        vec0.store(p0);
        CHECK_CONDITION( (test_float(p0[8], 9999.9999) == 0), "void store(float * p) const 1");
    }

    // void store_a(float * p) const 
    {
        __declspec(align(64)) float p0[16] = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6};
        Vec16f vec0 = Vec16f(9999.9999);
        vec0.store_a(p0);
        CHECK_CONDITION( (test_float(p0[8], 9999.9999) == 0), "void store_a(float * p) const 1");
    }

    // Vec16f & load_partial(int n, float const * p) 
    {
        __declspec(align(64)) float p0[16] = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6};
        Vec16f vec0 = Vec16f(7777.777);
        vec0.load_partial(7, p0);
        // print_all_elements((void*)&vec0, VECTYPE_16F);
        CHECK_CONDITION( (test_float(vec0[6], 0.7) == 0), "Vec16f & load_partial(int n, float const * p) 1");
        CHECK_CONDITION( (test_float(vec0[7], 0.0) == 0), "Vec16f & load_partial(int n, float const * p) 2");
    }
   
    // void store_partial(int n, float * p) const 
    {
        __declspec(align(64)) float p0[16] = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6};
        Vec16f vec0 = Vec16f(7777.777);
        vec0.store_partial(11, p0);
        //print_all_elements((void*)&vec0, VECTYPE_16F);
        //for(int i = 0; i < 16; i++)printf("p0[%d] = %f\n", i, p5[i]);
        CHECK_CONDITION( (test_float(p0[10], 7777.777) == 0), "void store_partial(int n, float * p) const  1");
        CHECK_CONDITION( (test_float(p0[11], 1.2) == 0), "void store_partial(int n, float * p) const  2");
    }

    // Vec16f & cutoff(int n) 
    {
        Vec16f vec0 = Vec16f(124983.32411);
        CHECK_CONDITION( (test_float(vec0[12], 124983.32411) == 0), "Vec16f & cutoff(int n) 1");
        vec0.cutoff(12);
        //print_all_elements((void*)&vec0, VECTYPE_16F);
        CHECK_CONDITION( (test_float(vec0[11], 124983.32411) == 0), "Vec16f & cutoff(int n) 2");
        CHECK_CONDITION( (test_float(vec0[12], 0.0) == 0), "Vec16f & cutoff(int n) 3");
    }

    // Vec16f const & insert(uint32_t index, float value) 
    {
        Vec16f vec0 = Vec16f(2134.0);
        for(int i = 0; i < 16; i++) vec0.insert(i, 9876.9);
        CHECK_CONDITION( (test_vector_with_float(vec0, 9876.9)) == 0 , "Vec16f const & insert(uint32_t index, float value) 1");
    }

    // These are checked implicitly by all other tests
    // float extract(uint32_t index) const
    // float operator [] (uint32_t index) const 
    
    // static inline Vec16f operator + (Vec16f const & a, Vec16f const & b)
    {
        Vec16f vec0 = Vec16f(1234.56789);
        Vec16f vec1 = Vec16f(9876.54320);
        Vec16f vec2 = vec0 + vec1;
        //print_all_elements((void*)&vec2, VECTYPE_16F);
        CHECK_CONDITION( (test_vector_with_float(vec2, 11111.11109)) == 0 , "static inline Vec16f operator + (Vec16f const & a, Vec16f const & b) 1");
    }

    // static inline Vec16f operator + (Vec16f const & a, float b)
    {
        Vec16f vec0 = Vec16f(1234.56789);
        Vec16f vec1 = vec0 + 777.9;
        CHECK_CONDITION( (test_vector_with_float(vec1, 2012.46789)) == 0 , "static inline Vec16f operator + (Vec16f const & a, float b) 1");
    }

    // static inline Vec16f operator + (float a, Vec16f const & b)
    {
        Vec16f vec2 = Vec16f(1234.56789);
        Vec16f vec3 = 777.9 + vec2;
        CHECK_CONDITION( (test_vector_with_float(vec3, 2012.46789)) == 0 , "static inline Vec16f operator + (float a, Vec16f const & b) 1");
    }

    // static inline Vec16f & operator += (Vec16f & a, Vec16f const & b)
    {
        Vec16f vec0 = Vec16f(12499.12312);
        Vec16f vec1 = 213.1;
        vec0 += vec1;
        CHECK_CONDITION( (test_vector_with_float(vec0, 12712.22312)) == 0 , "static inline Vec16f & operator += (Vec16f & a, Vec16f const & b) 1");
    }

    // static inline Vec16f operator ++ (Vec16f & a, int)
    {
        Vec16f vec0 = 12487.1231;
        vec0++;
        CHECK_CONDITION( (test_vector_with_float(vec0, 12488.1231)) == 0 , "static inline Vec16f operator ++ (Vec16f & a, int) 1");
    }
    
    // static inline Vec16f & operator ++ (Vec16f & a)
    {
        Vec16f vec0 = 12487.1231;
        ++vec0;
        CHECK_CONDITION( (test_vector_with_float(vec0, 12488.1231)) == 0 , "static inline Vec16f & operator ++ (Vec16f & a) 1");
    }

    // static inline Vec16f operator - (Vec16f const & a, Vec16f const & b)
    {
        Vec16f vec0 = 12487.1231;
        Vec16f vec1 = 12087.1231;
        Vec16f vec2 = vec0 - vec1;
        CHECK_CONDITION( (test_vector_with_float(vec2, 400.0)) == 0 , "static inline Vec16f operator - (Vec16f const & a, Vec16f const & b) 1");
    }

    // static inline Vec16f operator - (Vec16f const & a, float b)
    {
        Vec16f vec0 = 12487.1231;
        float p0 = 12087.1231;
        Vec16f vec1 = vec0 - p0;
        CHECK_CONDITION( (test_vector_with_float(vec1, 400.0)) == 0 , "static inline Vec16f operator - (Vec16f const & a, float b) 1");
    }

    // static inline Vec16f operator - (float a, Vec16f const & b)
    {
        Vec16f vec0 = 12087.1231;
        float p0 = 12487.1231;
        Vec16f vec1 = p0 - vec0;
        CHECK_CONDITION( (test_vector_with_float(vec1, 400.0)) == 0 , "static inline Vec16f operator - (float a, Vec16f const & b) 1");
    }

    // static inline Vec16f operator - (Vec16f const & a)
    {
        Vec16f vec0 = 12087.1231;
        Vec16f vec1 = -vec0;
        CHECK_CONDITION( (test_vector_with_float(vec1, -12087.1231)) == 0 , "static inline Vec16f operator - (Vec16f const & a) 1");
    }

    // static inline Vec16f & operator -= (Vec16f & a, Vec16f const & b)
    {
        Vec16f vec0 = Vec16f(12499.12312);
        Vec16f vec1 = 213.1;
        vec1 -= vec0;
        CHECK_CONDITION( (test_vector_with_float(vec1, -12286.02312)) == 0 , "static inline Vec16f & operator -= (Vec16f & a, Vec16f const & b) 1");
    }
    
    // static inline Vec16f operator -- (Vec16f & a, int)
    {
        Vec16f vec0 = 12487.1231;
        vec0--;
        CHECK_CONDITION( (test_vector_with_float(vec0, 12486.1231)) == 0 , "static inline Vec16f operator -- (Vec16f & a, int) 1");
    }
    
    // static inline Vec16f & operator -- (Vec16f & a)
    {
        Vec16f vec0 = 12487.1231;
        --vec0;
        CHECK_CONDITION( (test_vector_with_float(vec0, 12486.1231)) == 0 , "static inline Vec16f & operator -- (Vec16f & a) 1");
    }

    // static inline Vec16f operator * (Vec16f const & a, Vec16f const & b)
    {
        Vec16f vec0 = 93218.532;
        Vec16f vec1 = 0.124;
        Vec16f vec2 = vec0 * vec1;
        CHECK_CONDITION( (test_vector_with_float(vec2, 11559.097968)) == 0 , "static inline Vec16f operator * (Vec16f const & a, Vec16f const & b) 1");
    }

    // static inline Vec16f operator * (Vec16f const & a, float b)
    {
        Vec16f vec0 = 93218.532;
        float p0 = 0.124;
        Vec16f vec1 = vec0 * p0;
        CHECK_CONDITION( (test_vector_with_float(vec1, 11559.097968)) == 0 , "static inline Vec16f operator * (Vec16f const & a, Vec16f const & b) 1");
    }

    // static inline Vec16f operator * (float a, Vec16f const & b)
    {
        Vec16f vec0 = 93218.532;
        float p0 = 0.124;
        Vec16f vec1 = p0 * vec0;
        CHECK_CONDITION( (test_vector_with_float(vec1, 11559.097968)) == 0 , "static inline Vec16f operator * (float a, Vec16f const & b) 1");
    }

    // static inline Vec16f & operator *= (Vec16f & a, Vec16f const & b)
    {
        Vec16f vec0 = 93218.532;
        Vec16f vec1 = -8124.1243;
        vec1 *= vec0;
        // print_all_elements((void*)&vec1, VECTYPE_16F);
        // NOTE: exact value is: -757318941.0315276. I think there might be a problem with promotion to double and back.
        CHECK_CONDITION( (test_vector_with_float(vec1, -757318976.0000000)) == 0 , "static inline Vec16f & operator *= (Vec16f & a, Vec16f const & b) 1");
    }

    // static inline Vec16f operator / (Vec16f const & a, Vec16f const & b)
    {
        Vec16f vec0 = 12420.1231;
        Vec16f vec1 = 12312.56;
        Vec16f vec2 = vec0 / vec1;
        //print_all_elements((void*)&vec2, VECTYPE_16F);
        // NOTE: exact value is 1.0087360467685030570409403081081
        CHECK_CONDITION( (test_vector_with_float(vec2, 1.0087361)) == 0 , "static inline Vec16f operator / (Vec16f const & a, Vec16f const & b) 1");
    }

    // static inline Vec16f operator / (Vec16f const & a, float b)
    {
        Vec16f vec0 = 12420.1231;
        float p0 = 12312.56;
        Vec16f vec2 = vec0 / p0;
        //print_all_elements((void*)&vec2, VECTYPE_16F);
        CHECK_CONDITION( (test_vector_with_float(vec2, 1.0087361)) == 0 , "static inline Vec16f operator / (Vec16f const & a, Vec16f const & b) 1");
    }

    // static inline Vec16f operator / (float a, Vec16f const & b)
    {
        float p0 = 12420.1231;
        Vec16f vec0 = 12312.56;
        Vec16f vec2 = p0 / vec0;
        //print_all_elements((void*)&vec2, VECTYPE_16F);
        CHECK_CONDITION( (test_vector_with_float(vec2, 1.0087361)) == 0 , "static inline Vec16f operator / (float a, Vec16f const & b) 1");
    }

    // static inline Vec16f & operator /= (Vec16f & a, Vec16f const & b)
    {
        Vec16f vec0 = 12420.1231;
        Vec16f vec1 = 12312.56;
        vec0 /= vec1;
        //print_all_elements((void*)&vec0, VECTYPE_16F);
        // NOTE: exact value is 1.0087360467685030570409403081081
        CHECK_CONDITION( (test_vector_with_float(vec0, 1.0087361)) == 0 , "static inline Vec16f & operator /= (Vec16f & a, Vec16f const & b) 1");
    }

    // static inline Vec16b operator == (Vec16f const & a, Vec16f const & b) 
    {
        Vec16f vec0 = 123456.000;
        Vec16f vec1 = 123456.00000001;
        Vec16f vec2 = -82.1;
        Vec16b vec3 = (vec0 == vec1);
        Vec16b vec4 = (vec1 == vec2);
        // print_all_elements((void*)&vec2, VECTYPE_16B);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16B, 1) == 0), "static inline Vec16b operator == (Vec16f const & a, Vec16f const & b)  1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_16B, 0) == 0), "static inline Vec16b operator == (Vec16f const & a, Vec16f const & b)  1");
    }

    // static inline Vec16b operator != (Vec16f const & a, Vec16f const & b)
    {
        Vec16f vec0 = 17257.1284;
        Vec16f vec1 = 17257.1284;
        Vec16f vec2 = 219847.128;
        Vec16b vec3 = (vec0 != vec1);
        Vec16b vec4 = (vec0 != vec2);
        // print_all_elements((void*)&vec2, VECTYPE_16B);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16B, 0) == 0), "static inline Vec16b operator != (Vec16f const & a, Vec16f const & b)  1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_16B, 1) == 0), "static inline Vec16b operator != (Vec16f const & a, Vec16f const & b)  2");
    }

    // static inline Vec16b operator < (Vec16f const & a, Vec16f const & b)
    {
        Vec16f vec0 = 17257.1284;
        Vec16f vec1 = 17257.1284;
        Vec16f vec2 = 219847.128;
        Vec16b vec3 = (vec0 < vec1);
        Vec16b vec4 = (vec0 < vec2);
        // print_all_elements((void*)&vec2, VECTYPE_16B);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16B, 0) == 0), "static inline Vec16b operator < (Vec16f const & a, Vec16f const & b)  1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_16B, 1) == 0), "static inline Vec16b operator < (Vec16f const & a, Vec16f const & b)  2");
    }

    // static inline Vec16b operator <= (Vec16f const & a, Vec16f const & b)
    {
        Vec16f vec0 = 17257.1284;
        Vec16f vec1 = 17257.1284;
        Vec16f vec2 = 219847.128;
        Vec16b vec3 = (vec0 <= vec1);
        Vec16b vec4 = (vec0 <= vec2);
        // print_all_elements((void*)&vec2, VECTYPE_16B);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16B, 1) == 0), "static inline Vec16b operator <= (Vec16f const & a, Vec16f const & b)  1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_16B, 1) == 0), "static inline Vec16b operator <= (Vec16f const & a, Vec16f const & b)  2");
    }

    // static inline Vec16b operator > (Vec16f const & a, Vec16f const & b)
    {
        Vec16f vec0 = 17257.1284;
        Vec16f vec1 = 17257.1284;
        Vec16f vec2 = 219847.128;
        Vec16b vec3 = (vec0 > vec1);
        Vec16b vec4 = (vec0 > vec2);
        // print_all_elements((void*)&vec2, VECTYPE_16B);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16B, 0) == 0), "static inline Vec16b operator > (Vec16f const & a, Vec16f const & b)  1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_16B, 0) == 0), "static inline Vec16b operator > (Vec16f const & a, Vec16f const & b)  2");
    }

    // static inline Vec16b operator >= (Vec16f const & a, Vec16f const & b)
    {
        Vec16f vec0 = 17257.1284;
        Vec16f vec1 = 17257.1284;
        Vec16f vec2 = 219847.128;
        Vec16b vec3 = (vec0 >= vec1);
        Vec16b vec4 = (vec0 >= vec2);
        // print_all_elements((void*)&vec2, VECTYPE_16B);
        CHECK_CONDITION((test_all_elements((void*)&vec3, VECTYPE_16B, 1) == 0), "static inline Vec16b operator >= (Vec16f const & a, Vec16f const & b)  1");
        CHECK_CONDITION((test_all_elements((void*)&vec4, VECTYPE_16B, 0) == 0), "static inline Vec16b operator >= (Vec16f const & a, Vec16f const & b)  2");
    }

    // static inline Vec16f operator & (Vec16f const & a, Vec16f const & b)
    {
        Vec16f vec0 = 1293.1288;
        Vec16f vec1 = 152794.1890;
        Vec16f vec2 = vec0 & vec1;
        CHECK_CONDITION((test_vector_with_float(vec2, 2.0178251) == 0), "static inline Vec16f operator & (Vec16f const & a, Vec16f const & b)  1");
    }
    
    // static inline Vec16f & operator &= (Vec16f & a, Vec16f const & b)
    {
        Vec16f vec0 = 1293.1288;
        Vec16f vec1 = 152794.1890;
        vec1 &= vec0;
        CHECK_CONDITION((test_vector_with_float(vec1, 2.0178251) == 0), "static inline Vec16f & operator &= (Vec16f const & a, Vec16f const & b)  1");
    }

    // static inline Vec16f operator & (Vec16f const & a, Vec16b const & b)
    {
        Vec16f vec0 = 1293.1288;
        Vec16b vec1 = 0xAF8B;
        Vec16f vec2 = vec1 & vec0;
        
        CHECK_CONDITION((test_float(vec2[15], 1293.1288) == 0), "static inline Vec16f operator & (Vec16f const & a, Vec16b const & b)  1");
        CHECK_CONDITION((test_float(vec2[14], 0.0) == 0), "static inline Vec16f operator & (Vec16f const & a, Vec16b const & b) 2");
    }
    
    // static inline Vec16f operator & (Vec16b const & a, Vec16f const & b)
    {
        Vec16f vec0 = 1293.1288;
        Vec16b vec1 = 0xAF8B;
        Vec16f vec2 = vec0 & vec1;
        
        CHECK_CONDITION((test_float(vec2[15], 1293.1288) == 0), "static inline Vec16f operator & (Vec16b const & a, Vec16f const & b) 1");
        CHECK_CONDITION((test_float(vec2[14], 0.0) == 0), "static inline Vec16f operator & (Vec16b const & a, Vec16f const & b) 2");
    }

    // static inline Vec16f operator | (Vec16f const & a, Vec16f const & b)
    {
        Vec16f vec0 = 1293.1288;
        Vec16f vec1 = 152794.1890;
        Vec16f vec2 = vec0 | vec1;
        CHECK_CONDITION((test_vector_with_float(vec2, 9.5270136E7) == 0), "static inline Vec16f operator | (Vec16f const & a, Vec16f const & b) 1");
    }

    // static inline Vec16f & operator |= (Vec16f & a, Vec16f const & b) 
    {
        Vec16f vec0 = 1293.1288;
        Vec16f vec1 = 152794.1890;
        vec1 |= vec0;
        CHECK_CONDITION((test_vector_with_float(vec1, 9.5270136E7) == 0), "static inline Vec16f & operator |= (Vec16f & a, Vec16f const & b) 1");
    }

    // static inline Vec16f operator ^ (Vec16f const & a, Vec16f const & b)
    {
        Vec16f vec0 = 349685.8721;
        Vec16f vec1 = 9532.9436;
        Vec16f vec2 = vec0 ^ vec1;
        CHECK_CONDITION((test_vector_with_float(vec2, 4.6913223E-30) == 0), "static inline Vec16f operator ^ (Vec16f const & a, Vec16f const & b) 1");
    }

    // static inline Vec16f & operator ^= (Vec16f & a, Vec16f const & b
    {
        Vec16f vec0 = 349685.8721;
        Vec16f vec1 = 9532.9436;
        vec1 ^= vec0;
        // print_all_elements((void*)&vec1, VECTYPE_16F);
        CHECK_CONDITION((test_vector_with_float(vec1, 4.6913223E-30) == 0), "static inline Vec16f operator ^ (Vec16f const & a, Vec16f const & b) 1");
    }

    // static inline Vec16b operator ! (Vec16f const & a)
    {
        Vec16f vec0 = -72894.12;
        vec0.insert(10, 0.0);
        vec0.insert(11, 0.0);
        Vec16b vec1 = !vec0;
        // print_all_elements((void*)&vec1, VECTYPE_16B);
        CHECK_CONDITION((test_vector_with_pattern((void*)&vec1, VECTYPE_16B, 0x0C00) == 0), "static inline Vec16f operator ^ (Vec16f const & a, Vec16f const & b) 1");
    }

    if(g_allSuccess == true)
    {
        printf("Vec16f OK\n");
    }
    else
    {
        printf("Vec16f found %d errors\n", g_failCount);
    }
}

void test_vec8d_basic()
{
    // NOTE: Testing for equality is not a good practice for floating point operations. 
    //       However, basic operations are deterministic and if for some reason results change, the unit tests should be updated with new, correct results.
    char header[] = "Vec8d test";
    INIT_TEST(header);
    
    // comment this out to see detailed test list
    SUPRESS_MESSAGES();

    // Vec8d()
    {
        Vec8d vec0 = Vec8d();
        // print_all_elements((void*)&vec0, VECTYPE_8D);
        CHECK_CONDITION(test_vector_with_double(vec0, 0.0) == 0, "Vec8d() 1");
    }
    
    // Vec8d(double d) 
    {
        Vec8d vec0 = Vec8d(3.14159265359);
        CHECK_CONDITION(test_vector_with_double(vec0, 3.14159265359) == 0, "Vec8d(double d) 1");
    }

    // Vec8d(double d0, double d1, double d2, double d3, double d4, double d5, double d6, double d7)
    {
        Vec8d vec0 = Vec8d(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8);
        CHECK_CONDITION(test_double(vec0[5], 0.6) == 0, "Vec8d(double d0, double d1, ...) 1");
        CHECK_CONDITION(test_double(vec0[7], 0.8) == 0, "Vec8d(double d0, double d1, ...) 2");
    }

    // Vec8d(__m512d const & x) 
    {
        __m512d zmm0 = _mm512_set1_pd(3.14159265359);
        Vec8d vec0 = Vec8d(zmm0);
        CHECK_CONDITION(test_vector_with_double(vec0, 3.14159265359) == 0, "Vec8d(__m512d const & x) 1");
    }

    // Vec8d & operator = (__m512d const & x) 
    {
        __m512d zmm0 = _mm512_set1_pd(3.14159265359);
        Vec8d vec0 = zmm0;
        CHECK_CONDITION(test_vector_with_double(vec0, 3.14159265359) == 0, "Vec8d & operator = (__m512d const & x) 1");
    }

    // operator __m512d() const 
    {
        Vec8d vec0 = 3.14159265359;
        __m512d zmm0 = vec0;
        Vec8d vec1 = zmm0;
        CHECK_CONDITION(test_vector_with_double(vec1, 3.14159265359) == 0, "operator __m512d() const 1");
    }

    // operator Vec8uq() const 
    // TODO:

    // Vec8d & load(double const * p) 
    {
        __declspec(align(64)) double p[8] = { 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7 };
        Vec8d vec0;
        vec0.load(p);
        CHECK_CONDITION(test_double(vec0[3], 0.4) == 0, "Vec8d & load(double const * p) 1")
    }

    // Vec8d & load_a(double const * p)
    {
        __declspec(align(64)) double p[8] = { 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7 };
        Vec8d vec0;
        vec0.load_a(p);
        CHECK_CONDITION(test_double(vec0[3], 0.4) == 0, "Vec8d & load_a(double const * p) 1")
    }

    // void store(double * p) const 
    {
        __declspec(align(64)) double p[8] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        Vec8d vec0 = Vec8d(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8);
        vec0.store(p);
        CHECK_CONDITION(test_double(p[3], 0.4) == 0, "void store(double * p) const 1")
    }

    // void store_a(double * p) const
    {
        __declspec(align(64)) double p[8] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        Vec8d vec0 = Vec8d(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8);
        vec0.store_a(p);
        CHECK_CONDITION(test_double(p[3], 0.4) == 0, "void store(double * p) const 1")
    }

    // Vec8d & load_partial(int n, double const * p)
    {
        __declspec(align(64)) double p[8] = { 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7 };
        Vec8d vec0;
        vec0.load_partial(4, p);
        CHECK_CONDITION(test_double(vec0[3], 0.4) == 0, "Vec8d & load_partial(int n, double const * p) 1")
        CHECK_CONDITION(test_double(vec0[4], 0.0) == 0, "Vec8d & load_partial(int n, double const * p) 2")
    }

    // void store_partial(int n, double * p) const 
    {
        __declspec(align(64)) double p[8] = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7};
        Vec8d vec0 = Vec8d(9.0, 8.0, 7.0, 6.0, 5.0, 4.0, 3.0, 2.0);
        vec0.store_partial(7, p);
        CHECK_CONDITION(test_double(p[3], 6.0) == 0, "void store_partial(int n, double * p) const 1")
        CHECK_CONDITION(test_double(p[7], 0.0) == 0, "void store_partial(int n, double * p) const 2")

    }

    // Vec8d & cutoff(int n) 
    {
        Vec8d vec0 = Vec8d(9.0, 8.0, 7.0, 6.0, 5.0, 4.0, 3.0, 2.0);
        vec0.cutoff(7);
        //print_all_elements((void*)&vec0, VECTYPE_8D);
        CHECK_CONDITION(test_double(vec0[3], 6.0) == 0, "Vec8d & cutoff(int n)  1")
        CHECK_CONDITION(test_double(vec0[7], 0.0) == 0, "Vec8d & cutoff(int n)  2")
    }

    // Vec8d const & insert(uint32_t index, double value)
    {
        Vec8d vec0 = Vec8d(17.14);
        vec0.insert(5, 3.14);
        CHECK_CONDITION(test_double(vec0[5], 3.14) == 0, "Vec8d const & insert(uint32_t index, double value) 1")
        CHECK_CONDITION(test_double(vec0[7], 17.14) == 0, "Vec8d const & insert(uint32_t index, double value) 2")
    }

    // Tested implicitly
    // double extract(uint32_t index) const 
    // double operator [] (uint32_t index) const

    // static inline Vec8d operator + (Vec8d const & a, Vec8d const & b) 
    {
        Vec8d vec0 = 174.918;
        Vec8d vec1 = 89120.827;
        Vec8d vec2 = vec0 + vec1;        
        // print_all_elements((void*)&vec2, VECTYPE_8D);
        CHECK_CONDITION(test_vector_with_double(vec2, 8.92957450000000098953023552895E4) == 0, "static inline Vec8d operator + (Vec8d const & a, Vec8d const & b) 1");
    }

    // static inline Vec8d operator + (Vec8d const & a, double b)
    {
        Vec8d vec0 = 174.918;
        double p = 89120.827;
        Vec8d vec2 = vec0 + p;        
        CHECK_CONDITION(test_vector_with_double(vec2, 8.92957450000000098953023552895E4) == 0, "static inline Vec8d operator + (Vec8d const & a, double b) 1");
    }

    // static inline Vec8d operator + (double a, Vec8d const & b)
    {
        Vec8d vec0 = 174.918;
        double p = 89120.827;
        Vec8d vec2 = p + vec0;        
        CHECK_CONDITION(test_vector_with_double(vec2, 8.92957450000000098953023552895E4) == 0, "static inline Vec8d operator + (double a, Vec8d const & b) 1");
    }

    // static inline Vec8d & operator += (Vec8d & a, Vec8d const & b)
    {
        Vec8d vec0 = 174.918;
        Vec8d vec1 = 89120.827;
        vec1 += vec0;
        CHECK_CONDITION(test_vector_with_double(vec1, 8.92957450000000098953023552895E4) == 0, "static inline Vec8d & operator += (Vec8d & a, Vec8d const & b) 1");
    }

    // static inline Vec8d operator ++ (Vec8d & a, int)
    {
        Vec8d vec0 = -174.918;
        vec0++;
        CHECK_CONDITION(test_vector_with_double(vec0, -173.918) == 0, "static inline Vec8d operator ++ (Vec8d & a, int) 1");
    }

    // static inline Vec8d & operator ++ (Vec8d & a)
    {
        Vec8d vec0 = -174.918;
        ++vec0;
        CHECK_CONDITION(test_vector_with_double(vec0, -173.918) == 0, "static inline Vec8d operator ++ (Vec8d & a, int) 1");
    }
    
    // static inline Vec8d operator - (Vec8d const & a, Vec8d const & b) 
    {
        Vec8d vec0 = 174.918;
        Vec8d vec1 = 89120.827;
        Vec8d vec2 = vec0 - vec1;        
        // print_all_elements((void*)&vec2, VECTYPE_8D);
        CHECK_CONDITION(test_vector_with_double(vec2, -8.89459089999999996507540345192E4) == 0, "static inline Vec8d operator - (Vec8d const & a, Vec8d const & b) 1");
    }

    // static inline Vec8d operator - (Vec8d const & a, double b) 
    {
        Vec8d vec0 = 174.918;
        double p = 89120.827;
        Vec8d vec2 = vec0 - p;        
        CHECK_CONDITION(test_vector_with_double(vec2, -8.89459089999999996507540345192E4) == 0, "static inline Vec8d operator - (Vec8d const & a, double b) 1");
    }


    // static inline Vec8d operator - (double a, Vec8d const & b) 
    {
        Vec8d vec0 = 174.918;
        double p = 89120.827;
        Vec8d vec2 = p - vec0;        
        CHECK_CONDITION(test_vector_with_double(vec2, 8.89459089999999996507540345192E4) == 0, "static inline Vec8d operator - (double a, Vec8d const & b) 1");
    }

    // static inline Vec8d operator - (Vec8d const & a) 
    {
        Vec8d vec0 = 174.918;
        Vec8d vec1 = -vec0;        
        CHECK_CONDITION(test_vector_with_double(vec1, -174.918) == 0, "static inline Vec8d operator - (Vec8d const & a) 1");
    }

    // static inline Vec8d & operator -= (Vec8d & a, Vec8d const & b)
    {
        Vec8d vec0 = 174.918;
        Vec8d vec1 = 89120.827;
        vec1 -= vec0;
        CHECK_CONDITION(test_vector_with_double(vec1, 8.89459089999999996507540345192E4) == 0, "static inline Vec8d & operator -= (Vec8d & a, Vec8d const & b) 1");
    }

    // static inline Vec8d operator -- (Vec8d & a, int)
    {
        Vec8d vec0 = -174.918;
        vec0--;
        CHECK_CONDITION(test_vector_with_double(vec0, -175.918) == 0, "static inline Vec8d operator -- (Vec8d & a, int) 1");
    }

    // static inline Vec8d & operator -- (Vec8d & a)
    {
        Vec8d vec0 = -174.918;
        --vec0;
        CHECK_CONDITION(test_vector_with_double(vec0, -175.918) == 0, "static inline Vec8d & operator -- (Vec8d & a) 1");
    }

    // static inline Vec8d operator * (Vec8d const & a, Vec8d const & b)
    {
        Vec8d vec0 = -174.918;
        Vec8d vec1 = 2984.12389;
        Vec8d vec2 = vec0 * vec1;
        CHECK_CONDITION(test_vector_with_double(vec2, -5.21976982591020001564174890518E5) == 0, "static inline Vec8d operator * (Vec8d const & a, Vec8d const & b) 1");
    }

    // static inline Vec8d operator * (Vec8d const & a, double b)
    {
        Vec8d vec0 = -174.918;
        double p = 2984.12389;
        Vec8d vec1 = vec0 * p;
        CHECK_CONDITION(test_vector_with_double(vec1, -5.21976982591020001564174890518E5) == 0, "static inline Vec8d operator * (Vec8d const & a, double b) 1");
    }

    // static inline Vec8d operator * (double a, Vec8d const & b) 
    {
        Vec8d vec0 = -174.918;
        double p = 2984.12389;
        Vec8d vec1 = p * vec0;
        CHECK_CONDITION(test_vector_with_double(vec1, -5.21976982591020001564174890518E5) == 0, "static inline Vec8d operator * (double a, Vec8d const & b)  1");
    }

    // static inline Vec8d & operator *= (Vec8d & a, Vec8d const & b)
    {
        Vec8d vec0 = -174.918;
        Vec8d vec1 = 2984.12389;
        vec1 *= vec0;
        CHECK_CONDITION(test_vector_with_double(vec1, -5.21976982591020001564174890518E5) == 0, "static inline Vec8d & operator *= (Vec8d & a, Vec8d const & b)  1");
    }

    // static inline Vec8d operator / (Vec8d const & a, Vec8d const & b)
    {
        Vec8d vec0 = 16.4;
        Vec8d vec1 = 4.1;
        Vec8d vec2 = vec0 / vec1;
        CHECK_CONDITION(test_vector_with_double(vec2, 4.0) == 0, "static inline Vec8d operator / (Vec8d const & a, Vec8d const & b) 1");
    }

    // static inline Vec8d operator / (Vec8d const & a, double b)
    {
        Vec8d vec0 = 16.4;
        double b = 4.1;
        Vec8d vec1 = vec0 / b;
        CHECK_CONDITION(test_vector_with_double(vec1, 4.0) == 0, "static inline Vec8d operator / (Vec8d const & a, double b) 1");
    }

    // static inline Vec8d operator / (double a, Vec8d const & b)
    {
        double a = 4.1;
        Vec8d vec0 = 16.4;
        Vec8d vec1 = vec0 / a;
        CHECK_CONDITION(test_vector_with_double(vec1, 4.0) == 0, "static inline Vec8d operator / (Vec8d const & a, double b) 1");
    }

    // static inline Vec8d & operator /= (Vec8d & a, Vec8d const & b)
    {
        Vec8d vec0 = 16.4;
        Vec8d vec1 = 4.1;
        vec0 /= vec1;
        CHECK_CONDITION(test_vector_with_double(vec0, 4.0) == 0, "static inline Vec8d & operator /= (Vec8d & a, Vec8d const & b) 1");
    }

    // static inline Vec8b operator == (Vec8d const & a, Vec8d const & b)
    {
        Vec8d vec0 = 28351720.12389;
        Vec8d vec1 = 11204891.129;
        Vec8d vec2 = 28351720.12389;
        Vec8b vec3 = vec0 == vec1;
        Vec8b vec4 = vec0 == vec2;
        // print_all_elements((void*)&vec3, VECTYPE_8B);
        // print_all_elements((void*)&vec4, VECTYPE_8B);
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec3, VECTYPE_8B, 0x00) == 0, "static inline Vec8b operator == (Vec8d const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec4, VECTYPE_8B, 0xFF) == 0, "static inline Vec8b operator == (Vec8d const & a, Vec8d const & b) 2");
    }

    // static inline Vec8b operator != (Vec8d const & a, Vec8d const & b)
    {
        Vec8d vec0 = 28351720.12389;
        Vec8d vec1 = 11204891.129;
        Vec8d vec2 = 28351720.12389;
        Vec8b vec3 = vec0 != vec1;
        Vec8b vec4 = vec0 != vec2;
        // print_all_elements((void*)&vec3, VECTYPE_8B);
        // print_all_elements((void*)&vec4, VECTYPE_8B);
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec3, VECTYPE_8B, 0xFF) == 0, "static inline Vec8b operator != (Vec8d const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec4, VECTYPE_8B, 0x00) == 0, "static inline Vec8b operator != (Vec8d const & a, Vec8d const & b) 2");
    }

    // static inline Vec8b operator < (Vec8d const & a, Vec8d const & b)
    {
        Vec8d vec0 = 28351720.12389;
        Vec8d vec1 = 11204891.129;
        Vec8d vec2 = 38351720.12389;
        Vec8b vec3 = vec0 < vec1;
        Vec8b vec4 = vec0 < vec2;
        // print_all_elements((void*)&vec3, VECTYPE_8B);
        // print_all_elements((void*)&vec4, VECTYPE_8B);
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec3, VECTYPE_8B, 0x00) == 0, "static inline Vec8b operator < (Vec8d const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec4, VECTYPE_8B, 0xFF) == 0, "static inline Vec8b operator < (Vec8d const & a, Vec8d const & b) 2");
    }

    // static inline Vec8b operator <= (Vec8d const & a, Vec8d const & b)
    {
        Vec8d vec0 = 28351720.12389;
        Vec8d vec1 = 11204891.129;
        Vec8d vec2 = 28351720.12389;
        Vec8b vec3 = vec0 <= vec1;
        Vec8b vec4 = vec0 <= vec2;
        // print_all_elements((void*)&vec3, VECTYPE_8B);
        // print_all_elements((void*)&vec4, VECTYPE_8B);
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec3, VECTYPE_8B, 0x00) == 0, "static inline Vec8b operator <= (Vec8d const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec4, VECTYPE_8B, 0xFF) == 0, "static inline Vec8b operator <= (Vec8d const & a, Vec8d const & b) 2");
    }

    // static inline Vec8b operator > (Vec8d const & a, Vec8d const & b)
    {
        Vec8d vec0 = 28351720.12389;
        Vec8d vec1 = 11204891.129;
        Vec8d vec2 = 38351720.12389;
        Vec8b vec3 = vec0 > vec1;
        Vec8b vec4 = vec0 > vec2;
        // print_all_elements((void*)&vec3, VECTYPE_8B);
        // print_all_elements((void*)&vec4, VECTYPE_8B);
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec3, VECTYPE_8B, 0xFF) == 0, "static inline Vec8b operator > (Vec8d const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec4, VECTYPE_8B, 0x00) == 0, "static inline Vec8b operator > (Vec8d const & a, Vec8d const & b) 2");
    }

    // static inline Vec8d operator & (Vec8d const & a, Vec8d const & b) 
    {
        Vec8d vec0 = 28351720.12389; // 0x417B09CE81FB7415
        Vec8d vec1 = 11204891.129;   // 0x41655F236420C49C
        Vec8d vec2 = 38351720.12389; // 0x4182499B40FDBA0A
        Vec8d vec3 = vec0 & vec1;   // 0x4161090200204414
        Vec8d vec4 = vec0 & vec2;   // 0x4102098A00F93000
        // print_all_elements((void*)&vec3, VECTYPE_8B);
        // print_all_elements((void*)&vec4, VECTYPE_8B);
        CHECK_CONDITION(test_vector_with_double(vec3, 8.93134400393871217966079711914E6) == 0, "static inline Vec8d operator & (Vec8d const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_vector_with_double(vec4,  1.4776125047528743743896484375E5) == 0, "static inline Vec8d operator & (Vec8d const & a, Vec8d const & b) 2");
    }

    // static inline Vec8d & operator &= (Vec8d & a, Vec8d const & b)
    {
        Vec8d vec0 = 28351720.12389; // 0x417B09CE81FB7415
        Vec8d vec1 = 11204891.129;   // 0x41655F236420C49C
        Vec8d vec2 = 38351720.12389; // 0x4182499B40FDBA0A
        vec1 &= vec0;   // 0x4161090200204414
        vec2 &= vec0;   // 0x4102098A00F93000
        // print_all_elements((void*)&vec1, VECTYPE_8B);
        // print_all_elements((void*)&vec2, VECTYPE_8B);
        CHECK_CONDITION(test_vector_with_double(vec1, 8.93134400393871217966079711914E6) == 0, "static inline Vec8d & operator &= (Vec8d & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_vector_with_double(vec2,  1.4776125047528743743896484375E5) == 0, "static inline Vec8d & operator &= (Vec8d & a, Vec8d const & b) 2");
    }

    // static inline Vec8d operator & (Vec8d const & a, Vec8b const & b)
    {
        Vec8d vec0 = 28351720.12389;     //0x417B09CE81FB7415
        Vec8b vec1 = 0xAB;
        Vec8d vec2 = vec0 & vec1;
        // print_all_elements((void*)&vec2, VECTYPE_8D);
        CHECK_CONDITION(test_double(vec2[4], 0.0) == 0, "static inline Vec8d operator & (Vec8d const & a, Vec8b const & b) 1");
        CHECK_CONDITION(test_double(vec2[7], 28351720.12389) == 0, "static inline Vec8d operator & (Vec8d const & a, Vec8b const & b) 2");
    }

    // static inline Vec8d operator & (Vec8b const & a, Vec8d const & b) 
    {
        Vec8d vec0 = 28351720.12389;     //0x417B09CE81FB7415
        Vec8b vec1 = 0xAB;
        Vec8d vec2 = vec1 & vec0;
        // print_all_elements((void*)&vec2, VECTYPE_8D);
        CHECK_CONDITION(test_double(vec2[4], 0.0) == 0, "static inline Vec8d operator & (Vec8b const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_double(vec2[7], 28351720.12389) == 0, "static inline Vec8d operator & (Vec8b const & a, Vec8d const & b) 2");
    }

    // static inline Vec8d operator | (Vec8d const & a, Vec8d const & b)
    {
        Vec8d vec0 = 28351720.12389; // 0x417B09CE81FB7415
        Vec8d vec1 = 11204891.129;   // 0x41655F236420C49C
        Vec8d vec2 = 38351720.12389; // 0x4182499B40FDBA0A
        Vec8d vec3 = vec1 | vec0;    // 0x417F5FEFE5FBF49D
        Vec8d vec4 = vec2 | vec0;    // 0x41FB49DFC1FFFE1F
        // print_all_elements((void*)&vec3, VECTYPE_8D);
        CHECK_CONDITION(test_vector_with_double(vec3, 3.28988143740125782787799835205E7) == 0, "static inline Vec8d operator | (Vec8d const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_vector_with_double(vec4,   7.32521987199954128265380859375E9) == 0, "static inline Vec8d operator | (Vec8d const & a, Vec8d const & b) 2");
    }

    // static inline Vec8d & operator |= (Vec8d & a, Vec8d const & b)
    {
        Vec8d vec0 = 28351720.12389; // 0x417B09CE81FB7415
        Vec8d vec1 = 11204891.129;   // 0x41655F236420C49C
        Vec8d vec2 = 38351720.12389; // 0x4182499B40FDBA0A
        vec1 |= vec0;    // 0x417F5FEFE5FBF49D
        vec2 |= vec0;    // 0x41FB49DFC1FFFE1F
        // print_all_elements((void*)&vec3, VECTYPE_8D);
        CHECK_CONDITION(test_vector_with_double(vec1, 3.28988143740125782787799835205E7) == 0, "static inline Vec8d & operator |= (Vec8d & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_vector_with_double(vec2,   7.32521987199954128265380859375E9) == 0, "static inline Vec8d & operator |= (Vec8d & a, Vec8d const & b) 2");
    }

    // static inline Vec8d operator ^ (Vec8d const & a, Vec8d const & b) 
    {
        Vec8d vec0 = 28351720.12389; // 0x417B09CE81FB7415
        Vec8d vec1 = 11204891.129;   // 0x41655F236420C49C
        Vec8d vec2 = 38351720.12389; // 0x4182499B40FDBA0A
        Vec8d vec3 = vec1 ^ vec0;    // 0x001E56EDE5DBB089
        Vec8d vec4 = vec2 ^ vec0;    // 0x00F94055C106CE1F
        // print_all_elements((void*)&vec3, VECTYPE_8D);
        CHECK_CONDITION(test_vector_with_double(vec3, 4.21923616234224836408018745617E-308) == 0, "static inline Vec8d operator ^ (Vec8d const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_vector_with_double(vec4, 5.75344910762826693374378958271E-304) == 0, "static inline Vec8d operator ^ (Vec8d const & a, Vec8d const & b) 2");
    }

    // static inline Vec8d & operator ^= (Vec8d & a, Vec8d const & b) 
    {
        Vec8d vec0 = 28351720.12389; // 0x417B09CE81FB7415
        Vec8d vec1 = 11204891.129;   // 0x41655F236420C49C
        Vec8d vec2 = 38351720.12389; // 0x4182499B40FDBA0A
        vec1 ^= vec0;    // 0x001E56EDE5DBB089
        vec2 ^= vec0;    // 0x00F94055C106CE1F
        // print_all_elements((void*)&vec1, VECTYPE_8D);
        CHECK_CONDITION(test_vector_with_double(vec1, 4.21923616234224836408018745617E-308) == 0, "static inline Vec8d operator ^ (Vec8d const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_vector_with_double(vec2, 5.75344910762826693374378958271E-304) == 0, "static inline Vec8d operator ^ (Vec8d const & a, Vec8d const & b) 2");
    }

    // static inline Vec8b operator ! (Vec8d const & a )
    {
        Vec8d vec0 = 0.0;
        Vec8d vec1 = 1282.1203;
        Vec8d vec2 = -0.0001238;
        Vec8b vec3 = !vec0;
        Vec8b vec4 = !vec1;
        Vec8b vec5 = !vec2;
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec3, VECTYPE_8B, 0xFF) == 0, "static inline Vec8b operator ! (Vec8d const & a ) 1");
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec4, VECTYPE_8B, 0x00) == 0, "static inline Vec8b operator ! (Vec8d const & a ) 2");
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec5, VECTYPE_8B, 0x00) == 0, "static inline Vec8b operator ! (Vec8d const & a ) 3");
    }
    
    if(g_allSuccess == true)
    {
        printf("Vec8d OK\n");
    }
    else
    {
        printf("Vec8d found %d errors\n", g_failCount);
    }
}

/*void test_vec8q_functions(){
    
    // static inline Vec8q select (Vec8b const & s, Vec8q const & a, Vec8q const & b) 
    // static inline Vec8q if_add (Vec8b const & f, Vec8q const & a, Vec8q const & b) 
    // static inline int64_t horizontal_add (Vec8q const & a)
    // static inline int64_t horizontal_add_x (Vec8q const & a)
    // static inline Vec8q add_saturated(Vec8q const & a, Vec8q const & b) 
    // static inline Vec8q sub_saturated(Vec8q const & a, Vec8q const & b) 
    // static inline Vec8q max(Vec8q const & a, Vec8q const & b) 
    // static inline Vec8q min(Vec8q const & a, Vec8q const & b) 
    // static inline Vec8q abs(Vec8q const & a) 
    // static inline Vec8q abs_saturated(Vec8q const & a)
    // static inline Vec8q rotate_left(Vec8q const & a, uint32_t b)
}*/

void test_vec16f_functions()
{
    // NOTE: Testing for equality is not a good practice for floating point operations. 
    //       However, basic operations are deterministic and if for some reason results change, the unit tests should be updated with new, correct results.
    char header[] = "Vec16f function test";
    INIT_TEST(header);
    
    // comment this out to see detailed test list
    //SUPRESS_MESSAGES();
        
    // Select between two operands. Corresponds to this pseudocode:
    // for (int i = 0; i < 16; i++) result[i] = s[i] ? a[i] : b[i];
    // Each byte in s must be either 0 (false) or 0xFFFFFFFF (true). No other values are allowed.
    // static inline Vec16f select (Vec16b const & s, Vec16f const & a, Vec16f const & b)
    {
        Vec16b vec0 = 0x8001;
        Vec16f vec1 = 3.14;
        Vec16f vec2 = 2.71;
        Vec16f vec3 = select(vec0, vec1, vec2);
       // print_all_elements((void*)&vec3, VECTYPE_16F);
        CHECK_CONDITION(test_float(vec3[0], 3.14) == 0, "static inline Vec16f select (Vec16b const & s, Vec16f const & a, Vec16f const & b) 1");
        CHECK_CONDITION(test_float(vec3[1], 2.71) == 0, "static inline Vec16f select (Vec16b const & s, Vec16f const & a, Vec16f const & b) 2");
        CHECK_CONDITION(test_float(vec3[15], 3.14) == 0, "static inline Vec16f select (Vec16b const & s, Vec16f const & a, Vec16f const & b) 3");
        CHECK_CONDITION(test_float(vec3[14], 2.71) == 0, "static inline Vec16f select (Vec16b const & s, Vec16f const & a, Vec16f const & b) 4");
    }

    // Conditional add: For all vector elements i: result[i] = f[i] ? (a[i] + b[i]) : a[i]
    //static inline Vec16f if_add (Vec16b const & f, Vec16f const & a, Vec16f const & b)
    {
        Vec16b vec0 = 0x8001;
        Vec16f vec1 = 3.14;
        Vec16f vec2 = 2.71;
        Vec16f vec3 = if_add(vec0, vec1, vec2);
      //  print_all_elements((void*)&vec3, VECTYPE_16F);
        CHECK_CONDITION(test_float(vec3[0],  5.8500003814697265625E0) == 0, "static inline Vec16f if_add (Vec16b const & f, Vec16f const & a, Vec16f const & b) 1");
        CHECK_CONDITION(test_float(vec3[1], 3.14) == 0, "static inline Vec16f if_add (Vec16b const & f, Vec16f const & a, Vec16f const & b) 2");
        CHECK_CONDITION(test_float(vec3[15],  5.8500003814697265625E0) == 0, "static inline Vec16f if_add (Vec16b const & f, Vec16f const & a, Vec16f const & b) 3");
        CHECK_CONDITION(test_float(vec3[14], 3.14) == 0, "static inline Vec16f if_add (Vec16b const & f, Vec16f const & a, Vec16f const & b) 4");
    }

    // Conditional multiply: For all vector elements i: result[i] = f[i] ? (a[i] * b[i]) : a[i]
    // static inline Vec16f if_mul (Vec16b const & f, Vec16f const & a, Vec16f const & b)
    {
        Vec16b vec0 = 0x8001;
        Vec16f vec1 = 3.14;
        Vec16f vec2 = 2.71;
        Vec16f vec3 = if_mul(vec0, vec1, vec2);
       // print_all_elements((void*)&vec3, VECTYPE_16F);
        CHECK_CONDITION(test_float(vec3[0],   8.50940036773681640625E0) == 0, "static inline Vec16f if_mul (Vec16b const & f, Vec16f const & a, Vec16f const & b) 1");
        CHECK_CONDITION(test_float(vec3[1], 3.14) == 0, "static inline Vec16f if_mul (Vec16b const & f, Vec16f const & a, Vec16f const & b) 2");
        CHECK_CONDITION(test_float(vec3[15],   8.50940036773681640625E0) == 0, "static inline Vec16f if_mul (Vec16b const & f, Vec16f const & a, Vec16f const & b) 3");
        CHECK_CONDITION(test_float(vec3[14], 3.14) == 0, "static inline Vec16f if_mul (Vec16b const & f, Vec16f const & a, Vec16f const & b) 4");
    }

    // Horizontal add: Calculates the sum of all vector elements.
    // static inline float horizontal_add (Vec16f const & a)
    {
        Vec16f vec0 = Vec16f(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6);
        float f = horizontal_add(vec0);
        CHECK_CONDITION(test_float(f, 13.6) == 0, "static inline float horizontal_add (Vec16f const & a) 1");
    }

    // function max: a > b ? a : b
    //static inline Vec16f max(Vec16f const & a, Vec16f const & b)
    {
        Vec16f vec0 = 3.14;
        Vec16f vec1 = 9999.9999;
        Vec16f vec2 = max(vec0, vec1);
        Vec16f vec3 = max(vec1, vec0);
        CHECK_CONDITION(test_vector_with_float(vec2, 9999.9999) == 0, "static inline Vec16f max(Vec16f const & a, Vec16f const & b) 1");
        CHECK_CONDITION(test_vector_with_float(vec3, 9999.9999) == 0, "static inline Vec16f max(Vec16f const & a, Vec16f const & b) 2");
    }

    // function min: a < b ? a : b
    //static inline Vec16f min(Vec16f const & a, Vec16f const & b)
    {
        Vec16f vec0 = 3.14;
        Vec16f vec1 = 9999.9999;
        Vec16f vec2 = min(vec0, vec1);
        Vec16f vec3 = min(vec1, vec0);
        CHECK_CONDITION(test_vector_with_float(vec2, 3.14) == 0, "static inline Vec16f min(Vec16f const & a, Vec16f const & b) 1");
        CHECK_CONDITION(test_vector_with_float(vec3, 3.14) == 0, "static inline Vec16f min(Vec16f const & a, Vec16f const & b) 2");
    }

    // function abs: absolute value
    // Removes sign bit, even for -0.0f, -INF and -NAN
    // static inline Vec16f abs(Vec16f const & a)
    {
        Vec16f vec0 = -297147.12298;
        Vec16f vec1 = 120398.1298;
        Vec16f vec2 = abs(vec0);
        Vec16f vec3 = abs(vec1);
        CHECK_CONDITION(test_vector_with_float(vec2, 297147.12298) == 0, "static inline Vec16f abs(Vec16f const & a) 1");
        CHECK_CONDITION(test_vector_with_float(vec1, 120398.1298) == 0, "static inline Vec16f abs(Vec16f const & a) 3");
    }

    // function sqrt: square root
    // static inline Vec16f sqrt(Vec16f const & a)
    {
        Vec16f vec0 = 12345.6789;
        Vec16f vec1 = -12345.6789;
        Vec16f vec2 = sqrt(vec0);
        Vec16f vec3 = sqrt(vec1);
        Vec16b vec4 = is_nan(vec2);
        Vec16b vec5 = is_nan(vec3);
       /* print_all_elements((void*)&vec3, VECTYPE_16F);
        print_all_elements((void*)&vec5, VECTYPE_16B);*/
        CHECK_CONDITION(test_vector_with_float(vec2, 111.11111060555555440541666143353) == 0, "static inline Vec16f sqrt(Vec16f const & a) 1");
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec4, VECTYPE_16B, 0x0000) == 0, "static inline Vec16f sqrt(Vec16f const & a) 2");
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec5, VECTYPE_16B, 0xFFFF) == 0, "static inline Vec16f sqrt(Vec16f const & a) 3");
    }

    // function square: a * a
    // static inline Vec16f square(Vec16f const & a)
    {
        Vec16f vec0 = 18.45;
        Vec16f vec1 = -18.45;
        Vec16f vec2 = square(vec0); // exact value: 340.4025
        Vec16f vec3 = square(vec1); // exact value: 340.4025
       /* print_all_elements((void*)&vec2, VECTYPE_16F);
        print_all_elements((void*)&vec3, VECTYPE_16F);*/
        CHECK_CONDITION(test_vector_with_float(vec2, 340.402527) == 0, "static inline Vec16f square(Vec16f const & a) 1");
        CHECK_CONDITION(test_vector_with_float(vec3, 340.402527) == 0, "static inline Vec16f square(Vec16f const & a) 2");
    }

    // pow(Vec8f, int):
    // Raise floating point numbers to integer power n
    // static inline Vec16f pow(Vec16f const & a, int n)
    {
        Vec16f vec0 = 18.45;
        Vec16f vec1 = pow(vec0, 2); // exact value: 340.4025
        Vec16f vec2 = pow(vec0, 8); // exact value: 13426751896.2434672750390625
        Vec16f vec3 = pow(-vec0, 8); // exact value: 13426751896.2434672750390625
       /* print_all_elements((void*)&vec1, VECTYPE_16F);
        print_all_elements((void*)&vec2, VECTYPE_16F);*/
        CHECK_CONDITION(test_vector_with_float(vec1, 340.402527) == 0, "static inline Vec16f pow(Vec16f const & a, int n) 1");
        CHECK_CONDITION(test_vector_with_float(vec2, 1.3426756608E10) == 0, "static inline Vec16f pow(Vec16f const & a, int n) 2");
        CHECK_CONDITION(test_vector_with_float(vec3, 1.3426756608E10) == 0, "static inline Vec16f pow(Vec16f const & a, int n) 3");
    }
    
    // Raise floating point numbers to integer power n, where n is a compile-time constant
    // template <int n>
    // static inline Vec16f pow_n(Vec16f const & a)
    {
        Vec16f vec0 = 18.45;
        Vec16f vec1 = pow_n<2>(vec0); // exact value: 340.4025
        Vec16f vec2 = pow_n<8>(vec0); // exact value: 13426751896.2434672750390625
        Vec16f vec3 = pow_n<8>(-vec0); // exact value: 13426751896.2434672750390625
        /*print_all_elements((void*)&vec1, VECTYPE_16F);
        print_all_elements((void*)&vec2, VECTYPE_16F);*/
        CHECK_CONDITION(test_vector_with_float(vec1, 340.402527) == 0, "static inline Vec16f pow_n(Vec16f const & a) 1");
        CHECK_CONDITION(test_vector_with_float(vec2, 1.3426756608E10) == 0, "static inline Vec16f pow_n(Vec16f const & a) 2");
        CHECK_CONDITION(test_vector_with_float(vec3, 1.3426756608E10) == 0, "static inline Vec16f pow_n(Vec16f const & a) 3");
    }

    // function round: round to nearest integer (even). (result as float vector)
    // static inline Vec16f round(Vec16f const & a)
    {
        Vec16f vec0 = 18.45;
        Vec16f vec1 = 18.85;
        Vec16f vec2 = -3.14;
        Vec16f vec3 = -2.71;
        Vec16f vec4 = round(vec0);
        Vec16f vec5 = round(vec1);
        Vec16f vec6 = round(vec2);
        Vec16f vec7 = round(vec3);
        CHECK_CONDITION(test_vector_with_float(vec4, 18.0) == 0, "static inline Vec16f round(Vec16f const & a) 1");
        CHECK_CONDITION(test_vector_with_float(vec5, 19.0) == 0, "static inline Vec16f round(Vec16f const & a) 2");
        CHECK_CONDITION(test_vector_with_float(vec6, -3.0) == 0, "static inline Vec16f round(Vec16f const & a) 3");
        CHECK_CONDITION(test_vector_with_float(vec7, -3.0) == 0, "static inline Vec16f round(Vec16f const & a) 4");
    }

    // function truncate: round towards zero. (result as float vector)
    // static inline Vec16f truncate(Vec16f const & a)
    {
        Vec16f vec0 = 18.45;
        Vec16f vec1 = 18.85;
        Vec16f vec2 = -3.14;
        Vec16f vec3 = -2.71;
        Vec16f vec4 = truncate(vec0);
        Vec16f vec5 = truncate(vec1);
        Vec16f vec6 = truncate(vec2);
        Vec16f vec7 = truncate(vec3);
        CHECK_CONDITION(test_vector_with_float(vec4, 18.0) == 0, "static inline Vec16f truncate(Vec16f const & a) 1");
        CHECK_CONDITION(test_vector_with_float(vec5, 18.0) == 0, "static inline Vec16f truncate(Vec16f const & a) 2");
        CHECK_CONDITION(test_vector_with_float(vec6, -3.0) == 0, "static inline Vec16f truncate(Vec16f const & a) 3");
        CHECK_CONDITION(test_vector_with_float(vec7, -2.0) == 0, "static inline Vec16f truncate(Vec16f const & a) 4");
    }

    // function floor: round towards minus infinity. (result as float vector)
    // static inline Vec16f floor(Vec16f const & a) 
    {
        Vec16f vec0 = 18.45;
        Vec16f vec1 = 18.85;
        Vec16f vec2 = -3.14;
        Vec16f vec3 = -2.71;
        Vec16f vec4 = floor(vec0);
        Vec16f vec5 = floor(vec1);
        Vec16f vec6 = floor(vec2);
        Vec16f vec7 = floor(vec3);
        CHECK_CONDITION(test_vector_with_float(vec4, 18.0) == 0, "static inline Vec16f floor(Vec16f const & a) 1");
        CHECK_CONDITION(test_vector_with_float(vec5, 18.0) == 0, "static inline Vec16f floor(Vec16f const & a) 2");
        CHECK_CONDITION(test_vector_with_float(vec6, -4.0) == 0, "static inline Vec16f floor(Vec16f const & a) 3");
        CHECK_CONDITION(test_vector_with_float(vec7, -3.0) == 0, "static inline Vec16f floor(Vec16f const & a) 4");
    }

    // function ceil: round towards plus infinity. (result as float vector)
    // static inline Vec16f ceil(Vec16f const & a)
    {
        Vec16f vec0 = 18.45;
        Vec16f vec1 = 18.85;
        Vec16f vec2 = -3.14;
        Vec16f vec3 = -2.71;
        Vec16f vec4 = ceil(vec0);
        Vec16f vec5 = ceil(vec1);
        Vec16f vec6 = ceil(vec2);
        Vec16f vec7 = ceil(vec3);
        CHECK_CONDITION(test_vector_with_float(vec4, 19.0) == 0, "static inline Vec16f floor(Vec16f const & a) 1");
        CHECK_CONDITION(test_vector_with_float(vec5, 19.0) == 0, "static inline Vec16f floor(Vec16f const & a) 2");
        CHECK_CONDITION(test_vector_with_float(vec6, -3.0) == 0, "static inline Vec16f floor(Vec16f const & a) 3");
        CHECK_CONDITION(test_vector_with_float(vec7, -2.0) == 0, "static inline Vec16f floor(Vec16f const & a) 4");
    }

    // static inline Vec16i round_to_int(Vec16f const & a)
    {
        Vec16f vec0 = 18.45;
        Vec16f vec1 = 18.85;
        Vec16f vec2 = -3.14;
        Vec16f vec3 = -2.71;
        Vec16i vec4 = round_to_int(vec0);
        Vec16i vec5 = round_to_int(vec1);
        Vec16i vec6 = round_to_int(vec2);
        Vec16i vec7 = round_to_int(vec3);
        CHECK_CONDITION(test_all_elements((void*)&vec4, VECTYPE_16I, 18) == 0, "static inline Vec16i round_to_int(Vec16f const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec5, VECTYPE_16I, 19) == 0, "static inline Vec16i round_to_int(Vec16f const & a) 2");
        CHECK_CONDITION(test_all_elements((void*)&vec6, VECTYPE_16I, -3) == 0, "static inline Vec16i round_to_int(Vec16f const & a) 3");
        CHECK_CONDITION(test_all_elements((void*)&vec7, VECTYPE_16I, -3) == 0, "static inline Vec16i round_to_int(Vec16f const & a) 4");
    }

    // function truncate_to_int: round towards zero. (result as integer vector)
    // static inline Vec16i truncate_to_int(Vec16f const & a)
    {
        Vec16f vec0 = 18.45;
        Vec16f vec1 = 18.85;
        Vec16f vec2 = -3.14;
        Vec16f vec3 = -2.71;
        Vec16i vec4 = truncate_to_int(vec0);
        Vec16i vec5 = truncate_to_int(vec1);
        Vec16i vec6 = truncate_to_int(vec2);
        Vec16i vec7 = truncate_to_int(vec3);
        CHECK_CONDITION(test_all_elements((void*)&vec4, VECTYPE_16I, 18) == 0, "static inline Vec16i truncate_to_int(Vec16f const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec5, VECTYPE_16I, 18) == 0, "static inline Vec16i truncate_to_int(Vec16f const & a) 2");
        CHECK_CONDITION(test_all_elements((void*)&vec6, VECTYPE_16I, -3) == 0, "static inline Vec16i truncate_to_int(Vec16f const & a) 3");
        CHECK_CONDITION(test_all_elements((void*)&vec7, VECTYPE_16I, -2) == 0, "static inline Vec16i truncate_to_int(Vec16f const & a) 4");
    }

    // function to_float: convert integer vector to float vector
    // static inline Vec16f to_float(Vec16i const & a)
    {
        Vec16i vec0 = 1234567;
        Vec16i vec1 = -123790;
        Vec16i vec2 = 0;
        Vec16i vec3 = -1; 
        Vec16f vec4 = to_float(vec0);
        Vec16f vec5 = to_float(vec1);
        Vec16f vec6 = to_float(vec2);
        Vec16f vec7 = to_float(vec3);
        //print_all_elements((void*)&vec4, VECTYPE_16F);
        //print_all_elements((void*)&vec5, VECTYPE_16F);
        //print_all_elements((void*)&vec6, VECTYPE_16F);
        //print_all_elements((void*)&vec7, VECTYPE_16F);
        CHECK_CONDITION(test_vector_with_float(vec4, 1234567.0) == 0, "static inline Vec16f to_float(Vec16i const & a) 1");
        CHECK_CONDITION(test_vector_with_float(vec5, -123790.0) == 0, "static inline Vec16f to_float(Vec16i const & a) 2");
        CHECK_CONDITION(test_vector_with_float(vec6, 0.0) == 0, "static inline Vec16f to_float(Vec16i const & a) 3");
        CHECK_CONDITION(test_vector_with_float(vec7, -1.0) == 0, "static inline Vec16f to_float(Vec16i const & a) 4");
    }    
   
    // approximate reciprocal (Faster than 1.f / a. relative accuracy better than 2^-11)
    // static inline Vec16f approx_recipr(Vec16f const & a)
    {
        Vec16f vec0 = 1234567.0;
        Vec16f vec1 = -0.1234567;
        Vec16f vec2 = 0.0000000001;
        Vec16f vec3 = -0.0000000001;
        Vec16f vec4 = approx_recipr(vec0);
        Vec16f vec5 = approx_recipr(vec1);
        Vec16f vec6 = approx_recipr(vec2);
        Vec16f vec7 = approx_recipr(vec3);
        /*print_all_elements((void*)&vec4, VECTYPE_16F);
        print_all_elements((void*)&vec5, VECTYPE_16F);
        print_all_elements((void*)&vec6, VECTYPE_16F);
        print_all_elements((void*)&vec7, VECTYPE_16F);*/
        CHECK_CONDITION(test_vector_with_float(vec4, 8.100006E-7) == 0, "static inline Vec16f approx_recipr(Vec16f const & a) 1");
        CHECK_CONDITION(test_vector_with_float(vec5, -8.100006) == 0, "static inline Vec16f approx_recipr(Vec16f const & a) 2");
        CHECK_CONDITION(test_vector_with_float(vec6, 1.0E10) == 0, "static inline Vec16f approx_recipr(Vec16f const & a) 3");
        CHECK_CONDITION(test_vector_with_float(vec7, -1.0E10) == 0, "static inline Vec16f approx_recipr(Vec16f const & a) 4");
    }

    // approximate reciprocal squareroot (Faster than 1.f / sqrt(a). Relative accuracy better than 2^-11)
    // static inline Vec16f approx_rsqrt(Vec16f const & a) 
    {
        Vec16f vec0 = 1234567.780;
        Vec16f vec1 = 0.1234567;
        Vec16f vec2 = approx_rsqrt(vec0);
        Vec16f vec3 = approx_rsqrt(vec1);
        /*print_all_elements((void*)&vec2, VECTYPE_16F);
        print_all_elements((void*)&vec3, VECTYPE_16F);*/
        CHECK_CONDITION(test_vector_with_float(vec2, 9.0000004E-4) == 0, "static inline Vec16f approx_recipr(Vec16f const & a) 1");
        CHECK_CONDITION(test_vector_with_float(vec3, 2.846051) == 0, "static inline Vec16f approx_recipr(Vec16f const & a) 2");
    }

    // Extract the exponent as an integer
    // exponent(a) = floor(log2(abs(a)));
    // exponent(1.0f) = 0, exponent(0.0f) = -127, exponent(INF) = +128, exponent(NAN) = +128
    // static inline Vec16i exponent(Vec16f const & a)
    {
        Vec16f vec0 = 13298570.1239817;
        Vec16f vec1 = -0.1239819;
        Vec16f vec2 = 123120000.0;
        Vec16f vec3 = 0.0;
        Vec16i vec4 = exponent(vec0);
        Vec16i vec5 = exponent(vec1);
        Vec16i vec6 = exponent(vec2);
        Vec16i vec7 = exponent(vec3); // -Inf
        /*print_all_elements((void*)&vec4, VECTYPE_16I);
        print_all_elements((void*)&vec5, VECTYPE_16I);
        print_all_elements((void*)&vec6, VECTYPE_16I);
        print_all_elements((void*)&vec7, VECTYPE_16I);  */
        CHECK_CONDITION(test_all_elements((void*)&vec4, VECTYPE_16I, 23) == 0, "static inline Vec16i exponent(Vec16f const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec5, VECTYPE_16I, -4) == 0, "static inline Vec16i exponent(Vec16f const & a) 2");
        CHECK_CONDITION(test_all_elements((void*)&vec6, VECTYPE_16I, 26) == 0, "static inline Vec16i exponent(Vec16f const & a) 3");
        CHECK_CONDITION(test_all_elements((void*)&vec7, VECTYPE_16I, 0x80000000) == 0, "static inline Vec16i exponent(Vec16f const & a) 4");
    }

    // Extract the fraction part of a floating point number
    // a = 2^exponent(a) * fraction(a), except for a = 0
    // fraction(1.0f) = 1.0f, fraction(5.0f) = 1.25f 
    // static inline Vec16f fraction(Vec16f const & a)
    {
        Vec16f vec0 = 13298570.1239817;
        Vec16f vec1 = -0.1239819;
        Vec16f vec2 = 123120000.0;
        Vec16f vec3 = 0.0;
        Vec16f vec4 = 5.0;
        Vec16f vec5 = fraction(vec0);
        Vec16f vec6 = fraction(vec1);
        Vec16f vec7 = fraction(vec2);
        Vec16f vec8 = fraction(vec3);   // NOTE: Agner's implementation gives 1.0 here.
        Vec16f vec9 = fraction(vec4);
        /*print_all_elements((void*)&vec5, VECTYPE_16F);
        print_all_elements((void*)&vec6, VECTYPE_16F);
        print_all_elements((void*)&vec7, VECTYPE_16F);  
        print_all_elements((void*)&vec8, VECTYPE_16F);
        print_all_elements((void*)&vec9, VECTYPE_16F);*/
        CHECK_CONDITION(test_vector_with_float(vec5, 1.5853131) == 0,  "static inline Vec16f fraction(Vec16f const & a) 1");
        CHECK_CONDITION(test_vector_with_float(vec6, 1.9837104) == 0,  "static inline Vec16f fraction(Vec16f const & a) 2");
        CHECK_CONDITION(test_vector_with_float(vec7, 1.834631) == 0,   "static inline Vec16f fraction(Vec16f const & a) 3");
        CHECK_CONDITION(test_vector_with_float(vec8, 1.0) == 0,        "static inline Vec16f fraction(Vec16f const & a) 4");  // TODO: what should be the correct result?
        CHECK_CONDITION(test_vector_with_float(vec9, 1.25) == 0,       "static inline Vec16f fraction(Vec16f const & a) 5");
    }

    // static inline Vec16f exp2(Vec16i const & n) 
    {
        Vec16i vec0  = 0;
        Vec16i vec1  = 128;
        Vec16i vec2  = 127;
        Vec16i vec3  = -127;
        Vec16i vec4  = -126;
        Vec16i vec5  = 42;
        Vec16f vec6  = exp2(vec0);
        Vec16f vec7  = exp2(vec1);
        Vec16f vec8  = exp2(vec2);
        Vec16f vec9  = exp2(vec3);
        Vec16f vec10 = exp2(vec4);
        /*print_all_elements((void*)&vec6,  VECTYPE_16F);
        print_all_elements((void*)&vec7,  VECTYPE_16F);  
        print_all_elements((void*)&vec8,  VECTYPE_16F);
        print_all_elements((void*)&vec9,  VECTYPE_16F);
        print_all_elements((void*)&vec10, VECTYPE_16F);*/
        CHECK_CONDITION(test_vector_with_float(vec6,  1.000000) == 0,          "static inline Vec16f exp2(Vec16i const & n) 1");
        CHECK_CONDITION(test_vector_with_float(vec7,  infinite16f()[0]) == 0,  "static inline Vec16f exp2(Vec16i const & n) 2");
        CHECK_CONDITION(test_vector_with_float(vec8,  1.7014118E38) == 0,      "static inline Vec16f exp2(Vec16i const & n) 3");  // TODO: what should be the correct result?
        CHECK_CONDITION(test_vector_with_float(vec9,  0.0) == 0,               "static inline Vec16f exp2(Vec16i const & n) 4");
        CHECK_CONDITION(test_vector_with_float(vec10, 1.17549435E-38) == 0,    "static inline Vec16f exp2(Vec16i const & n) 5");
    }

    // static inline Vec16b sign_bit(Vec16f const & a) 
    {
        Vec16f vec0 = infinite16f();
        Vec16f vec1 = -infinite16f();
        Vec16f vec2 = -0.0;
        Vec16f vec3 = 0.0;
        Vec16f vec4 = -12.22;
        Vec16f vec5 = 12.22;
        Vec16b vec6 = sign_bit(vec0);
        Vec16b vec7 = sign_bit(vec1);
        Vec16b vec8 = sign_bit(vec2);
        Vec16b vec9 = sign_bit(vec3);
        Vec16b vec10 = sign_bit(vec4);
        Vec16b vec11 = sign_bit(vec5);
        /*print_all_elements((void*)&vec6,  VECTYPE_16B);
        print_all_elements((void*)&vec7,  VECTYPE_16B);  
        print_all_elements((void*)&vec8,  VECTYPE_16B);
        print_all_elements((void*)&vec9,  VECTYPE_16B);
        print_all_elements((void*)&vec10, VECTYPE_16B);*/
        CHECK_CONDITION(test_all_elements((void*)&vec6, VECTYPE_16B, 0)  == 0,  "static inline Vec16b sign_bit(Vec16f const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec7, VECTYPE_16B, 1)  == 0,  "static inline Vec16b sign_bit(Vec16f const & a) 2");
        CHECK_CONDITION(test_all_elements((void*)&vec8, VECTYPE_16B, 1)  == 0,  "static inline Vec16b sign_bit(Vec16f const & a) 3");
        CHECK_CONDITION(test_all_elements((void*)&vec9, VECTYPE_16B, 0)  == 0,  "static inline Vec16b sign_bit(Vec16f const & a) 4");
        CHECK_CONDITION(test_all_elements((void*)&vec10, VECTYPE_16B, 1)  == 0,  "static inline Vec16b sign_bit(Vec16f const & a) 5");
        CHECK_CONDITION(test_all_elements((void*)&vec11, VECTYPE_16B, 0)  == 0,  "static inline Vec16b sign_bit(Vec16f const & a) 6");
    }

    //static inline Vec16f sign_combine(Vec16f const & a, Vec16f const & b)
    {
        Vec16f vec0  = infinite16f();
        Vec16f vec1  = -infinite16f();
        Vec16f vec2  = -0.0;
        Vec16f vec3  = 0.0;
        Vec16f vec4  = -12.22;
        Vec16f vec5  = 12.22;

        Vec16f vec6  = -1.0;
        Vec16f vec7  = 1.0;
        Vec16f vec8  = -1.0;
        Vec16f vec9  = -1.0;
        Vec16f vec10 = 1.0;
        Vec16f vec11 = 1.0;

        Vec16f vec12 = sign_combine(vec0, vec6);
        Vec16f vec13 = sign_combine(vec1, vec7);
        Vec16f vec14 = sign_combine(vec2, vec8);
        Vec16f vec15 = sign_combine(vec3, vec9);
        Vec16f vec16 = sign_combine(vec4, vec10);
        Vec16f vec17 = sign_combine(vec5, vec11);

       /* print_all_elements((void*)&vec12, VECTYPE_16F);
        print_all_elements((void*)&vec13, VECTYPE_16F);  
        print_all_elements((void*)&vec14, VECTYPE_16F);
        print_all_elements((void*)&vec15, VECTYPE_16F);
        print_all_elements((void*)&vec16, VECTYPE_16F);
        print_all_elements((void*)&vec17, VECTYPE_16F);*/

        CHECK_CONDITION(test_vector_with_float(vec12,  -infinite16f()[0]) == 0,  "static inline Vec16b sign_bit(Vec16f const & a) 1");
        CHECK_CONDITION(test_vector_with_float(vec13,  -infinite16f()[0]) == 0,  "static inline Vec16b sign_bit(Vec16f const & a) 2");
        CHECK_CONDITION(test_vector_with_float(vec14,  0.0) == 0,  "static inline Vec16b sign_bit(Vec16f const & a) 3");
        CHECK_CONDITION(test_vector_with_float(vec15,  -0.0) == 0,  "static inline Vec16b sign_bit(Vec16f const & a) 4");
        CHECK_CONDITION(test_vector_with_float(vec16,  -12.22) == 0,  "static inline Vec16b sign_bit(Vec16f const & a) 5");
        CHECK_CONDITION(test_vector_with_float(vec17,  12.22) == 0,  "static inline Vec16b sign_bit(Vec16f const & a) 6");
    }

    //static inline Vec16b is_finite(Vec16f const & a)
    {
        Vec16f vec0 = 0.0;
        Vec16f vec1 = 12.34;
        Vec16f vec2 =  infinite16f();
        Vec16f vec3 = -infinite16f();

        Vec16b vec4 = is_finite(vec0);
        Vec16b vec5 = is_finite(vec1);
        Vec16b vec6 = is_finite(vec2);
        Vec16b vec7 = is_finite(vec3);

        CHECK_CONDITION(test_all_elements((void*)&vec4, VECTYPE_16B, 1) == 0,  "static inline Vec16b is_finite(Vec16f const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec5, VECTYPE_16B, 1) == 0,  "static inline Vec16b is_finite(Vec16f const & a) 2");
        CHECK_CONDITION(test_all_elements((void*)&vec6, VECTYPE_16B, 0) == 0,  "static inline Vec16b is_finite(Vec16f const & a) 3");
        CHECK_CONDITION(test_all_elements((void*)&vec7, VECTYPE_16B, 0) == 0,  "static inline Vec16b is_finite(Vec16f const & a) 4");
    }

    //static inline Vec16b is_inf(Vec16f const & a)
    {
        Vec16f vec0 = 0.0;
        Vec16f vec1 = 12.34;
        Vec16f vec2 =  infinite16f();
        Vec16f vec3 = -infinite16f();

        Vec16b vec4 = is_inf(vec0);
        Vec16b vec5 = is_inf(vec1);
        Vec16b vec6 = is_inf(vec2);
        Vec16b vec7 = is_inf(vec3);

        CHECK_CONDITION(test_all_elements((void*)&vec4, VECTYPE_16B, 0) == 0,  "static inline Vec16b is_inf(Vec16f const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec5, VECTYPE_16B, 0) == 0,  "static inline Vec16b is_inf(Vec16f const & a) 2");
        CHECK_CONDITION(test_all_elements((void*)&vec6, VECTYPE_16B, 1) == 0,  "static inline Vec16b is_inf(Vec16f const & a) 3");
        CHECK_CONDITION(test_all_elements((void*)&vec7, VECTYPE_16B, 1) == 0,  "static inline Vec16b is_inf(Vec16f const & a) 4");
    }

    //static inline Vec16b is_nan(Vec16f const & a)
    {
        Vec16f vec0 = 0.0;
        Vec16f vec1 = 12.34;
        Vec16f vec2 = infinite16f();
        Vec16f vec3 = -infinite16f();
        Vec16f vec4 = nan16f();

        Vec16b vec5 = is_nan(vec0);
        Vec16b vec6 = is_nan(vec1);
        Vec16b vec7 = is_nan(vec2);
        Vec16b vec8 = is_nan(vec3);
        Vec16b vec9 = is_nan(vec4);

        CHECK_CONDITION(test_all_elements((void*)&vec5, VECTYPE_16B, 0) == 0,  "static inline Vec16b is_nan(Vec16f const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec6, VECTYPE_16B, 0) == 0,  "static inline Vec16b is_nan(Vec16f const & a) 2");
        CHECK_CONDITION(test_all_elements((void*)&vec7, VECTYPE_16B, 0) == 0,  "static inline Vec16b is_nan(Vec16f const & a) 3");
        CHECK_CONDITION(test_all_elements((void*)&vec8, VECTYPE_16B, 0) == 0,  "static inline Vec16b is_nan(Vec16f const & a) 4");
        CHECK_CONDITION(test_all_elements((void*)&vec9, VECTYPE_16B, 1) == 0,  "static inline Vec16b is_nan(Vec16f const & a) 5");
    }

    //static inline Vec16b is_subnormal(Vec16f const & a)
    {
        // TODO: 
    }

    // static inline  Vec16f infinite16f() 
        // tested implicitly
    // static inline Vec16f nan16f(int n = 0x10)
        // tested implicitly

    // static inline Vec16f change_sign(Vec16f const & a) 
    {
        Vec16f vec0  = infinite16f();
        Vec16f vec1  = -infinite16f();
        Vec16f vec2  = -0.0;
        Vec16f vec3  = 0.0;
        Vec16f vec4  = -12.22;
        Vec16f vec5  = 12.22;

        Vec16f vec6  = change_sign<1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1>(vec0);
        Vec16f vec7  = change_sign<1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1>(vec1);
        Vec16f vec8  = change_sign<1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0>(vec2);
        Vec16f vec9  = change_sign<0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1>(vec3);
        Vec16f vec10 = change_sign<1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0>(vec4);
        Vec16f vec11 = change_sign<1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0>(vec5);

        Vec16b vec12 = sign_bit(vec6);
        Vec16b vec13 = sign_bit(vec7);
        Vec16b vec14 = sign_bit(vec8);
        Vec16b vec15 = sign_bit(vec9);
        Vec16b vec16 = sign_bit(vec10);
        Vec16b vec17 = sign_bit(vec11);
        
       /* print_all_elements((void*)&vec12, VECTYPE_16B);
        print_all_elements((void*)&vec13, VECTYPE_16B);  
        print_all_elements((void*)&vec14, VECTYPE_16B);
        print_all_elements((void*)&vec15, VECTYPE_16B);
        print_all_elements((void*)&vec16, VECTYPE_16B);
        print_all_elements((void*)&vec17, VECTYPE_16B); */

        CHECK_CONDITION(test_all_elements((void*)&vec12, VECTYPE_16B, 1) == 0,  "static inline Vec16f change_sign(Vec16f const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec13, VECTYPE_16B, 0) == 0,  "static inline Vec16f change_sign(Vec16f const & a) 2");
        CHECK_CONDITION((vec14[7] == 0 && vec14[8] == 1),          "static inline Vec16f change_sign(Vec16f const & a) 3");
        CHECK_CONDITION((vec15[7] == 0 && vec15[8] == 1),          "static inline Vec16f change_sign(Vec16f const & a) 4");
        CHECK_CONDITION((vec16[0] == 0 && vec16[1] == 1),          "static inline Vec16f change_sign(Vec16f const & a) 5");
        CHECK_CONDITION((vec17[0] == 1 && vec17[1] == 0),          "static inline Vec16f change_sign(Vec16f const & a) 6");
    }

    if(g_allSuccess == true)
    {
        printf("Vec16f functions: OK\n");
    }
    else
    {
        printf("Vec16f functions: found %d errors\n", g_failCount);
    }
}

void test_vec8d_functions()
{    
    // NOTE: Testing for equality is not a good practice for floating point operations. 
    //       However, basic operations are deterministic and if for some reason results change, the unit tests should be updated with new, correct results.
    char header[] = "Vec8d functions test";
    INIT_TEST(header);
    
    // comment this out to see detailed test list
    //SUPRESS_MESSAGES();

    // Select between two operands. Corresponds to this pseudocode:
    // for (int i = 0; i < 8; i++) result[i] = s[i] ? a[i] : b[i];
    // Each byte in s must be either 0 (false) or 0xFFFFFFFFFFFFFFFF (true). 
    // No other values are allowed.
    // static inline Vec8d select (Vec8b const & s, Vec8d const & a, Vec8d const & b) 
    {
        Vec8b vec0 = 0x81;
        Vec8d vec1 = 3.14;
        Vec8d vec2 = 2.71;
        Vec8d vec3 = select(vec0, vec1, vec2);
        //print_all_elements((void*)&vec3, VECTYPE_8D);
        CHECK_CONDITION(test_double(vec3[0],  3.14) == 0, "static inline Vec8d select (Vec8b const & s, Vec8d const & a, Vec8d const & b)  1");
        CHECK_CONDITION(test_double(vec3[1],  2.71) == 0, "static inline Vec8d select (Vec8b const & s, Vec8d const & a, Vec8d const & b)  2");
        CHECK_CONDITION(test_double(vec3[7], 3.14) == 0, "static inline Vec8d select (Vec8b const & s, Vec8d const & a, Vec8d const & b)  3");
        CHECK_CONDITION(test_double(vec3[6], 2.71) == 0, "static inline Vec8d select (Vec8b const & s, Vec8d const & a, Vec8d const & b)  4");
    }
    // Conditional add: For all vector elements i: result[i] = f[i] ? (a[i] + b[i]) : a[i]
    // static inline Vec8d if_add (Vec8b const & f, Vec8d const & a, Vec8d const & b)
    {
        Vec8b vec0 = 0x81;
        Vec8d vec1 = 3.14;
        Vec8d vec2 = 2.71;
        Vec8d vec3 = if_add(vec0, vec1, vec2);
        //print_all_elements((void*)&vec3, VECTYPE_8D);
        CHECK_CONDITION(test_double(vec3[0], 5.84999999999999964472863211995E0) == 0, "static inline Vec8d if_add (Vec8b const & f, Vec8d const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_double(vec3[1], 3.14000000000000012434497875802E0) == 0, "static inline Vec8d if_add (Vec8b const & f, Vec8d const & a, Vec8d const & b) 2");
        CHECK_CONDITION(test_double(vec3[7], 5.84999999999999964472863211995E0) == 0, "static inline Vec8d if_add (Vec8b const & f, Vec8d const & a, Vec8d const & b) 3");
        CHECK_CONDITION(test_double(vec3[6], 3.14000000000000012434497875802E0) == 0, "static inline Vec8d if_add (Vec8b const & f, Vec8d const & a, Vec8d const & b) 4");
    }

    // Conditional multiply: For all vector elements i: result[i] = f[i] ? (a[i] * b[i]) : a[i]
    // static inline Vec8d if_mul (Vec8b const & f, Vec8d const & a, Vec8d const & b)
    {
        Vec8b vec0 = 0x81;
        Vec8d vec1 = 3.14;
        Vec8d vec2 = 2.71;
        Vec8d vec3 = if_mul(vec0, vec1, vec2);
        //print_all_elements((void*)&vec3, VECTYPE_8D);
        CHECK_CONDITION(test_double(vec3[0],  8.5093999999999994088284438476E0) == 0,  "static inline Vec8d if_mul (Vec8b const & f, Vec8d const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_double(vec3[1],  3.14000000000000012434497875802E0) == 0, "static inline Vec8d if_mul (Vec8b const & f, Vec8d const & a, Vec8d const & b) 2");
        CHECK_CONDITION(test_double(vec3[15], 8.5093999999999994088284438476E0) == 0,  "static inline Vec8d if_mul (Vec8b const & f, Vec8d const & a, Vec8d const & b) 3");
        CHECK_CONDITION(test_double(vec3[14], 3.14000000000000012434497875802E0) == 0, "static inline Vec8d if_mul (Vec8b const & f, Vec8d const & a, Vec8d const & b) 4");
    }

    // General arithmetic functions, etc.

    // Horizontal add: Calculates the sum of all vector elements.
    // static inline double horizontal_add (Vec8d const & a)
    {
        Vec8d vec0 = Vec8d(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8);
        double d = horizontal_add(vec0);
        CHECK_CONDITION(test_double(d, 3.6) == 0, "static inline double horizontal_add (Vec8d const & a) 1");
    }

    // function max: a > b ? a : b
    // static inline Vec8d max(Vec8d const & a, Vec8d const & b) 
    {
        Vec8d vec0 = 3.14;
        Vec8d vec1 = 9999.9999;
        Vec8d vec2 = max(vec0, vec1);
        Vec8d vec3 = max(vec1, vec0);
        CHECK_CONDITION(test_vector_with_double(vec2, 9999.9999) == 0, "static inline Vec8d max(Vec8d const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_vector_with_double(vec3, 9999.9999) == 0, "static inline Vec8d max(Vec8d const & a, Vec8d const & b) 2");
    }

    // function min: a < b ? a : b
    // static inline Vec8d min(Vec8d const & a, Vec8d const & b)
    {
        Vec8d vec0 = 3.14;
        Vec8d vec1 = 9999.9999;
        Vec8d vec2 = min(vec0, vec1);
        Vec8d vec3 = min(vec1, vec0);
        CHECK_CONDITION(test_vector_with_double(vec2, 3.14) == 0, "static inline Vec8d min(Vec8d const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_vector_with_double(vec3, 3.14) == 0, "static inline Vec8d min(Vec8d const & a, Vec8d const & b) 2");
    }

    // function abs: absolute value
    // Removes sign bit, even for -0.0f, -INF and -NAN
    // static inline Vec8d abs(Vec8d const & a)
    {
        Vec8d vec0 = -297147.12298;
        Vec8d vec1 = 120398.1298;
        Vec8d vec2 = abs(vec0);
        Vec8d vec3 = abs(vec1);
        CHECK_CONDITION(test_vector_with_double(vec2, 297147.12298) == 0, "static inline Vec8d abs(Vec8d const & a) 1");
        CHECK_CONDITION(test_vector_with_double(vec1, 120398.1298) == 0,  "static inline Vec8d abs(Vec8d const & a) 3");
    }

    // function sqrt: square root
    // static inline Vec8d sqrt(Vec8d const & a)
    {
        Vec8d vec0 = 12345.6789;
        Vec8d vec1 = -12345.6789;
        Vec8d vec2 = sqrt(vec0);
        Vec8d vec3 = sqrt(vec1);
        Vec8b vec4 = is_nan(vec2);
        Vec8b vec5 = is_nan(vec3);
        /*print_all_elements((void*)&vec2, VECTYPE_8D);
        print_all_elements((void*)&vec3, VECTYPE_8D);
        print_all_elements((void*)&vec4, VECTYPE_8B);
        print_all_elements((void*)&vec5, VECTYPE_8B);*/
        CHECK_CONDITION(test_vector_with_double(vec2, 1.11111110605555566621660545934E2) == 0,  "static inline Vec8d sqrt(Vec8d const & a) 1");
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec4, VECTYPE_8B, 0x00) == 0,          "static inline Vec8d sqrt(Vec8d const & a) 3");
        CHECK_CONDITION(test_vector_with_pattern((void*)&vec5, VECTYPE_8B, 0xFF) == 0,          "static inline Vec8d sqrt(Vec8d const & a) 4");
    }

    // function square: a * a
    // static inline Vec8d square(Vec8d const & a)
    {
        Vec8d vec0 = 18.45;
        Vec8d vec1 = -18.45;
        Vec8d vec2 = square(vec0); // exact value: 340.4025
        Vec8d vec3 = square(vec1); // exact value: 340.4025
        /* print_all_elements((void*)&vec2, VECTYPE_8D);
        print_all_elements((void*)&vec3, VECTYPE_8D); */
        CHECK_CONDITION(test_vector_with_double(vec2, 3.40402499999999974988895701244E2) == 0, "static inline Vec8d square(Vec8d const & a) 1");
        CHECK_CONDITION(test_vector_with_double(vec3, 3.40402499999999974988895701244E2) == 0, "static inline Vec8d square(Vec8d const & a) 2");
    }
 

    // pow(Vec4d, int):
    // Raise floating point numbers to integer power n
    // static inline Vec8d pow(Vec8d const & a, int n)
    {
        Vec8d vec0 = 18.45;
        Vec8d vec1 = pow(vec0, 2); // exact value: 340.4025
        Vec8d vec2 = pow(vec0, 8); // exact value: 13426751896.2434672750390625
        Vec8d vec3 = pow(-vec0, 8); // exact value: 13426751896.2434672750390625
        /* print_all_elements((void*)&vec1, VECTYPE_8D);
        print_all_elements((void*)&vec2, VECTYPE_8D);
        print_all_elements((void*)&vec3, VECTYPE_8D); */
        CHECK_CONDITION(test_vector_with_double(vec1, 3.40402499999999974988895701244E2) == 0, "static inline Vec8d pow(Vec8d const & a, int n) 1");
        CHECK_CONDITION(test_vector_with_double(vec2, 1.34267518962434635162353515625E10) == 0, "static inline Vec8d pow(Vec8d const & a, int n) 2");
        CHECK_CONDITION(test_vector_with_double(vec3, 1.34267518962434635162353515625E10) == 0, "static inline Vec8d pow(Vec8d const & a, int n) 3");
    }

    // Raise floating point numbers to integer power n, where n is a compile-time constant
    // template <int n>
    // static inline Vec8d pow_n(Vec8d const & a)
    {
        Vec8d vec0 = 18.45;
        Vec8d vec1 = pow_n<2>(vec0); // exact value: 340.4025
        Vec8d vec2 = pow_n<8>(vec0); // exact value: 13426751896.2434672750390625
        Vec8d vec3 = pow_n<8>(-vec0); // exact value: 13426751896.2434672750390625
        /*print_all_elements((void*)&vec1, VECTYPE_8D);
        print_all_elements((void*)&vec2, VECTYPE_8D);
        print_all_elements((void*)&vec3, VECTYPE_8D); */
        CHECK_CONDITION(test_vector_with_double(vec1, 3.40402499999999974988895701244E2) == 0, "static inline Vec8d pow_n(Vec8d const & a) 1");
        CHECK_CONDITION(test_vector_with_double(vec2, 1.34267518962434635162353515625E10) == 0, "static inline Vec8d pow_n(Vec8d const & a) 2");
        CHECK_CONDITION(test_vector_with_double(vec3, 1.34267518962434635162353515625E10) == 0, "static inline Vec8d pow_n(Vec8d const & a) 3");
    } 

    // function round: round to nearest integer (even). (result as double vector)
    // static inline Vec8d round(Vec8d const & a)
    {
        Vec8d vec0 = 18.45;
        Vec8d vec1 = 18.85;
        Vec8d vec2 = -3.14;
        Vec8d vec3 = -2.71;
        Vec8d vec4 = round(vec0);
        Vec8d vec5 = round(vec1);
        Vec8d vec6 = round(vec2);
        Vec8d vec7 = round(vec3);
        CHECK_CONDITION(test_vector_with_double(vec4, 18.0) == 0, "static inline Vec8d round(Vec8d const & a) 1");
        CHECK_CONDITION(test_vector_with_double(vec5, 19.0) == 0, "static inline Vec8d round(Vec8d const & a) 2");
        CHECK_CONDITION(test_vector_with_double(vec6, -3.0) == 0, "static inline Vec8d round(Vec8d const & a) 3");
        CHECK_CONDITION(test_vector_with_double(vec7, -3.0) == 0, "static inline Vec8d round(Vec8d const & a) 4");
    }

    // function truncate: round towards zero. (result as double vector)
    // static inline Vec8d truncate(Vec8d const & a)
    {
        Vec8d vec0 = 18.45;
        Vec8d vec1 = 18.85;
        Vec8d vec2 = -3.14;
        Vec8d vec3 = -2.71;
        Vec8d vec4 = truncate(vec0);
        Vec8d vec5 = truncate(vec1);
        Vec8d vec6 = truncate(vec2);
        Vec8d vec7 = truncate(vec3);
        CHECK_CONDITION(test_vector_with_double(vec4, 18.0) == 0, "static inline Vec8d truncate(Vec8d const & a) 1");
        CHECK_CONDITION(test_vector_with_double(vec5, 18.0) == 0, "static inline Vec8d truncate(Vec8d const & a) 2");
        CHECK_CONDITION(test_vector_with_double(vec6, -3.0) == 0, "static inline Vec8d truncate(Vec8d const & a) 3");
        CHECK_CONDITION(test_vector_with_double(vec7, -2.0) == 0, "static inline Vec8d truncate(Vec8d const & a) 4");
    }

    // function floor: round towards minus infinity. (result as double vector)
    // static inline Vec8d floor(Vec8d const & a)
    {
        Vec8d vec0 = 18.45;
        Vec8d vec1 = 18.85;
        Vec8d vec2 = -3.14;
        Vec8d vec3 = -2.71;
        Vec8d vec4 = floor(vec0);
        Vec8d vec5 = floor(vec1);
        Vec8d vec6 = floor(vec2);
        Vec8d vec7 = floor(vec3);
        CHECK_CONDITION(test_vector_with_double(vec4, 18.0) == 0, "static inline Vec8d floor(Vec8d const & a) 1");
        CHECK_CONDITION(test_vector_with_double(vec5, 18.0) == 0, "static inline Vec8d floor(Vec8d const & a) 2");
        CHECK_CONDITION(test_vector_with_double(vec6, -4.0) == 0, "static inline Vec8d floor(Vec8d const & a) 3");
        CHECK_CONDITION(test_vector_with_double(vec7, -3.0) == 0, "static inline Vec8d floor(Vec8d const & a) 4");
    }

    // function ceil: round towards plus infinity. (result as double vector)
    // static inline Vec8d ceil(Vec8d const & a)
    {
        Vec8d vec0 = 18.45;
        Vec8d vec1 = 18.85;
        Vec8d vec2 = -3.14;
        Vec8d vec3 = -2.71;
        Vec8d vec4 = ceil(vec0);
        Vec8d vec5 = ceil(vec1);
        Vec8d vec6 = ceil(vec2);
        Vec8d vec7 = ceil(vec3);
        CHECK_CONDITION(test_vector_with_double(vec4, 19.0) == 0, "static inline Vec8d ceil(Vec8d const & a) 1");
        CHECK_CONDITION(test_vector_with_double(vec5, 19.0) == 0, "static inline Vec8d ceil(Vec8d const & a) 2");
        CHECK_CONDITION(test_vector_with_double(vec6, -3.0) == 0, "static inline Vec8d ceil(Vec8d const & a) 3");
        CHECK_CONDITION(test_vector_with_double(vec7, -2.0) == 0, "static inline Vec8d ceil(Vec8d const & a) 4");
    }

    // function round_to_int: round to nearest integer (even). (result as integer vector)
    // The only way to represent Vec8i is to use Vec16i
    // static inline Vec16i round_to_int(Vec8d const & a)
    {
        Vec8d vec0 = 18.45;
        Vec8d vec1 = 18.85;
        Vec8d vec2 = -3.14;
        Vec8d vec3 = -2.71;
        Vec8q vec4 = round_to_int(vec0);
        Vec8q vec5 = round_to_int(vec1);
        Vec8q vec6 = round_to_int(vec2);
        Vec8q vec7 = round_to_int(vec3);
        CHECK_CONDITION(test_all_elements((void*)&vec4, VECTYPE_8Q, 18) == 0, "static inline Vec16i round_to_int(Vec8d const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec5, VECTYPE_8Q, 19) == 0, "static inline Vec16i round_to_int(Vec8d const & a) 2");
        CHECK_CONDITION(test_all_elements((void*)&vec6, VECTYPE_8Q, -3) == 0, "static inline Vec16i round_to_int(Vec8d const & a) 3");
        CHECK_CONDITION(test_all_elements((void*)&vec7, VECTYPE_8Q, -3) == 0, "static inline Vec16i round_to_int(Vec8d const & a) 4");
    }

    // static inline Vec16i truncate_to_int(Vec8d const & a, Vec8d const & b)
    {
        Vec8d vec0 = 18.45;
        Vec8d vec1 = 18.85;
        Vec8d vec2 = -3.14;
        Vec8d vec3 = -2.71;
        Vec16i vec4 = truncate_to_int(vec0, vec0);
        Vec16i vec5 = truncate_to_int(vec1, vec1);
        Vec16i vec6 = truncate_to_int(vec2, vec2);
        Vec16i vec7 = truncate_to_int(vec3, vec3);
        CHECK_CONDITION(test_all_elements((void*)&vec4, VECTYPE_16I, 18) == 0, "static inline Vec16i truncate_to_int(Vec8d const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec5, VECTYPE_16I, 18) == 0, "static inline Vec16i truncate_to_int(Vec8d const & a, Vec8d const & b) 2");
        CHECK_CONDITION(test_all_elements((void*)&vec6, VECTYPE_16I, -3) == 0, "static inline Vec16i truncate_to_int(Vec8d const & a, Vec8d const & b) 3");
        CHECK_CONDITION(test_all_elements((void*)&vec7, VECTYPE_16I, -2) == 0, "static inline Vec16i truncate_to_int(Vec8d const & a, Vec8d const & b) 4");
    }

    // function to_double: convert low part of integer vector to double vector
    // static inline Vec8d to_double_low(Vec16i const & a) 
    {
        Vec16i vec0 = 18;
        Vec16i vec1 = 12131;
        Vec16i vec2 = -3;
        Vec16i vec3 = -1249121;
        Vec8d vec4 = to_double_low(vec0);
        Vec8d vec5 = to_double_low(vec1);
        Vec8d vec6 = to_double_low(vec2);
        Vec8d vec7 = to_double_low(vec3);
        CHECK_CONDITION(test_vector_with_double(vec4, 18.0) == 0, "static inline Vec8d to_double_low(Vec16i const & a) 1");
        CHECK_CONDITION(test_vector_with_double(vec5, 12131.0) == 0, "static inline Vec8d to_double_low(Vec16i const & a) 2");
        CHECK_CONDITION(test_vector_with_double(vec6, -3.0) == 0, "static inline Vec8d to_double_low(Vec16i const & a) 3");
        CHECK_CONDITION(test_vector_with_double(vec7, -1249121.0) == 0, "static inline Vec8d to_double_low(Vec16i const & a) 4");
    }

    // function compress: convert two Vec8d to one Vec16f
    // static inline Vec16f compress (Vec8d const & low, Vec8d const & high)
    {
        Vec16f vec0 = 1.2;
        Vec8d  vec1 = 3.14;
        Vec8d  vec2 = -2.71;
        vec0 = compress(vec1, vec2);
        /*print_all_elements((void*)&vec0, VECTYPE_16F);
        print_all_elements((void*)&vec1, VECTYPE_8D);
        print_all_elements((void*)&vec2, VECTYPE_8D); */
        CHECK_CONDITION(test_float(vec0[7], 3.14) == 0, "static inline Vec16f compress (Vec8d const & low, Vec8d const & high) 1");
        CHECK_CONDITION(test_float(vec0[8], -2.71) == 0, "static inline Vec16f compress (Vec8d const & low, Vec8d const & high) 2");
    }

    // Function extend_low : convert Vec16f vector elements 0 - 7 to Vec8d
    // static inline Vec8d extend_low(Vec16f const & a)
    {
        Vec8d vec0 = 3.14;
        Vec16f vec1 = Vec16f(1.25, 1.25, 1.25, 1.25, 1.25, 1.25, 1.25, 1.25, -4865.21, -4865.21, -4865.21, -4865.21, -4865.21, -4865.21, -4865.21, -4865.21);
        vec0 = extend_low(vec1);
        CHECK_CONDITION(test_vector_with_double(vec0, 1.25) == 0, "static inline Vec8d extend_low(Vec16f const & a) 1");
    }

    // Function extend_high : convert Vec8f vector elements 8 - 15 to Vec8d
    // static inline Vec8d extend_high (Vec16f const & a) 
    {
        Vec8d vec0 = 3.14;
        Vec16f vec1 = Vec16f(1.25, 1.25, 1.25, 1.25, 1.25, 1.25, 1.25, 1.25, -4865.21, -4865.21, -4865.21, -4865.21, -4865.21, -4865.21, -4865.21, -4865.21);
        vec0 = extend_high(vec1);
        CHECK_CONDITION(test_vector_with_double(vec0, -4865.21) == 0, "static inline Vec8d extend_high(Vec16f const & a) 1");
    }

    // Function sign_bit: gives true for elements that have the sign bit set
    // even for -0.0, -INF and -NAN
    // Note that sign_bit(Vec8d(-0.0)) gives true, while Vec8d(-0.0) < Vec8d(0.0) gives false
    // static inline Vec8b sign_bit(Vec8d const & a)
    {
        Vec8d vec0 = 3.14;
        Vec8d vec1 = -0.0;
        Vec8d vec2 = -3.14;
        Vec8b vec3 = sign_bit(vec0);
        Vec8b vec4 = sign_bit(vec1);
        Vec8b vec5 = sign_bit(vec2);
        print_all_elements((void*)&vec3, VECTYPE_8B);
        print_all_elements((void*)&vec4, VECTYPE_8B);
        print_all_elements((void*)&vec5, VECTYPE_8B); 
        CHECK_CONDITION(test_all_elements((void*)&vec3, VECTYPE_8B, 0) == 0, "static inline Vec8b sign_bit(Vec8d const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec4, VECTYPE_8B, 1) == 0, "static inline Vec8b sign_bit(Vec8d const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec5, VECTYPE_8B, 1) == 0, "static inline Vec8b sign_bit(Vec8d const & a) 1")
    }

    // Function sign_combine: changes the sign of a when b has the sign bit set
    // same as select(sign_bit(b), -a, a)
    // static inline Vec8d sign_combine(Vec8d const & a, Vec8d const & b)
    {
        Vec8d vec0 = 3.14;
        Vec8d vec1 = 7.28;
        Vec8d vec2 = -2.71;
        Vec8d vec3 = sign_combine(vec0, vec1);
        Vec8d vec4 = sign_combine(vec0, vec2);
        CHECK_CONDITION(test_vector_with_double(vec3, 3.14) == 0, "static inline Vec8d sign_combine(Vec8d const & a, Vec8d const & b) 1");
        CHECK_CONDITION(test_vector_with_double(vec4, -3.14) == 0, "static inline Vec8d sign_combine(Vec8d const & a, Vec8d const & b) 1");
    }

    // Function is_finite: gives true for elements that are normal, denormal or zero, 
    // false for INF and NAN
    // static inline Vec8b is_finite(Vec8d const & a)
    {
        Vec8d vec0 = infinite8d();
        Vec8d vec1 = nan8d();
        Vec8d vec2 = 7.2;
        Vec8b vec3 = is_finite(vec0);
        Vec8b vec4 = is_finite(vec1);
        Vec8b vec5 = is_finite(vec2);
        print_all_elements((void*)&vec3, VECTYPE_8B);
        print_all_elements((void*)&vec4, VECTYPE_8B);   
        CHECK_CONDITION(test_all_elements((void*)&vec3, VECTYPE_8B, 0) == 0, "static inline Vec8b is_finite(Vec8d const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec4, VECTYPE_8B, 0) == 0, "static inline Vec8b is_finite(Vec8d const & a) 2");
        CHECK_CONDITION(test_all_elements((void*)&vec5, VECTYPE_8B, 1) == 0, "static inline Vec8b is_finite(Vec8d const & a) 3");
    }

    // Function is_inf: gives true for elements that are +INF or -INF
    // false for finite numbers and NAN
    // static inline Vec8b is_inf(Vec8d const & a)
    {
        Vec8d vec0 = infinite8d();
        Vec8d vec1 = nan8d();
        Vec8d vec2 = 7.2;
        Vec8b vec3 = is_inf(vec0);
        Vec8b vec4 = is_inf(vec1);
        Vec8b vec5 = is_inf(vec2);
        CHECK_CONDITION(test_all_elements((void*)&vec3, VECTYPE_8B, 1) == 0, "static inline Vec8b is_inf(Vec8d const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec4, VECTYPE_8B, 0) == 0, "static inline Vec8b is_inf(Vec8d const & a) 2");
        CHECK_CONDITION(test_all_elements((void*)&vec5, VECTYPE_8B, 0) == 0, "static inline Vec8b is_inf(Vec8d const & a) 3");
    }

    // Function is_nan: gives true for elements that are +NAN or -NAN
    // false for finite numbers and +/-INF
    // static inline Vec8b is_nan(Vec8d const & a)
    {
        Vec8d vec0 = infinite8d();
        Vec8d vec1 = nan8d();
        Vec8d vec2 = 7.2;
        Vec8b vec3 = is_nan(vec0);
        Vec8b vec4 = is_nan(vec1);
        Vec8b vec5 = is_nan(vec2);
        CHECK_CONDITION(test_all_elements((void*)&vec3, VECTYPE_8B, 0) == 0, "static inline Vec8b is_nan(Vec8d const & a) 1");
        CHECK_CONDITION(test_all_elements((void*)&vec4, VECTYPE_8B, 1) == 0, "static inline Vec8b is_nan(Vec8d const & a) 2");
        CHECK_CONDITION(test_all_elements((void*)&vec5, VECTYPE_8B, 0) == 0, "static inline Vec8b is_nan(Vec8d const & a) 3");
    }

    // Function is_subnormal: gives true for elements that are denormal (subnormal)
    // false for finite numbers, zero, NAN and INF
    // static inline Vec8b is_subnormal(Vec8d const & a)
        // TODO: 

    // Function infinite2d: returns a vector where all elements are +INF
    // static inline Vec8d infinite8d()
        // Tested implicitly 

    // Function nan2d: returns a vector where all elements are +NAN (quiet)
    // static inline Vec8d nan8d(int n = 0x10)
        // Tested implicitly 

    // change signs on vectors Vec4d
    // Each index i0 - i3 is 1 for changing sign on the corresponding element, 0 for no change
    // template <int i0, int i1, int i2, int i3, int i4, int i5, int i6, int i7, int i8>
    // static inline Vec8d change_sign(Vec8d const & a) 
    {
        Vec8d vec0 = infinite8d();
        Vec8d vec1 = -3.14;
        Vec8d vec2 = 7.2;
        Vec8d vec3 = 0;//change_sign(vec0);
        Vec8d vec4 = 0;//change_sign(vec1);
        Vec8d vec5 = 0;//change_sign(vec2);
        CHECK_CONDITION(test_vector_with_double(vec3, -infinite8d()[0]) == 0, "static inline Vec8d change_sign(Vec8d const & a) 1");
        CHECK_CONDITION(test_vector_with_double(vec4, 3.14) == 0,             "static inline Vec8d change_sign(Vec8d const & a) 2");
        CHECK_CONDITION(test_vector_with_double(vec5, -7.2) == 0,             "static inline Vec8d change_sign(Vec8d const & a) 3");
    }
}

/***************************************************************************************
****************************************************************************************
*
*   Helper functions
*
****************************************************************************************
***************************************************************************************/

/****************************************************************************
*   Test single vector element with a given value.
*
*   returns 0 - when tested element is equal to given value
*   
*   Ranges of values for different vector types:
*   Vec512b - value should be '0' or '1', tests each bit separately
*   Vec16b  - value should be '0' or '1', tests each bit separately
*   Vec8b   - value should be '0' or '1', tests each bit separately
*
*****************************************************************************/
int test_element(void * vector, VECTYPE type, uint64_t value, int index)
{
    int retval = 1;
    switch(type)
    {
    case VECTYPE_512B:
        Vec512b *vec512b = (Vec512b *)vector;
        retval = 0;
        if(vec512b->get_bit(index) != ((int)value))
        {
            retval = -1;
        }
        break;
    case VECTYPE_16B:
        Vec16b *vec16b = (Vec16b *)vector;
        retval = 0;
        if(vec16b[index] != (uint32_t)value)
        {
            retval = -1;
        }
        break;
    case VECTYPE_16I:
        Vec16i *vec16i = (Vec16i *)vector;
        int32_t *value_ptr16i = (int32_t *) &value;
        retval = 0;
        if(vec16i->extract(index) != *value_ptr16i)
        {
            if(g_supressMessages == false) printf("Error at index: %d value: %08X, vector: %08X\n", index, value, vec16i->extract(index));
            retval = -1;
        }
        break;
    case VECTYPE_16UI:
        Vec16ui *vec16ui = (Vec16ui *)vector;
        uint32_t *value_ptr16ui = (uint32_t *) &value;
        retval = 0;
        if(vec16ui->extract(index) != *value_ptr16ui)
        {
            if(g_supressMessages == false) printf("Error at index: %d value: %08X, vector: %08X\n", index, value, vec16ui->extract(index));
            retval = -1;
        }
        break;
    case VECTYPE_8Q:
        Vec8q *vec8q = (Vec8q *)vector;
        int64_t *value_ptr8q = (int64_t *) &value;
        retval = 0;
        if(vec8q->extract(index) != *value_ptr8q)
        {
            if(g_supressMessages == false) printf("Error at index: %d value: %08X, vector: %08X\n", index, value, vec8q->extract(index));
            retval = -1;
        }
        break;
    default:
        if(g_supressMessages == false) printf(" test_element: incorrect vector type!\n");
    }

    return retval;
}

/****************************************************************************
*   Test all vector elements with given value.
*
*   returns 0 when all elements have the same value equal tu 'uint64_t value'
*   
*   Ranges of values for different vector types:
*   Vec512b - value should be '0' or '1', tests each bit separately
*   Vec16b  - value should be '0' or '1', tests each bit separately
*   Vec8b   - value should be '0' or '1', tests each bit separately
*
*****************************************************************************/
int test_all_elements(void * vector, VECTYPE type, uint64_t value)
{
    int retval = 1;

    switch(type)
    {
    case VECTYPE_512B:
        Vec512b *vec512b = (Vec512b *)vector;
        retval = 0;
        for(int index = 0; index < 512; index++)
        {
            if(vec512b->get_bit(index) != ((int)value))
            {
                retval = -1;
                break;
            }
        }
        break;
    case VECTYPE_16B:
        Vec16b *vec16b = (Vec16b *)vector;

        // Check input argument:
        if((value != 0x0) && (value != 0x1))
        {
            break;
        }

        retval = 0;
        for(int index = 0; index < 16; index++)
        {
            if(vec16b->get_bit(index) != (int)value)
            {
                retval = -1;
                break;
            }
        }
        break;
    case VECTYPE_8B:
        Vec8b *vec8b = (Vec8b *)vector;

        // Check input argument:
        if((value != 0x0) && (value != 0x1))
        {
            break;
        }

        retval = 0;
        for(int index = 0; index < 8; index++)
        {
            if(vec8b->get_bit(index) != (int)value)
            {
                retval = -1;
                break;
            }
        }
        break;
    case VECTYPE_16I:
        Vec16i *vec16i = (Vec16i *)vector;
        int32_t value16;
        int32_t *value_ptr = (int32_t *) &value;
        retval = 0;
        for(int index = 0; index < 16; index++)
        {
            value16 = vec16i->extract(index);
            if(value16 != (*value_ptr))
            {
                retval = -1;
                break;
            }
        }
        break;
    case VECTYPE_16UI:
        Vec16ui *vec16ui = (Vec16ui *)vector;
        uint32_t value16u;
        uint32_t *value16u_ptr = (uint32_t *) &value;
        retval = 0;
        for(int index = 0; index < 16; index++)
        {
            value16u = vec16ui->extract(index);
            if(value16u != (*value16u_ptr))
            {
                retval = -1;
                break;
            }
        }
        break;
    case VECTYPE_8Q:
        Vec8q *vec8q = (Vec8q *)vector;
        int64_t value8q;
        int64_t *value8q_ptr = (int64_t *)&value;
        retval = 0;
        for(int index = 0; index < 8; index++)
        {
           value8q = vec8q->extract(index);
           if(value8q != (*value8q_ptr))
           {
               retval = -1;
               break;
           }
        }
        break;
    default:
        if(g_supressMessages == false) printf(" test_all_elements: incorrect vector type!\n");
        break;
    }
    
    return retval;
}

/****************************************************************************
*   Test vector elements with values given in an array.
*
*   returns 0 when all elements have the same values as values from array
*   
*****************************************************************************/
int test_vector_with_array(void *vector, VECTYPE type, void *arr)
{
    int retval = 1;

    do
    {
        if(vector == NULL || arr == NULL)
        {
            if(g_supressMessages == false) printf(" test_vector_with_array: incorrect argument\n");
            break;
        }

        switch(type)
        {
        case VECTYPE_16I:
            int32_t *ptr_16i = (int32_t *)arr;
            retval = 0;
            for(int i = 0; i < 16; i++)
            {
                if(test_element(vector, type, ptr_16i[i], i) != 0)
                {
                    retval = -1;
                    break;
                }
            }
            break;
        case VECTYPE_16UI:
            uint32_t *ptr_16ui = (uint32_t *)arr;
            retval = 0;
            for(int i = 0; i < 16; i++)
            {
                if(test_element(vector, type, ptr_16ui[i], i) != 0)
                {
                    retval = -1;
                    break;
                }
            }
            break;
        case VECTYPE_8Q:
            int64_t *ptr_8q = (int64_t *)arr;
            retval = 0;
            for(int i = 0; i < 8; i++)
            {
                if(test_element(vector, type, ptr_8q[i], i) != 0)
                {
                    retval = -1;
                    break;
                }
            }
            break;
        default:
            if(g_supressMessages == false) printf(" test_vector_with_array: incorrect vector type!\n");
            break;
        }
    } while(0);

    return retval;
}

/****************************************************************************
*   Test whole vector with repeating pattern
*
*   returns 0 when all elements have the same value equal tu 'uint64_t value'
*   
*   Ranges of values for different vector types:
*   Vec512b - tests using 64b wide repeating pattern
*   Vec16b  - trims uint64_t to uint16_t and compares with vector mask
*   Vec8b   - trims uint64_t to uint8_t and compares with vector mask
*
*****************************************************************************/
int test_vector_with_pattern(void * vector, VECTYPE type, uint64_t value)
{
    // Todo: move variables declaratin to the beginning of the function
    int retval = 1;
    switch(type)
    {
    case VECTYPE_512B:
        Vec512b *vec512b = (Vec512b *)vector;
        uint64_t pattern512b;
        retval = 0;
        for(int i = 0; i <8; i++)
        {
            pattern512b = 0;
            for(int j = 0; j < 64; j++)
            {
                //printf("bit %d: %08X\n", i*64+j,  vec512b->get_bit(i*64+j));
                uint64_t bit = (uint64_t)(vec512b->get_bit(i*64 + j));
                pattern512b |= bit << j;
            }
            if(pattern512b != value)
            {
                retval = -1;
                break;
            }
        }
        break;
    case VECTYPE_16B:
        Vec16b *vec16b = (Vec16b *)vector;
        uint16_t pattern16b = 0;
        retval = 0;
        for(int i = 0; i < 16; i++)
        {
            uint16_t bit = (uint16_t)(vec16b->get_bit(i));
            pattern16b |= bit << i;
        }
        if(pattern16b != (uint16_t)(value & 0xFFFF))
        {
            retval = -1;
            break;
        }
        break;
    case VECTYPE_8B:
        Vec8b *vec8b = (Vec8b *)vector;
        uint8_t pattern8b = 0;
        retval = 0;
        for(int i = 0; i < 8; i++)
        {
            uint8_t bit = (uint8_t)(vec8b->get_bit(i));
            pattern8b |= bit << i;
        }
        if(pattern8b != (uint8_t)(value & 0xFF))
        {
            retval = -1;
            break;
        }
        break;
    default:
        if(g_supressMessages == false) printf(" test_vector_with_pattern: incorrect vector type!\n");
        break;
    }

    return retval;
}

/****************************************************************************
*   Performs bitwise comparison on two float numbers.
*
*   returns 0 when both values are exact.
*   
*****************************************************************************/
int test_float(float x, float y)
{
    // Casting to hex is more convenient when debugging
    uint32_t x_hex = *((uint32_t *)&x);
    uint32_t y_hex = *((uint32_t *)&y);

    //printf("test_float: x: %f(%08X) y: %f(%08X)\n", x, x_hex, y, y_hex); 
    if(x_hex == y_hex) return 0;
    else return -1;
}

/****************************************************************************
*   Performs bitwise comparison on two double numbers.
*
*   returns 0 when both values are exact.
*   
*****************************************************************************/
int test_double(double x, double y)
{
    // Casting to hex is more convenient when debugging
    uint64_t x_hex = *((uint64_t *)&x);
    uint64_t y_hex = *((uint64_t *)&y);

    /*uint32_t x_hex_hi =*(((uint32_t *)&x)+1);
    uint32_t y_hex_hi =*(((uint32_t *)&y)+1);

    printf("test_float: x: %32f(%08X%08X) y: %32f(%08X%08X)\n", x,x_hex_hi, x_hex, y, y_hex_hi,y_hex);*/
    if(x_hex == y_hex) return 0;
    else return -1;
}

/****************************************************************************
*   Performs exact test for float values in the given vector
*
*   returns 0 when all elements have the same bit pattern as 'float value'
*
*****************************************************************************/
int test_vector_with_float(Vec16f & vec, float value)
{
    int retval= 0;
    for(int i = 0; i < 16; i++)
    {
        if(test_float(vec[i], value) != 0)
        {
            retval = -1;
            break;
        }
    }

    return retval;
}

/****************************************************************************
*   Performs exact test for double values in the given vector
*
*   returns 0 when all elements have the same bit pattern as 'double value'
*
*****************************************************************************/
int test_vector_with_double(Vec8d & vec, double value)
{
    int retval= 0;
    
    for(int i = 0; i < 8; i++)
    {
        if(test_double(vec[i], value) != 0)
        {
            retval = -1;
            break;
        }
    }

    return retval;
}

/****************************************************************************
*   Fills vector with 64b pattern
*   
*   Ranges of values for different vector types:
*   Vec512b - value should be '0' or '1', sets each bit separately, extends 64b in a repetitive pattern
*   Vec16b  - value should be '0' or '1', sets each bit separately, trims pattern to uint16_t
*   Vec8b   - value should be '0' or '1', sets each bit separately, trims pattern to uint8_t
*
*****************************************************************************/
void fill_vector_with_pattern(void *vector, VECTYPE type, uint64_t pattern)
{
    switch(type)
    {
    case VECTYPE_512B:
        Vec512b *vec = (Vec512b *)vector;
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 64; j++)
            {
                int bit = (pattern >> j) & 1;
                vec->set_bit(i*64 + j, bit);
            }
        }
        break;
    case VECTYPE_16B:
    case VECTYPE_8B:
        if(g_supressMessages == false) printf(" fill_vector_with_pattern: use constructor/assignment operator for this vector type instead!\n");
        break;
    default:
        if(g_supressMessages == false) printf(" fill_vector_with_pattern: incorrect vector type!\n");
    }
}

/****************************************************************************
*   Print the content of a vector for debugging purposes
*   
*   Ranges of values for different vector types:
*   Vec512b - values packed in 32b DWORDS LSb to the right
*   Vec16b  - values as single bits
*   Vec8b   - values as single bits
*
*****************************************************************************/
void print_all_elements(void * vector, VECTYPE type)
{
    switch(type)
    {
    case VECTYPE_512B:
        Vec512b *vec512b = (Vec512b *)vector;
        for(int i = 0; i < 16; i++)
        {
            uint32_t k = 0;
            for(int j = 0; j <32; j++)
            {
                if(vec512b->get_bit(i*32 + j) == 1 ) k |= (1 << j);
            }
            if(g_supressMessages == false) printf(" Vec512 DWord %d: %08X\n", i, k);
        }
        break;
    case VECTYPE_16B:
        Vec16b *vec16b = (Vec16b *)vector;
        uint16_t mask16 = 0;
        uint16_t bit16 = 0;
        for(int i = 0; i < 16; i++)
        {
            bit16 = vec16b->get_bit(i);
            mask16 |= bit16 << i;
            if(g_supressMessages == false) printf(" Vec16b[%d]: %d\n", i, bit16);
        }
        if(g_supressMessages == false) printf(" Vec16b: %08X\n", mask16);
        break;
    case VECTYPE_8B:
        Vec8b *vec8b = (Vec8b *)vector;
        uint8_t mask8 = 0;
        uint8_t bit8 = 0;
        for(int i = 0; i < 8; i++)
        {
            bit8 = vec8b->get_bit(i);
            mask8 |= bit8 << i;
            if(g_supressMessages == false) printf(" Vec8b[%d]: %08X\n", i, bit8);
        }
        break;  
    case VECTYPE_16I:
        Vec16i *vec16i = (Vec16i *)vector;

        for(int i = 0; i < 16; i++)
        {
            if(g_supressMessages == false) printf(" Vec16i[%d]: %08X (%d)\n", i, vec16i->extract(i), vec16i->extract(i));
        }
        break;  
    case VECTYPE_16UI:
        Vec16ui *vec16ui = (Vec16ui *)vector;

        for(int i = 0; i < 16; i++)
        {
            if(g_supressMessages == false) printf(" Vec16i[%d]: %08X (%u)\n", i, vec16ui->extract(i), vec16ui->extract(i));
        }
        break;
    case VECTYPE_16F:
        Vec16f *vec16f = (Vec16f *)vector;
        float vec16f_value;
        uint32_t *vec16f_hex;
        for(int i = 0; i < 16; i++)
        {
            vec16f_value = vec16f->extract(i);
            vec16f_hex = (uint32_t *)&vec16f_value; // reinterpret floating point as hexadecimal representation
            if(g_supressMessages == false) printf(" Vec16f[%d]: %f (%08X)\n", i, vec16f->extract(i), *vec16f_hex);
        }
        break;
    case VECTYPE_8Q:
        Vec8q *vec8q = (Vec8q *)vector;
        int64_t vec8q_value;
        uint32_t *vec8q_hex_hi;
        uint32_t *vec8q_hex_lo;
        for(int i = 0; i <8; i++)
        {
            vec8q_value = vec8q->extract(i);
            vec8q_hex_lo = (uint32_t *)&vec8q_value; // reinterpret as hex
            vec8q_hex_hi = ((uint32_t *)&vec8q_value) + 1;
            if(g_supressMessages == false) printf(" Vec8q[%d]: 0x%08X%08X\n", i, *vec8q_hex_hi, *vec8q_hex_lo);
        }
        break;
    case VECTYPE_8D:
        Vec8d *vec8d = (Vec8d *)vector;
        double vec8d_value;
        uint32_t *vec8d_hex_hi;
        uint32_t *vec8d_hex_lo;
        for(int i = 0; i <8; i++)
        {
            vec8d_value = vec8d->extract(i);
            vec8d_hex_lo = (uint32_t *)&vec8d_value; // reinterpret as hex
            vec8d_hex_hi = ((uint32_t *)&vec8d_value) + 1;
            if(g_supressMessages == false) printf(" Vec8d[%d]: %f (%08X%08X)\n", i, vec8d->extract(i), *vec8d_hex_hi, *vec8d_hex_lo);
        }
        break;
    default:
        if(g_supressMessages == false) printf(" print_all_elements: incorrect vector type!\n");
        break;
    }
}
