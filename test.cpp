#include <stdio.h>
#include "vectorclass.h"

/* Vec512ie_test()
{
    /*bool retval = true;
    class Vec512ie vec1;

    vec1.get_low();
    vec1.get_high();
    

    __m256i r1 = _mm256_set_epi32(0, 1, 2, 3, 4 ,5 ,6 ,7);
    __m256i r2 = _mm256_set_epi32(8, 9, 10, 11, 12, 13, 14, 15);
        

    __m256i r3 =_mm256_add_epi8(r1, r2);

    
    return r3;
}*/

Vec128b Vec128b_test()
{
    uint8_t memory[20] = { 1, 2, 3 ,4 ,5 ,6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19};

    Vec128b vec1 = Vec128b(7, 8);
    Vec128b vec2 = Vec128b();
    Vec128b vec3 = Vec128b();

    vec2.load((void *)(memory + 3));
    vec2.store((void *)(memory));
    vec3.load_a((void *)(memory));
    vec3.store_a((void*)(memory + 2));

    Vec128b vec4 = Vec128b();
    vec4.set_bit(17);
    vec4.set_bit(120);

    int b = vec4.get_bit(120);

    printf("vec1: %lld  %lld\n (expected: 7 8)\n", vec1.get_low(), vec1.get_high());
    printf("vec2: %lld  %lld\n (expected: 795458214266537220 1374179596971150604)\n", vec2.get_low(), vec2.get_high());
    printf("vec3: %lld  %lld\n (expected: )\n", vec3.get_low(), vec3.get_high());
    printf("vec4: %lld  %lld\n (expected: 131072 72057594037927936)", vec4.get_low(), vec4.get_high());

    printf("memory: ");
    for(int i = 0; i < 20; i++)
    {
        printf("%d ", memory[i]);
    }

    printf("\nVec128b[]: ");
    for(int i = 0; i < 8; i++)
    {
        for(int j = 0; j < 16; j++)
        {
            printf("%d", vec4[i*16 + j]);
        }
    }

    return vec4;
}

void Vec16c_test()
{
    Vec16c vec1 = Vec16c();
    Vec16c vec2 = Vec16c(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
    Vec128b vecb1 = Vec128b(1234, 5678);
    Vec16c vec3 = Vec16c(vecb1);

    printf("vec1: %l %l (expected 0)\n", vec1.get_low(), vec1.get_high());
    printf("vec1: %l %l (expected)\n", vec2.get_low(), vec2.get_high());
    printf("vec3: %l %l (expected 1234, 5678)\n", vec3.get_low(), vec3.get_high());

}

int main()
{
    Vec128b_test();
    Vec16c_test();

    return 0;
}
