/***************************  testvectormath.cpp  ****************************
* Author:        Agner Fog
* Date created:  2014-04-22
* Last modified: 2014-04-22
* Project:       test vector math function
* Description:
* Test different implementations of vectorized mathematical functions
*
******************************************************************************/


/******************************************************************************
* You may set any of these compiler options for g++
* -O3       optimize
* -Ofast    better vectorization
* -msse2    instruction set SSE2
* -msse3    instruction set SSE3
* -mssse3   instruction set SSSE3
* -msse4.1  instruction set SSE4.1
* -mavx     instruction set avx
* -mavx2    instruction set avx2
* -mfma     fused multiply-and-add instructions 
* -mmic     instruction set Knights Corner (Intel Xeon Phi)
* 
* -ftree-vectorizer-verbose=1  report auto-vectorization
* -mveclibabi=svml             use Intel SVML library for auto-vectorization
* 
* link with libsvml.a 32/64 bit version if needed
*
* run multiple times to see if the timing values are stable
*
* set the desired values for the defines below:
*
******************************************************************************/

#ifndef VECTOR_TYPE
#define VECTOR_TYPE   6   //  1: vector of 4  float
                          //  2: vector of 8  float
                          //  3: vector of 16 float
                          //  4: vector of 2  double
                          //  5: vector of 4  double
                          //  6: vector of 8  double
#endif

#ifndef MAT_IMPLEMENT		
#define MAT_IMPLEMENT 5   //  1: compiler default
                          //  2: VDT inline library with possible auto-vectorization
                          //  3: inline library with explicit vectorization
                          //  4: external vector math library
                          //  5: inline library with explicit vectorization (Knights Corner-only)
#endif

#ifndef MAT_FUNCTION					
#define MAT_FUNCTION  5  //  1:  exp
                          //  2:  expm1
                          //  3:  log
                          //  4:  log1p
                          //  5:  pow(vector,vector)
                          //  6:  pow(vector,scalar)
                          //  10: sin
                          //  11: cos
                          //  12: tan
                          //  13: asin
                          //  14: acos
                          //  15: atan
#endif

// performance monitor counter:
//#define USEPMC              0x40000001    // core clock cycles (Intel only)
//#define USEPMC              1


#define USE_DENORMALS  0  // may be faster if 0 (no denormals)

#define WARMUP         1  // warmup dummy work

// number of x values in array
#define NUM_VALUES   100
// first x value
#define FIRST_VALUE  -11.5
// increments of x
#define STEP_VALUE   1.4  
// max values in output listing
#define MAX_LIST     12
// formats in output listing
#define XFORMAT "%8.3f"
#define YFORMAT "%15.7E"


// define types
#if VECTOR_TYPE == 1
#define stype float          // scalar type
#define vtype Vec4f          // vector type
#define numelements 4        // elements per vector
#elif VECTOR_TYPE == 2
#define stype float          // scalar type
#define vtype Vec8f          // vector type
#define numelements 8        // elements per vector
#elif VECTOR_TYPE == 3
#define stype float          // scalar type
#define vtype Vec16f         // vector type
#define numelements 16       // elements per vector
#elif VECTOR_TYPE == 4
#define stype double         // scalar type
#define vtype Vec2d          // vector type
#define numelements 2        // elements per vector
#elif VECTOR_TYPE == 5
#define stype double         // scalar type
#define vtype Vec4d          // vector type
#define numelements 4        // elements per vector
#elif VECTOR_TYPE == 6
#define stype double         // scalar type
#define vtype Vec8d          // vector type
#define numelements 8        // elements per vector
#else
#error unknown type
#endif

// define functions
#if MAT_FUNCTION == 1
#define FUNCNAME        "exp"
#define SCALAR_FUNC(x)  exp(x)             // scalar function, compiler default
#if VECTOR_TYPE < 4
#define VDT_FUNC(x)     vdt::fast_expf(x)  // VDT function, expect auto-vectorization
#else
#define VDT_FUNC(x)     vdt::fast_exp(x)   // same, double precision
#endif
#define VECT_FUNC(x)    exp(x)             // inline vector function
#define LIB_FUNC(x)     exp(x)             // external library vector function
#define LD_FUNC(x)      expl(x)             // long double precision scalar function

#elif MAT_FUNCTION == 2
#define FUNCNAME        "expm1"
#define SCALAR_FUNC(x)  expm1(x)
#if VECTOR_TYPE < 4
#define VDT_FUNC(x)     (vdt::fast_expf(x)-1.f)
#else
#define VDT_FUNC(x)     (vdt::fast_exp(x)-1.0)
#endif
#define VECT_FUNC(x)    expm1(x)
#define LIB_FUNC(x)     expm1(x)
#define LD_FUNC(x)      expm1l(x)

#elif MAT_FUNCTION == 3
#define FUNCNAME        "log"
#define SCALAR_FUNC(x)  log(x)
#if VECTOR_TYPE < 4
#define VDT_FUNC(x)     vdt::fast_logf(x)
#else
#define VDT_FUNC(x)     vdt::fast_log(x)
#endif
#define VECT_FUNC(x)    log(x)
#define LIB_FUNC(x)     log(x)
#define LD_FUNC(x)      logl(x)

#elif MAT_FUNCTION == 4
#define FUNCNAME        "log1p"
#define SCALAR_FUNC(x)  log1p(x)
#if VECTOR_TYPE < 4
#define VDT_FUNC(x)     vdt::fast_logf((x)+1.f)
#else
#define VDT_FUNC(x)     vdt::fast_log((x)+1.0)
#endif
#define VECT_FUNC(x)    log1p(x)
#define LIB_FUNC(x)     log1p(x)
#define LD_FUNC(x)      log1pl(x)

#elif MAT_FUNCTION == 5
#define FUNCNAME        "pow(vector,vector)"
#define SCALAR_FUNC(x)  pow(x,x)
#if VECTOR_TYPE < 4
#define VDT_FUNC(x)     vdt::fast_expf(x*vdt::fast_logf(abs(x)))
#else
#define VDT_FUNC(x)     vdt::fast_exp(x*vdt::fast_log(abs(x)))
#endif
#define VECT_FUNC(x)    pow(x,x)
#define LIB_FUNC(x)     pow(x,x)
#define LD_FUNC(x)      powl(x,x)

#elif MAT_FUNCTION == 6
#define FUNCNAME        "pow(vector,scalar)"
#define SCALAR_FUNC(x)  pow(x,y_pow)
#if VECTOR_TYPE < 4
#define VDT_FUNC(x)     vdt::fast_expf(y_pow*vdt::fast_logf(x))
#else
#define VDT_FUNC(x)     vdt::fast_exp(y_pow*vdt::fast_log(abs(x)))
#endif
#define VECT_FUNC(x)    pow(x,y_pow)
#define LIB_FUNC(x)     pow(x,vtype(y_pow))
#define LD_FUNC(x)      powl(x,y_pow)

#elif MAT_FUNCTION == 10
#define FUNCNAME        "sin"
#define SCALAR_FUNC(x)  sin(x)
#if VECTOR_TYPE < 4
#define VDT_FUNC(x)     vdt::fast_sinf(x)
#else
#define VDT_FUNC(x)     vdt::fast_sin(x)
#endif
#define VECT_FUNC(x)    sin(x)
#define LIB_FUNC(x)     sin(x)
#define LD_FUNC(x)      sinl(x)

#elif MAT_FUNCTION == 11
#define FUNCNAME        "cos"
#define SCALAR_FUNC(x)  cos(x)
#if VECTOR_TYPE < 4
#define VDT_FUNC(x)     vdt::fast_cosf(x)
#else
#define VDT_FUNC(x)     vdt::fast_cos(x)
#endif
#define VECT_FUNC(x)    cos(x)
#define LIB_FUNC(x)     cos(x)
#define LD_FUNC(x)      cosl(x)

#elif MAT_FUNCTION == 12
#define FUNCNAME        "tan"
#define SCALAR_FUNC(x)  tan(x)
#if VECTOR_TYPE < 4
#define VDT_FUNC(x)     vdt::fast_tanf(x)
#else
#define VDT_FUNC(x)     vdt::fast_tan(x)
#endif
#define VECT_FUNC(x)    tan(x)
#define LIB_FUNC(x)     tan(x)
#define LD_FUNC(x)      tanl(x)

#elif MAT_FUNCTION == 13
#define FUNCNAME        "asin"
#define SCALAR_FUNC(x)  asin(x)
#if VECTOR_TYPE < 4
#define VDT_FUNC(x)     vdt::fast_asinf(x)
#else
#define VDT_FUNC(x)     vdt::fast_asin(x)
#endif
#define VECT_FUNC(x)    asin(x)
#define LIB_FUNC(x)     asin(x)
#define LD_FUNC(x)      asinl(x)

#elif MAT_FUNCTION == 14
#define FUNCNAME        "acos"
#define SCALAR_FUNC(x)  acos(x)
#if VECTOR_TYPE < 4
#define VDT_FUNC(x)     vdt::fast_acosf(x)
#else
#define VDT_FUNC(x)     vdt::fast_acos(x)
#endif
#define VECT_FUNC(x)    acos(x)
#define LIB_FUNC(x)     acos(x)
#define LD_FUNC(x)      acosl(x)

#elif MAT_FUNCTION == 15
#define FUNCNAME        "atan"
#define SCALAR_FUNC(x)  atan(x)
#if VECTOR_TYPE < 4
#define VDT_FUNC(x)     vdt::fast_atanf(x)
#else
#define VDT_FUNC(x)     vdt::fast_atan(x)
#endif
#define VECT_FUNC(x)    atan(x)
#define LIB_FUNC(x)     atan(x)
#define LD_FUNC(x)      atanl(x)

#else
#error unknown function
#endif

// implementation names
#if   MAT_IMPLEMENT == 1
#define IMPLEMENTATIONNAME  "compiler default"
#elif MAT_IMPLEMENT == 2
#define IMPLEMENTATIONNAME  "VDT inline library with auto-vectorization"
#elif MAT_IMPLEMENT == 3  
#define IMPLEMENTATIONNAME  "Agner's vector class library with explicit vectorization"  
#elif MAT_IMPLEMENT == 4
#define IMPLEMENTATIONNAME  "external vector math library"
#elif MAT_IMPLEMENT == 5
#define IMPLEMENTATIONNAME  "vector class library wihth explicit vectorization (Knights Corner version)"
#else
#error unknown implementation
#endif


// include necessary header files
//#include "stdafx.h"
#include <stdio.h>
#include <math.h>
#include "vectorclass.h"
#include "timingtest.h"

// include implementation specific header files

#if   MAT_IMPLEMENT == 2     //  2: VDT inline library with auto-vectorization
#include "exp.h"
#include "log.h"
#include "sin.h"
#include "cos.h"
#include "tan.h"
#include "asin.h"
#include "atan.h"

#elif MAT_IMPLEMENT == 3     //  3: inline library with explicit vectorization
#include "vectormath_exp.h"
#include "vectormath_trig.h"

#elif MAT_IMPLEMENT == 4     //  4: external vector math library
#define VECTORMATH 2
#include "vectormath_lib.h"

#elif MAT_IMPLEMENT == 5     //  5: inline library with explicit vectorization (Knights Corner only)
#include "vectormath_exp_mic.h"
#include "vectormath_trig_mic.h"
#endif

// instruction set names
#if INSTRSET == 1
#define INSTRUCTIONSETNAME  "SSE"
#elif INSTRSET == 2
#define INSTRUCTIONSETNAME  "SSE2"
#elif INSTRSET == 3
#define INSTRUCTIONSETNAME  "SSE3"
#elif INSTRSET == 4
#define INSTRUCTIONSETNAME  "SSSE3"
#elif INSTRSET == 5
#define INSTRUCTIONSETNAME  "SSE4.1"
#elif INSTRSET == 6
#define INSTRUCTIONSETNAME  "SSE4.2"
#elif INSTRSET == 7
#define INSTRUCTIONSETNAME  "AVX"
#elif INSTRSET == 8
#define INSTRUCTIONSETNAME  "AVX2"
#elif INSTRSET == 9
#define INSTRUCTIONSETNAME  "KNCNI"
#else
#define INSTRUCTIONSETNAME  "unknown"
#endif

int run_count = 0;
// get value of least significant bit
float delta_unit(float x) {
    union {
        float f;
        uint32_t i;
    } u;
    x = fabs(x);
    Vec16f xv = Vec16f(x);
    if (!(is_finite(xv)[0])) {
        return 1.f;
    }
    if (x == 0.f || is_subnormal(xv)[0]) {
        u.i = 0x00800000;  // smallest positive normal number
        return u.f;
    }
    float x1 = x;
    u.f = x;
    u.i++;
    return u.f - x1;
}

double delta_unit(double x) {
    union {
        double f;
        uint64_t i;
    } u;
    x = fabs(x);
    Vec8d xv = Vec8d(x);
    if (!(is_finite(xv)[0])) return 1.;
    if (x == 0. || is_subnormal(xv)[0]) {
        u.i = 0x0010000000000000;  // smallest positive normal number
        return u.f;
    }
    double x1 = x;
    u.f = x;
    u.i++;
    return u.f - x1;
}

#if 1
// override CPU dispatch in Intel library
extern "C" {
#if defined ( __AVX2__ )
int __intel_cpu_indicator = 0x7FFFFF;
int __intel_cpu_feature_indicator = 0x816FFF;
#elif defined ( __AVX__ )
int __intel_cpu_indicator = 0x3FFFF;
int __intel_cpu_feature_indicator = 0x10FFF;
#else
int __intel_cpu_indicator = 0xFFFF;
int __intel_cpu_feature_indicator = 0x0FFF;
#endif
int  __intel_cpu_feature_indicator_x = __intel_cpu_feature_indicator;
void __intel_cpu_indicator_init() {}
void __intel_cpu_features_init() {}
void __intel_cpu_features_init_x() {}
}
#endif


// round up size to nearest multiple of numelements
//const int num = (NUM_VALUES + numelements - 1) & (-numelements);
//const int num = (NUM_VALUES + numelements - 2) & (-numelements);
const int num = 104;
#if INSTRSET == 9
// KNCNI requires 64b alignment
__declspec(align(64)) stype x[num] = {0};
__declspec(align(64)) stype y[num] = {0};

#else
// define aligned arrays
#ifdef __GNUC__
stype x[num] __attribute__ ((aligned (32))) = {0};
stype y[num] __attribute__ ((aligned (32))) = {0};
#else
__declspec(align(32)) stype x[num] = {0};
__declspec(align(32)) stype y[num] = {0};
#endif

#endif
// void test_loop() __attribute__ ((noinline));
void test_loop() {
    int i;
    vtype xvec, yvec;

    // loop to calculate y values
    for (i = 0; i < num; ) {

#if   MAT_IMPLEMENT == 1   //  1: compiler default
        y[i] = SCALAR_FUNC(x[i]);
        i++;
#elif MAT_IMPLEMENT == 2   //  2: VDT inline library with auto-vectorization
        y[i] = VDT_FUNC(x[i]);
        i++;
#elif MAT_IMPLEMENT == 3   //  3: inline library with explicit vectorization
        xvec.load(x+i);
        yvec = VECT_FUNC(xvec);
        yvec.store(y+i);
        i += numelements;
#elif MAT_IMPLEMENT == 4   //  4: external vector math library
        xvec.load(x+i);
        yvec = LIB_FUNC(xvec);
        yvec.store(y+i);
        i += numelements;
#elif MAT_IMPLEMENT == 5   //  5: inline library with explicit vectorization (Knights Corner only)
        xvec.load(x+i);
        yvec = VECT_FUNC(xvec);
        yvec.store(y+i);
        i += numelements;
#else


#error unknown MAT IMPLEMENT MAT_IMPLEMENT
#endif
    }      // end i loop
}


void run_test() {
    int i;

#if USE_DENORMALS == 0 // may be faster if 0 (no denormals)
    no_denormals();
#endif

#if WARMUP
    // dummy work to warm up CPU
    static double dum = 0.;
    for (i=0; i<1000000*WARMUP; i++) {
        dum = cos(dum);
    }
#endif

    printf("\nMeasuring precision");

    // calculate y values
    test_loop();

    // output listing
    int di = num / MAX_LIST;
    if (di < 1) di = 1;

    long double exacty;

    int count = 3;
    printf("\n    x       calculated y     exact y        error ULP");
    //for(i = 0; i < num; i++)printf("x[%d] = %5.100f\n", i, x[i]);
    for (i = 0; i < num; i += di) {
        //printf("iteration: %d x[%d]: %f\n", i, i, x[i]);
        exacty = LD_FUNC((long double)x[i]);
        //printf("exacty: %f\n", exacty);
        count --;
        //if(count == 0)assert(0);
        //printf("num di exacty: %d %d %5.20f\n", num, di,  delta_unit((stype)(exacty)));
        double error = ((double)(exacty - (long double)y[i])) / delta_unit((stype)(exacty));
        printf("\n" XFORMAT "  " YFORMAT "  " YFORMAT "  %5.1f", x[i], y[i], (double)exacty, error);
    }
    if (i != num - 1 + di) {
        // get last output
        i = num - 1;
        exacty = LD_FUNC((long double)x[i]);
        double error = ((double)(exacty - (long double)y[i])) / delta_unit((stype)(exacty));
        printf("\n" XFORMAT "  " YFORMAT "  " YFORMAT "  %5.1f", x[i], y[i], (double)exacty, error);
    }

    // calculate average error and max error
    double errorsum = 0, errormax = 0;
    for (i = 0; i < num; i ++) {
        exacty = LD_FUNC((long double)x[i]);
        double error = ((double)(exacty - (long double)y[i])) / delta_unit((stype)(exacty));
        errorsum += fabs(error);
        if (fabs(error) > fabs(errormax)) errormax = error;
    }
    printf("\nAverage error %5.2f, max error %6.2f (ULP)", errorsum / num, errormax); 
}

void run_timing() {
    int64_t tim1, tim2, pmc1, pmc2;
    const int num_runs = 10;
    int64_t timelist[num_runs];
    int64_t pmclist [num_runs];
    int i;
    double time_per_element;
    double pmc_per_element;


    for (i = 0; i < num_runs; i++) {

        serialize();    
        tim1 = readtsc();
#if defined(USEPMC)
        pmc1 = readpmc(USEPMC);
#endif

        test_loop();

        serialize();
        tim2 = readtsc();
#if defined(USEPMC)
        pmc2 = readpmc(USEPMC);
#endif
        timelist[i] = tim2 - tim1;
#if defined(USEPMC)
        pmclist[i] = pmc2 - pmc1;
#endif

    }
#if defined(USEPMC)
    printf("\nTiming test. Throughput, counts per element\nclock cycles   PMC count");
    for (i = 0; i < num_runs; i++) {
        time_per_element = (double)timelist[i] / (double)numelements;
        pmc_per_element  = (double)pmclist[i] / (double)numelements;
        printf("\n%10.0f  %10.0f", time_per_element, pmc_per_element);
    }
#else
    printf("\nTiming test. Throughput, clock cycles per element");
    for (i = 0; i < num_runs; i++) {
        time_per_element = (double)timelist[i] / (double)numelements;
        printf("\n%10.0f", time_per_element);
    }
#endif
}


int main(int argc, char* argv[])
{
    printf("\nTest function: %s", FUNCNAME);
    printf("\nImplementation: %s", IMPLEMENTATIONNAME);
    printf("\nInstruction set: %s", INSTRUCTIONSETNAME);
    printf("\nVectors of %i %s", numelements, VECTOR_TYPE < 4 ? "float" : "double");

    int i;
    stype xi = (stype)FIRST_VALUE;
    // put values into x
    for (i = 0; i < num; i++, xi += (stype)STEP_VALUE) x[i] = xi;

    printf("\n");

    // test timing
    run_timing();

    printf("\n");

    // test precision
    run_test();    

    printf("\n");

    return 0;
}  